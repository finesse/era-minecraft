package pw.finesse.era.events.system;

import pw.finesse.commons.event.Event;

/**
 * Called whenever the game is shut down.
 *
 * @author Marc
 * @since 8/26/2016
 */
public class ShutdownGameEvent extends Event {
}
