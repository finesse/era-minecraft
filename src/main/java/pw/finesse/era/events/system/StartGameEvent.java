package pw.finesse.era.events.system;

import net.minecraft.client.Minecraft;
import pw.finesse.commons.event.Event;

/**
 * Called when the game is initialized.
 * {@link Minecraft#startGame()}
 *
 * @author Marc
 * @since 8/2/2016
 */
public class StartGameEvent extends Event {

    /**
     * An instance of {@link Minecraft}.
     */
    private final Minecraft minecraft;

    public StartGameEvent(Minecraft minecraft) {
        this.minecraft = minecraft;
    }

    /**
     * Returns the {@link Minecraft} instance.
     *
     * @return {@link Minecraft} instance
     */
    public Minecraft getMinecraft() {
        return minecraft;
    }
}
