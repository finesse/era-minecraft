package pw.finesse.era.events.game.render.entity;

import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.EntityLivingBase;
import pw.finesse.commons.event.Cancellable;

/**
 * Called before an entity is rendered.
 * {@link RendererLivingEntity#doRender}
 *
 * @author Marc
 * @since 8/5/2016
 */
public class PreRenderEntityEvent extends Cancellable {

    /**
     * The calling class of the event.
     */
    private RendererLivingEntity caller;

    /**
     * An instance of the rendered {@link EntityLivingBase}.
     */
    private EntityLivingBase entityLivingBase;

    /**
     * The x coordinate of the rendered {@link EntityLivingBase}.
     */
    private double posX;

    /**
     * The y coordinate of the rendered {@link EntityLivingBase}.
     */
    private double posY;

    /**
     * The z coordinate of the rendered {@link EntityLivingBase}.
     */
    private double posZ;

    /**
     * The yaw of the entity.
     */
    private float yaw;

    /**
     * The current ticks of the world.
     */
    private float partialTicks;

    public PreRenderEntityEvent(RendererLivingEntity caller, EntityLivingBase entityLivingBase, double posX, double posY, double posZ, float yaw, float partialTicks) {
        this.caller = caller;
        this.entityLivingBase = entityLivingBase;
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
        this.partialTicks = partialTicks;
    }

    /**
     * Returns the caller of the event.
     *
     * @return caller of the event
     */
    public RendererLivingEntity getCaller() {
        return caller;
    }

    /**
     * Returns an instance of the rendered {@link EntityLivingBase}.
     *
     * @return instance of the rendered {@link EntityLivingBase}
     */
    public EntityLivingBase getEntity() {
        return entityLivingBase;
    }

    /**
     * Returns the x coordinate of the rendered {@link EntityLivingBase}..
     *
     * @return x coordinate of the rendered {@link EntityLivingBase}
     */
    public double getPosX() {
        return posX;
    }

    /**
     * Sets the x coordinate of the rendered {@link EntityLivingBase} to the specified coordinate.
     *
     * @param posX x coordinate to set
     */
    public void setPosX(double posX) {
        this.posX = posX;
    }

    /**
     * Returns the y coordinate of the rendered {@link EntityLivingBase}.
     *
     * @return y coordinate of the rendered {@link EntityLivingBase}
     */
    public double getPosY() {
        return posY;
    }

    /**
     * Sets the y coordinate of the rendered {@link EntityLivingBase} to the specified coordinate.
     *
     * @param posY y coordinate to set
     */
    public void setPosY(double posY) {
        this.posY = posY;
    }

    /**
     * Returns the z coordinate of the rendered {@link EntityLivingBase}.
     *
     * @return z coordinate of the rendered {@link EntityLivingBase}
     */
    public double getPosZ() {
        return posZ;
    }

    /**
     * Sets the z coordinate of the rendered {@link EntityLivingBase} to the specified coordinate.
     *
     * @param posZ z coordinate to set
     */
    public void setPosZ(double posZ) {
        this.posZ = posZ;
    }

    /**
     * Returns the yaw of the entity.
     *
     * @return yaw of the entity
     */
    public float getYaw() {
        return yaw;
    }

    /**
     * Returns the current ticks of the world.
     *
     * @return current ticks of the world
     */
    public float getPartialTicks() {
        return partialTicks;
    }
}
