package pw.finesse.era.events.game.player.movement;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import pw.finesse.commons.event.Cancellable;

/**
 * Called whenever the player moves.
 * {@link Entity#moveEntity(double, double, double)}
 *
 * @author Marc
 * @since 8/8/2016
 */
public class PlayerMovementEvent extends Cancellable {

    /**
     * An instance of the player.
     */
    private EntityPlayerSP player;

    /**
     * The x motion of the player.
     */
    private double motionX;

    /**
     * The y motion of the player.
     */
    private double motionY;

    /**
     * The z motion of the player.
     */
    private double motionZ;

    public PlayerMovementEvent(EntityPlayerSP player, double motionX, double motionY, double motionZ) {
        this.player = player;
        this.motionX = motionX;
        this.motionY = motionY;
        this.motionZ = motionZ;
    }

    /**
     * Returns the instance of the player.
     *
     * @return instance of the player
     */
    public EntityPlayerSP getPlayer() {
        return player;
    }

    /**
     * Returns the x motion of the player.
     *
     * @return x motion of the player
     */
    public double getMotionX() {
        return motionX;
    }

    /**
     * Sets the x motion of the player to the specified motion multiplier.
     *
     * @param motionX motion to set
     */
    public void setMotionX(double motionX) {
        this.motionX = motionX;
    }

    /**
     * Returns the y motion of the player.
     *
     * @return y motion of the player
     */
    public double getMotionY() {
        return motionY;
    }

    /**
     * Sets the y motion of the player to the specified motion multiplier.
     *
     * @param motionY motion to set
     */
    public void setMotionY(double motionY) {
        this.motionY = motionY;
    }

    /**
     * Returns the z motion of the player.
     *
     * @return z motion of the player
     */
    public double getMotionZ() {
        return motionZ;
    }

    /**
     * Sets the z motion of the player to the specified motion multiplier.
     *
     * @param motionZ motion to set
     */
    public void setMotionZ(double motionZ) {
        this.motionZ = motionZ;
    }
}
