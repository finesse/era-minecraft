package pw.finesse.era.events.game.render.entity;

import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import pw.finesse.commons.event.Event;

/**
 * Called when the player model is rendered.
 * {@link RendererLivingEntity#doRender(EntityLivingBase, double, double, double, float, float)}
 *
 * @author Jack
 * @since Jul 08, 2016
 */
public final class RenderPlayerModelEvent extends Event {
    /**
     * The class that the event is called in
     */
    private final RendererLivingEntity caller;

    /**
     * The player being rendered.
     */
    private final EntityPlayer entity;

    /**
     * The current ling swing time.
     */
    private final float limbSwing;

    /**
     * The current ling swing amount.
     */
    private final float limbSwingAmount;

    /**
     * The age of the entity.
     */
    private final float ticksExisted;

    /**
     * The head yaw of the entity.
     */
    private final float rotationYawHead;

    /**
     * The pitch of the entity
     */
    private final float rotationPitch;

    public RenderPlayerModelEvent(RendererLivingEntity caller, EntityPlayer entity, float limbSwing, float limbSwingAmount, float ticksExisted, float rotationYawHead, float rotationPitch) {
        this.caller = caller;
        this.entity = entity;
        this.limbSwing = limbSwing;
        this.limbSwingAmount = limbSwingAmount;
        this.ticksExisted = ticksExisted;
        this.rotationYawHead = rotationYawHead;
        this.rotationPitch = rotationPitch;
    }

    /**
     * Returns the calling entity of the event.
     *
     * @return caller entity
     */
    public RendererLivingEntity getCaller() {
        return this.caller;
    }

    /**
     * Returns the entity being rendered.
     *
     * @return rendered entity
     */
    public EntityPlayer getEntity() {
        return this.entity;
    }

    /**
     * Returns the limb swing time for the entity.
     *
     * @return entity swing time
     */
    public float getLimbSwing() {
        return this.limbSwing;
    }

    /**
     * Returns the limb swing amount for the entity.
     *
     * @return entity swing amount
     */
    public float getLimbSwingAmount() {
        return this.limbSwingAmount;
    }

    /**
     * Returns the age of the entity in ticks.
     *
     * @return entity age
     */
    public float getTicksExisted() {
        return this.ticksExisted;
    }

    /**
     * Returns the head yaw of the entity.
     *
     * @return entity head yaw
     */
    public float getRotationYawHead() {
        return this.rotationYawHead;
    }

    /**
     * Returns the rotation pitch of the entity.
     *
     * @return entity pitch
     */
    public float getRotationPitch() {
        return this.rotationPitch;
    }
}