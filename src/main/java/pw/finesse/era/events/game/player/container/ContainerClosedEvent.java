package pw.finesse.era.events.game.player.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import pw.finesse.commons.event.Cancellable;

/**
 * Called whenever a container is closed.
 * {@link Container#onContainerClosed(EntityPlayer)}
 *
 * @author Marc
 * @since 8/23/2016
 */
public class ContainerClosedEvent extends Cancellable {

    /**
     * The container that was closed.
     */
    private Container container;

    /**
     * The player that closed the container.
     */
    private EntityPlayer player;

    public ContainerClosedEvent(Container container, EntityPlayer player) {
        this.container = container;
        this.player = player;
    }

    /**
     * Returns the container that was closed.
     *
     * @return container that was closed
     */
    public Container getContainer() {
        return container;
    }

    /**
     * Returns the player that closed the container.
     *
     * @return player that closed the container
     */
    public EntityPlayer getPlayer() {
        return player;
    }
}
