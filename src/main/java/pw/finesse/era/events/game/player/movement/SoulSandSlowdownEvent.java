package pw.finesse.era.events.game.player.movement;

import net.minecraft.block.BlockSoulSand;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import pw.finesse.commons.event.Cancellable;

/**
 * Called whenever the player is slowed down by soul sand.
 * {@link BlockSoulSand#onEntityCollidedWithBlock(World, BlockPos, Entity)}
 *
 * @author Marc
 * @since 8/11/2016
 */
public class SoulSandSlowdownEvent extends Cancellable {

    /**
     * An instance of the player.
     */
    private EntityPlayerSP player;

    public SoulSandSlowdownEvent(EntityPlayerSP player) {
        this.player = player;
    }

    /**
     * Returns an instance of the player.
     *
     * @return instance of the player
     */
    public EntityPlayerSP getPlayer() {
        return player;
    }
}
