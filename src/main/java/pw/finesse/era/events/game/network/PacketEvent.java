package pw.finesse.era.events.game.network;

import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import pw.finesse.commons.event.Cancellable;

/**
 * Called when a packet is sent or received.
 * {@link NetHandlerPlayClient#addToSendQueue}
 * {@link NetworkManager#channelRead0}
 *
 * @author Marc
 * @since 8/6/2016
 */
public class PacketEvent extends Cancellable {

    /**
     * An instance of the packet.
     */
    private Packet packet;

    public PacketEvent(Packet packet) {
        this.packet = packet;
    }

    /**
     * Returns the instance of the packet.
     *
     * @return instance of the packet
     */
    public Packet getPacket() {
        return packet;
    }

    /**
     * Sets the packet to the specified packet.
     *
     * @param packet packet to set
     */
    public void setPacket(Packet packet) {
        this.packet = packet;
    }
}
