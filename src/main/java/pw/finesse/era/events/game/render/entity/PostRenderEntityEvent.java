package pw.finesse.era.events.game.render.entity;

import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.EntityLivingBase;
import pw.finesse.commons.event.Event;

/**
 * Called after an entity is rendered.
 * {@link RendererLivingEntity#doRender}
 * {@link PreRenderEntityEvent}
 *
 * @author Marc
 * @since 8/5/2016
 */
public class PostRenderEntityEvent extends Event {

    /**
     * The calling renderer of the event.
     */
    private RendererLivingEntity caller;

    /**
     * An instance of the rendered {@link EntityLivingBase}.
     */
    private EntityLivingBase entityLivingBase;

    /**
     * The current ticks of the world.
     */
    private float partialTicks;

    public PostRenderEntityEvent(RendererLivingEntity caller, EntityLivingBase entityLivingBase, float partialTicks) {
        this.caller = caller;
        this.entityLivingBase = entityLivingBase;
        this.partialTicks = partialTicks;
    }

    /**
     * Returns the caller of the event.
     *
     * @return caller of the event
     */
    public RendererLivingEntity getCaller() {
        return caller;
    }

    /**
     * Returns an instance of the rendered {@link EntityLivingBase}.
     *
     * @return instance of the rendered {@link EntityLivingBase}
     */
    public EntityLivingBase getEntity() {
        return entityLivingBase;
    }

    /**
     * Returns the current ticks of the world.
     *
     * @return current ticks of the world
     */
    public float getPartialTicks() {
        return partialTicks;
    }
}
