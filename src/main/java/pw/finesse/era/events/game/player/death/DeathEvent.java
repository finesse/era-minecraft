package pw.finesse.era.events.game.player.death;

import net.minecraft.entity.EntityLivingBase;
import pw.finesse.commons.event.Event;

/**
 * Called whenever an entity dies.
 *
 * @author Marc
 * @since 8/19/2016
 */
public class DeathEvent extends Event {

    /**
     * The {@link EntityLivingBase} that died.
     */
    private EntityLivingBase entity;

    /**
     * The death time of the entity.
     */
    private int deathTime;

    public DeathEvent(EntityLivingBase entity, int deathTime) {
        this.entity = entity;
        this.deathTime = deathTime;
    }

    /**
     * Returns the {@link EntityLivingBase} that died.
     *
     * @return {@link EntityLivingBase} that died
     */
    public EntityLivingBase getEntity() {
        return entity;
    }

    /**
     * Returns the death time of the entity.
     *
     * @return death time of the entity
     */
    public int getDeathTime() {
        return deathTime;
    }
}
