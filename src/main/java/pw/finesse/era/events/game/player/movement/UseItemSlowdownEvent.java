package pw.finesse.era.events.game.player.movement;

import net.minecraft.client.entity.EntityPlayerSP;
import pw.finesse.commons.event.Event;

/**
 * Called whenever the player is slowed down by the use of an item.
 * {@link EntityPlayerSP#onLivingUpdate()}
 *
 * @author Marc
 * @since 8/8/2016
 */
public class UseItemSlowdownEvent extends Event {

    /**
     * An instance of the player.
     */
    private EntityPlayerSP player;

    /**
     * The walk speed of the player.
     */
    private float walkSpeed;

    public UseItemSlowdownEvent(EntityPlayerSP player, float walkSpeed) {
        this.player = player;
        this.walkSpeed = walkSpeed;
    }

    /**
     * Returns the instance of the player.
     *
     * @return instance of the player
     */
    public EntityPlayerSP getPlayer() {
        return player;
    }

    /**
     * Returns the walk speed of the player.
     *
     * @return walk speed of the player
     */
    public float getWalkSpeed() {
        return walkSpeed;
    }

    /**
     * Sets the walk speed of the player to the specified speed.
     *
     * @param walkSpeed walk speed to set
     */
    public void setWalkSpeed(float walkSpeed) {
        this.walkSpeed = walkSpeed;
    }
}
