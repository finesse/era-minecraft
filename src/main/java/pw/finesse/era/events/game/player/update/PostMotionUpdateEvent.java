package pw.finesse.era.events.game.player.update;

import net.minecraft.client.entity.EntityPlayerSP;
import pw.finesse.commons.event.Event;

/**
 * Called after every player motion update.
 * {@link EntityPlayerSP#onUpdateWalkingPlayer()}
 *
 * @author Marc
 * @since 8/5/2016
 */
public class PostMotionUpdateEvent extends Event {

    /**
     * An instance of the player.
     */
    private EntityPlayerSP player;

    public PostMotionUpdateEvent(EntityPlayerSP player) {
        this.player = player;
    }

    /**
     * Returns the instance of the player.
     *
     * @return instance of the player
     */
    public EntityPlayerSP getPlayer() {
        return player;
    }

    /**
     * Return {@code true} if the player is moving, otherwise {@code false}.
     *
     * @return {@code true} if the player is moving, otherwise {@code false}
     */
    public boolean isMoving(EntityPlayerSP player) {
        double xDif = player.posX - player.lastTickPosX;
        double yDif = player.posY - player.lastTickPosY;
        double zDif = player.posZ - player.lastTickPosZ;

        return xDif * xDif + yDif * yDif + zDif * zDif > 9.0E-4D;
    }
}
