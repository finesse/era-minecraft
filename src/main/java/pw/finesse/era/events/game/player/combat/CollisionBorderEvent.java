package pw.finesse.era.events.game.player.combat;

import net.minecraft.entity.Entity;
import pw.finesse.commons.event.Event;

/**
 * Called whenever the collision border size is set.
 * {@link Entity#getCollisionBorderSize()}
 *
 * @author Marc
 * @since 8/14/2016
 */
public class CollisionBorderEvent extends Event {

    /**
     * An instance of {@link Entity}.
     */
    private Entity entity;

    /**
     * The size of the entity's collision border.
     */
    private float borderSize;

    public CollisionBorderEvent(Entity entity, float borderSize) {
        this.entity = entity;
        this.borderSize = borderSize;
    }

    /**
     * Returns the instance of {@link Entity}.
     *
     * @return instance of {@link Entity}
     */
    public Entity getEntity() {
        return entity;
    }

    /**
     * Returns the size of the entity's collision border.
     *
     * @return size of the entity's collision border
     */
    public float getBorderSize() {
        return borderSize;
    }

    /**
     * Sets the size of the entity's collision border to the specified size.
     *
     * @param borderSize border size to set
     */
    public void setBorderSize(float borderSize) {
        this.borderSize = borderSize;
    }
}
