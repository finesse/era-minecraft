package pw.finesse.era.events.game.player.update;

import net.minecraft.client.entity.EntityPlayerSP;
import pw.finesse.commons.event.Event;

/**
 * Called before every player motion update.
 * {@link EntityPlayerSP#onUpdateWalkingPlayer()}
 *
 * @author Marc
 * @since 8/5/2016
 */
public class PreMotionUpdateEvent extends Event {

    /**
     * An instance of the player.
     */
    private EntityPlayerSP player;

    /**
     * The x coordinate of the player.
     */
    private double posX;

    /**
     * The y coordinate of the player.
     */
    private double posY;

    /**
     * The z coordinate of the player.
     */
    private double posZ;

    /**
     * The yaw and pitch of the player.
     */
    private float rotationYaw, rotationPitch;

    /**
     * Whether the player is on the ground.
     */
    private boolean onGround;

    /**
     * Whether the player is sprinting.
     */
    private boolean isSprinting;

    public PreMotionUpdateEvent(EntityPlayerSP player, double posX, double posY, double posZ, float rotationYaw, float rotationPitch, boolean onGround, boolean isSprinting) {
        this.player = player;
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
        this.rotationYaw = rotationYaw;
        this.rotationPitch = rotationPitch;
        this.onGround = onGround;
        this.isSprinting = isSprinting;
    }

    /**
     * Returns the instance of the player.
     *
     * @return instance of the player
     */
    public EntityPlayerSP getPlayer() {
        return player;
    }

    /**
     * Returns the x coordinate of the player.
     *
     * @return x coordinate of the player
     */
    public double getPosX() {
        return posX;
    }

    /**
     * Sets the x coordinate of the player to the specified coordinate.
     *
     * @param posX x coordinate to set
     */
    public void setPosX(double posX) {
        this.posX = posX;
    }

    /**
     * Returns the y coordinate of the player.
     *
     * @return y coordinate of the player
     */
    public double getPosY() {
        return posY;
    }

    /**
     * Sets the y coordinate of the player to the specified coordinate.
     *
     * @param posY y coordinate to set
     */
    public void setPosY(double posY) {
        this.posY = posY;
    }

    /**
     * Returns the z coordinate of the player.
     *
     * @return z coordinate of the player
     */
    public double getPosZ() {
        return posZ;
    }

    /**
     * Sets the z coordinate of the player to the specified coordinate.
     *
     * @param posZ z coordinate to set
     */
    public void setPosZ(double posZ) {
        this.posZ = posZ;
    }

    /**
     * Returns the yaw of the player.
     *
     * @return yaw of the player
     */
    public float getRotationYaw() {
        return rotationYaw;
    }

    /**
     * Sets the yaw of the player to the specified yaw.
     *
     * @param rotationYaw yaw to set
     */
    public void setRotationYaw(float rotationYaw) {
        this.rotationYaw = rotationYaw;
    }

    /**
     * Returns the yaw of the player.
     *
     * @return yaw of the player
     */
    public float getRotationPitch() {
        return rotationPitch;
    }

    /**
     * Sets the pitch of the player to the specified pitch.
     *
     * @param rotationPitch pitch to set
     */
    public void setRotationPitch(float rotationPitch) {
        this.rotationPitch = rotationPitch;
    }

    /**
     * Returns {@code true} if the player is on ground, otherwise {@code false}.
     *
     * @return {@code true} if the player is on ground, otherwise {@code false}
     */
    public boolean isOnGround() {
        return onGround;
    }

    /**
     * Sets the on ground state of the player to the specified on ground
     * state. {@code true} will put the player on ground, while {@code false}
     * will not put the player on ground.
     *
     * @param onGround state to set
     */
    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }

    /**
     * Returns {@code true} if the player is on ground, otherwise {@code false}.
     *
     * @return {@code true} if the player is on ground, otherwise {@code false}
     */
    public boolean isSprinting() {
        return isSprinting;
    }

    /**
     * Sets the sprinting state of the player to the specified sprinting
     * state. {@code true} will make the player sprint, while {@code false}
     * will make the player stop sprinting.
     *
     * @param sprinting state to set
     */
    public void setSprinting(boolean sprinting) {
        isSprinting = sprinting;
    }

    /**
     * Return {@code true} if the player is moving, otherwise {@code false}.
     *
     * @return {@code true} if the player is moving, otherwise {@code false}
     */
    public boolean isMoving(EntityPlayerSP player) {
        double xDif = player.posX - player.lastTickPosX;
        double yDif = player.posY - player.lastTickPosY;
        double zDif = player.posZ - player.lastTickPosZ;

        return xDif * xDif + yDif * yDif + zDif * zDif > 9.0E-4D;
    }
}
