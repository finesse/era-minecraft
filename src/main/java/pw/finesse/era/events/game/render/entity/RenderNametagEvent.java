package pw.finesse.era.events.game.render.entity;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import pw.finesse.commons.event.Cancellable;

/**
 * Called whenever {@link Minecraft}'s name tags are rendered.
 * {@link Render#renderLivingLabel(Entity, String, double, double, double, int)}
 * {@link RendererLivingEntity#passSpecialRender(EntityLivingBase, double, double, double)}
 *
 * @author Marc
 * @since 8/11/2016
 */
public class RenderNametagEvent extends Cancellable {

    /**
     * An instance of {@link Entity}.
     */
    private Entity entity;

    /**
     * The text of the name tag.
     */
    private String text;

    public RenderNametagEvent(Entity entity, String text) {
        this.entity = entity;
        this.text = text;
    }

    /**
     * Returns the instance of {@link Entity}.
     *
     * @return instance of {@link Entity}
     */
    public Entity getEntity() {
        return entity;
    }

    /**
     * Returns the text of the name tag.
     *
     * @return text of the name tag
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the text of the name tag to the specified text.
     *
     * @param text text to set
     */
    public void setText(String text) {
        this.text = text;
    }
}
