package pw.finesse.era.events.game.render.world;

import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.world.World;
import pw.finesse.commons.event.Event;

/**
 * Called every world render update.
 * {@link EntityRenderer#renderWorldPass(int, float, long)}
 *
 * @author Marc
 * @since 8/6/2016
 */
public class RenderWorldEvent extends Event {

    /**
     * An instance of {@link World}.
     */
    private World world;

    /**
     * The current ticks of the {@link World}.
     */
    private float partialTicks;

    public RenderWorldEvent(World world, float partialTicks) {
        this.world = world;
        this.partialTicks = partialTicks;
    }

    /**
     * Returns the {@link World} instance.
     *
     * @return {@link World} instance
     */
    public World getWorld() {
        return world;
    }

    /**
     * Returns the current ticks of the {@link World}.
     *
     * @return current ticks of the {@link World}
     */
    public float getPartialTicks() {
        return partialTicks;
    }
}
