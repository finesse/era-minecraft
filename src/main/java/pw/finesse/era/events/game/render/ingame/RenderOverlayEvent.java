package pw.finesse.era.events.game.render.ingame;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.ScaledResolution;
import pw.finesse.commons.event.Event;

/**
 * Called when the in-game overlay is rendered.
 * {@link GuiIngame#renderGameOverlay(float)}
 *
 * @author Marc
 * @since 8/5/2016
 */
public class RenderOverlayEvent extends Event {

    /**
     * An instance of {@link Minecraft}.
     */
    private Minecraft minecraft;

    /**
     * The font renderer of the in-game overlay.
     */
    private FontRenderer fontRenderer;

    /**
     * The scaled resolution of the in-game overlay.
     */
    private ScaledResolution scaledResolution;

    public RenderOverlayEvent(Minecraft minecraft, FontRenderer fontRenderer, ScaledResolution scaledResolution) {
        this.minecraft = minecraft;
        this.fontRenderer = fontRenderer;
        this.scaledResolution = scaledResolution;
    }

    /**
     * Returns the {@link Minecraft} instance.
     *
     * @return {@link Minecraft} instance
     */
    public Minecraft getMinecraft() {
        return minecraft;
    }

    /**
     * Returns the font renderer of the in-game overlay.
     *
     * @return font renderer of the in-game overlay
     */
    public FontRenderer getFontRenderer() {
        return fontRenderer;
    }

    /**
     * Returns the scaled resolution of the in-game overlay.
     *
     * @return scaled resolution of the in-game overlay
     */
    public ScaledResolution getScaledResolution() {
        return scaledResolution;
    }

}
