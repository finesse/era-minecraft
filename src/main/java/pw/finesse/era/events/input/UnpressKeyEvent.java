package pw.finesse.era.events.input;

import net.minecraft.client.settings.KeyBinding;
import pw.finesse.commons.event.Cancellable;

/**
 * Called when keys are unpressed.
 * {@link KeyBinding#unPressAllKeys()}
 *
 * @author Marc
 * @since 8/14/2016
 */
public class UnpressKeyEvent extends Cancellable {

    /**
     * The key to be un-pressed.
     */
    private KeyBinding key;

    public UnpressKeyEvent(KeyBinding key) {
        this.key = key;
    }

    /**
     * Returns the key to be un-pressed.
     *
     * @return key to be un-pressed
     */
    public KeyBinding getKey() {
        return key;
    }
}
