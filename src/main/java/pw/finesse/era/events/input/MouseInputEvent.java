package pw.finesse.era.events.input;

import net.minecraft.client.Minecraft;
import pw.finesse.commons.event.Cancellable;

/**
 * Called when mouse input is recorded.
 * {@link Minecraft#runTick()}
 *
 * @author Marc
 * @since 8/5/2016
 */
public class MouseInputEvent extends Cancellable {

    /**
     * The mouse button of the event.
     */
    private int mouseButton;

    public MouseInputEvent(int mouseButton) {
        this.mouseButton = mouseButton;
    }

    /**
     * Returns the mouse button of the event.
     *
     * @return mouse button of the event.
     */
    public int getButton() {
        return mouseButton;
    }

    /**
     * Sets the mouse button of the event to the specified mouse button.
     *
     * @param mouseButton mouse button to set
     */
    public void setButton(int mouseButton) {
        this.mouseButton = mouseButton;
    }
}
