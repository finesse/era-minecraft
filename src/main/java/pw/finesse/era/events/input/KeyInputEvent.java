package pw.finesse.era.events.input;

import net.minecraft.client.Minecraft;
import pw.finesse.commons.event.Cancellable;

/**
 * Called when keyboard input is recorded.
 * {@link Minecraft#runTick()}
 *
 * @author Marc
 * @since 8/5/2016
 */
public class KeyInputEvent extends Cancellable {

    /**
     * The key code of the event.
     */
    private int keyCode;

    public KeyInputEvent(int keyCode) {
        this.keyCode = keyCode;
    }

    /**
     * Returns the key code of the event.
     *
     * @return key code of the event.
     */
    public int getKey() {
        return keyCode;
    }

    /**
     * Sets the key code of the event to the specified key code.
     *
     * @param keyCode key code to set
     */
    public void setKey(int keyCode) {
        this.keyCode = keyCode;
    }
}
