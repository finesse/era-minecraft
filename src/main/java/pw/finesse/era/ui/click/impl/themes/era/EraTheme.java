package pw.finesse.era.ui.click.impl.themes.era;

import net.minecraft.client.gui.Gui;
import pw.finesse.era.ui.click.api.component.Component;
import pw.finesse.era.ui.click.api.panel.Panel;
import pw.finesse.era.ui.click.api.theme.Theme;
import pw.finesse.era.ui.click.impl.components.module.ModuleButton;
import pw.finesse.era.ui.click.impl.components.property.PropertyButton;
import pw.finesse.era.ui.click.impl.components.property.PropertySlider;
import pw.finesse.era.ui.click.impl.themes.era.components.ModuleButtonUI;
import pw.finesse.era.ui.click.impl.themes.era.components.PropertyButtonUI;
import pw.finesse.era.ui.click.impl.themes.era.components.PropertySliderUI;
import pw.finesse.era.ui.font.NahrFont;

import static pw.finesse.era.ui.font.NahrFont.FontType.EMBOSS_TOP;
import static pw.finesse.era.ui.font.NahrFont.FontType.NORMAL;

/**
 * @author Marc
 * @since 11/12/2016
 */
public class EraTheme extends Theme {

    /**
     * The primary font used in the panel.
     */
    private final NahrFont font = new NahrFont("Arial", 17.0F);

    /**
     * The font used to draw the key-binding text.
     */
    private final NahrFont secondaryFont = new NahrFont("Arial", 11F);

    public EraTheme() {
        super("Era", 90, 14);
    }

    @Override
    protected void loadComponents() {
        this.componentUIs.put(ModuleButton.class, new ModuleButtonUI(this));
        this.componentUIs.put(PropertyButton.class, new PropertyButtonUI(this));
        this.componentUIs.put(PropertySlider.class, new PropertySliderUI(this));
    }

    @Override
    public void drawPanel(Panel panel, int x, int y) {
        // Draws a rectangle, used for the top bar of the panel.
        Gui.drawRect(panel.getX(), panel.getY(), panel.getX() + panel.getWidth(), panel.getY() + panel.getHeight(), 0xF82C2C2C);
        // Draws the title text over the top bar of the panel.
        font.drawString(panel.getLabel(), panel.getX() + 3, panel.getY(), NORMAL, -1);

        // Checks if the top bar of the panel is being hovered over.
        if(panel.isHovered(x, y)) {
            // Draws a string displaying the number of modules in the panel.
            secondaryFont.drawString(panel.size() + "", panel.getX() + panel.getWidth() - secondaryFont.getStringWidth(panel.size() + "") - 4, panel.getY() + 2, EMBOSS_TOP, 0x80EEEEEE);
        }

        // Checks if the panel is open.
        if (panel.isOpen()) {
            // Creates a local instance of the height of panel.
            int height = panel.getY() + this.getHeight();
            // Creates a local instance of the offset of the sub components.
            int offset = 1;

            // Iterates through the components in the panel.
            for (Component c : panel.registry()) {
                    // Checks if the component is a module button.
                if (c instanceof ModuleButton) {
                    // Creates a local cast of the module button.
                    ModuleButton component = (ModuleButton) c;

                    // Adjusts the size of the component.
                    component.setSize(panel.getX() + offset, height, this.getWidth() - offset * 2, 16);
                    // Draws the component.
                    this.componentUIs.get(component.getClass()).draw(component, x, y);
                    // Increments the height of the panel.
                    height += component.getHeight();

                    // Iterates through the subcomponents of the module component.
                    for (Component subcomponent : component.getSubcomponents()) {
                        // Checks if the component is visible.
                        if (subcomponent.isVisible()) {
                            // Adjusts the size of the subcomponent.
                            subcomponent.setSize(panel.getX() + offset * 2, height, this.getWidth() - offset * 4, 14);
                            // Draws the subcomponent.
                            this.componentUIs.get(subcomponent.getClass()).draw(subcomponent, x, y);

                            height += subcomponent.getHeight();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void mouseClicked(Panel panel, int x, int y, int button) {
        // Checks if the user is hovering over the panel header.
        if (panel.isHovered(x, y)) {
            // Checks if the button is a right click.
            if (button == 1) {
                // Toggles the opened state of the panel.
                panel.setOpen(!panel.isOpen());
            }
        } else {
            // Checks if the panel is open.
            if (panel.isOpen()) {
                // Iterates through each component and calls the mouse click event.
                panel.registry().forEach(component -> component.mouseClicked(x, y, button));
            }
        }


    }

    @Override
    public void mouseReleased(Panel panel, int x, int y, int button) {
        // Checks if the panel is open.
        if (panel.isOpen()) {
            // Iterates through each component and calls the mouse release event.
            panel.registry().forEach(component -> component.mouseReleased(x, y, button));
        }
    }

    @Override
    public void keyTyped(Panel panel, char typedChar, int keyCode) {
        // Checks if the panel is open.
        if (panel.isOpen()) {
            // Iterates through each component and calls the key typing event.
            panel.registry().forEach(component -> component.keyTyped(typedChar, keyCode));
        }
    }
}
