package pw.finesse.era.ui.click.impl.themes.era.components;

import net.minecraft.client.gui.Gui;
import pw.finesse.commons.property.Clamp;
import pw.finesse.era.ui.click.api.component.ComponentUI;
import pw.finesse.era.ui.click.api.theme.Theme;
import pw.finesse.era.ui.click.impl.components.property.PropertySlider;
import pw.finesse.era.ui.font.NahrFont;

/**
 * @author Marc
 * @since 11/12/2016
 */
public class PropertySliderUI extends ComponentUI<PropertySlider> {

    /**
     * The font used to draw the button,
     */
    private final NahrFont font = new NahrFont("Arial", 13F);

    public PropertySliderUI(Theme theme) {
        super(theme);
    }

    @Override
    public void draw(PropertySlider component, int x, int y) {
        // Performs the logic of the slider.
        this.logic(component, x, y);

        // Creates a local instance of the completion of the slider.
        int completion = (int) component.getCompletion();

        // Draws the blank area of the slider.
        Gui.drawRect(component.getX() + completion, component.getY(), component.getX() + component.getWidth(), component.getY() + component.getHeight(), 0xF8383838);
        // Draws the filled area of the slider.
        Gui.drawRect(component.getX(), component.getY(), component.getX() + completion, component.getY() + component.getHeight(), 0xF8476989);

        // Checks if the slider is being hovered over.
        if (component.isHovered(x, y)) {
            // Draws a white overlay over the slider.
            Gui.drawRect(component.getX(), component.getY(), component.getX() + component.getWidth(), component.getY() + component.getHeight(), 0x26FFFFFF);
        }

        // Draws the name of the value.
        font.drawString(component.getLabel(), component.getX() + 4, component.getY() + 2, NahrFont.FontType.NORMAL, -1);

        // Draws the value of the slider.
        font.drawString(component.getValue().toString(), component.getX() + component.getWidth() - font.getStringWidth(component.getValue() + "") - 4, component.getY() + 2, NahrFont.FontType.NORMAL, -1);
    }

    /**
     * The logic of the slider.
     *
     * @param component component instance
     * @param x mouse x coordinate
     * @param y mouse y coordinate
     */
    private void logic(PropertySlider component, int x, int y) {
        // Checks if the slider is being dragged.
        if (component.isDragging()) {
            // Creates a local instance of the x coordinate of the mouse from the start of the slider.
            double mouseX = x - component.getX();
            // Creates a local instance of how completed the slider is from 0-1.
            double percentage = Math.min(Math.max(0.0, mouseX / component.getWidth()), 1.0);
            // Creates a local instance of how far along the mouse is on the width of the component.
            double completion = (component.getMax() - component.getMin()) * percentage;
            // Creates a local instance of the final value of the slider.
            double value = Math.round((component.getMin() + completion) * 10.0D) / 10.0D;

            if (component.getValue() instanceof Double) {
                component.setValue(value);
            } else if (component.getValue() instanceof Float) {
                component.setValue((float) value);
            } else if (component.getValue() instanceof Integer) {
                component.setValue((int) value);
            }
        }

        // Creates a local instance of the completion of slider for graphical display.
        double completion = 0;

        // Sets the minimum and maximum values of the slider.
        component.setMin(component.getField().getAnnotation(Clamp.class).min());
        component.setMax(component.getField().getAnnotation(Clamp.class).max());

        if (component.getValue() instanceof Double) {
            completion = (Double) component.getValue() - component.getMin();
        } else if (component.getValue() instanceof Float) {
            completion = (Float) component.getValue() - component.getMin();
        } else if (component.getValue() instanceof Integer) {
            completion = (Integer) component.getValue() - component.getMin();
        }

        // Creates a local instance of how completed the slider is from 0-1.
        double percentage = completion / (component.getMax() - component.getMin());

        // Sets the completion by the width of the slider and the percentage factor.
        component.setCompletion(component.getWidth() * percentage);
    }
}
