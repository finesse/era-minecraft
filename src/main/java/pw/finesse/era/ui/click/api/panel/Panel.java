package pw.finesse.era.ui.click.api.panel;

import pw.finesse.commons.interfaces.Labelled;
import pw.finesse.commons.management.impl.ListManager;
import pw.finesse.era.ui.click.api.component.Component;
import pw.finesse.era.ui.click.impl.GuiClick;

import java.util.ArrayList;

/**
 * A panel, or window, for use in a clickable menu.
 *
 * @author Marc
 * @since 11/11/2016
 */
public abstract class Panel extends ListManager<Component> implements Labelled {

    /**
     * The parent screen of the panel.
     */
    private final GuiClick parent;

    /**
     * The label of the panel.
     */
    private String label;

    /**
     * The x and y coordinates of the panel.
     */
    private int x, y;

    /**
     * The previous x and y coordinates of the panel.
     */
    private int prevX, prevY;

    /**
     * The width and height of the panel.
     */
    private int width, height;

    /**
     * The opened state of the panel.
     */
    private boolean open;

    /**
     * The dragging state of the panel.
     */
    private boolean dragging;

    public Panel(GuiClick parent, String label, int x, int y, int width, int height) {
        super(new ArrayList<>());

        this.parent = parent;
        this.label = label;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        this.setup();
    }

    /**
     * Called when the panel is initialized.
     */
    protected abstract void setup();

    public void drawPanel(int x, int y) {
        // Checks if the panel is being dragged.
        if(this.dragging) {
            // Sets the x and y coordinates of the panel to the location of the mouse.
            this.x = x + this.prevX;
            this.y = y + this.prevY;
        }

        parent.getTheme().drawPanel(this, x, y);
    }

    /**
     * Called when the mouse is clicked.
     *
     * @param x      the x coordinate of the mouse
     * @param y      the y coordinate of the mouse
     * @param button the button id of the mouse input
     */
    public void mouseClicked(int x, int y, int button) {
        // Checks if the user is hovered over the panel.
        if(this.isHovered(x, y)) {
            // Checks if the button is a left click.
            if(button == 0) {
                // Sets the previous positions to the difference between mouse coordinates.
                this.prevX = this.x - x;
                this.prevY = this.y - y;

                // Sets the dragging state of the panel.
                this.dragging = true;

                for (Panel panel : this.parent.getPanels()) {
                    // Checks if it's the current panel.
                    if(panel == this) {
                        continue;
                    }

                    // Sets the dragging state of the panel.
                    panel.dragging = false;
                }
            }

            // Unregisters and re-registers the panel to keep it in focus and on top.
            this.parent.getPanels().remove(this);
            this.parent.getPanels().add(this);
        }

        // Calls the mouse click event in the theme.
        parent.getTheme().mouseClicked(this, x, y, button);
    }

    /**
     * Called when the mouse is released.
     *
     * @param x      the x coordinate of the mouse
     * @param y      the y coordinate of the mouse
     * @param button the button id of the mouse input
     */
    public void mouseReleased(int x, int y, int button) {
        // Sets the dragging state of the panel.
        this.dragging = false;

        // Calls the mouse release event in the theme.
        parent.getTheme().mouseReleased(this, x, y, button);
    }

    /**
     * Called when key input is recorded.
     *
     * @param typedChar typed character
     * @param keyCode key code
     */
    public void keyTyped(char typedChar, int keyCode) {
        // Calls the key typing event in the theme.
        parent.getTheme().keyTyped(this, typedChar, keyCode);
    }

    @Override
    public String getLabel() {
        return label;
    }

    /**
     * Returns the x coordinate of the panel.
     *
     * @return x coordinate of the panel
     */
    public int getX() {
        return x;
    }

    /**
     * Sets the x coordinate of the panel to the specified coordinate.
     *
     * @param x x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Returns the y coordinate of the panel.
     *
     * @return y coordinate of the panel
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the y coordinate of the panel to the specified coordinate.
     *
     * @param y y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Returns the width of the panel.
     *
     * @return width of the panel
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width of the panel to the specified width.
     *
     * @param width width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Returns the height of the panel.
     *
     * @return height of the panel
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the height of the panel to the specified height.
     *
     * @param height height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Returns {@code true} if the panel is open, otherwise {@code false}.
     *
     * @return {@code true} if the panel is open, otherwise {@code false}
     */
    public boolean isOpen() {
        return open;
    }

    /**
     * Sets the opened state of the panel.
     *
     * @param open opened state to set
     */
    public void setOpen(boolean open) {
        this.open = open;
    }

    /**
     * Returns {@code true} if the panel is being dragged, otherwise {@code false}.
     *
     * @return {@code true} if the panel is being dragged, otherwise {@code false}
     */
    public boolean isDragging() {
        return dragging;
    }

    /**
     * Sets the dragging state of the panel.
     *
     * @param dragging dragging state to set
     */
    public void setDragging(boolean dragging) {
        this.dragging = dragging;
    }

    /**
     * Returns {@code true} if the user is hovering over the defined area, otherwise {@code false}.
     *
     * @param x the x coordinate of the mouse
     * @param y the y coordinate of the mouse
     * @return Returns {@code true} if the user is hovering over the defined area, otherwise {@code false}.
     */
    public boolean isHovered(int x, int y) {
        return x > this.x && y > this.y && x < this.x + width && y < this.y + height;
    }
}
