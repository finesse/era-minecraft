package pw.finesse.era.ui.click.impl.components.property;

import pw.finesse.era.ui.click.impl.GuiClick;
import pw.finesse.era.ui.click.impl.components.basic.Button;

import java.lang.reflect.Field;

/**
 * @author Marc
 * @since 11/12/2016
 */
public class PropertyButton extends Button {

    /**
     * The field of the property.
     */
    private Field field;

    /**
     * The object of which the field is to be extracted.
     */
    private Object object;

    public PropertyButton(GuiClick parent, String label, Field field, Object object, int x, int y, int width, int height) throws IllegalAccessException {
        super(parent, label, (Boolean) field.get(object), x, y, width, height);

        this.field = field;
        this.object = object;
    }

    @Override
    public boolean isEnabled() {
        try {
            return (boolean) this.field.get(this.object);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void setEnabled(boolean enabled) {
        try {
            this.field.set(this.object, enabled);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
