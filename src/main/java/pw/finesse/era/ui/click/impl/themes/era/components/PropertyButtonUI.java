package pw.finesse.era.ui.click.impl.themes.era.components;

import net.minecraft.client.gui.Gui;
import pw.finesse.era.ui.click.api.component.ComponentUI;
import pw.finesse.era.ui.click.api.theme.Theme;
import pw.finesse.era.ui.click.impl.components.property.PropertyButton;
import pw.finesse.era.ui.font.NahrFont;

/**
 * @author Marc
 * @since 11/12/2016
 */
public class PropertyButtonUI extends ComponentUI<PropertyButton> {

    /**
     * The font used to draw the button,
     */
    private final NahrFont font = new NahrFont("Arial", 13F);

    public PropertyButtonUI(Theme theme) {
        super(theme);
    }

    @Override
    public void draw(PropertyButton component, int x, int y) {
        // Draws the background of the button.
        Gui.drawRect(component.getX(), component.getY(), component.getX() + component.getWidth(), component.getY() + component.getHeight(), component.isEnabled() ? 0xF8466787 : 0xF8383838);

        // Checks if the button is being hovered over.
        if(component.isHovered(x, y)) {
            // Draws a white overlay over the button.
            Gui.drawRect(component.getX(), component.getY(), component.getX() + component.getWidth(), component.getY() + component.getHeight(), 0x26FFFFFF);
        }

        // Draws the name of the property.
        font.drawString(component.getLabel(), component.getX() + 4, component.getY() + 2, NahrFont.FontType.NORMAL, -1);
    }
}
