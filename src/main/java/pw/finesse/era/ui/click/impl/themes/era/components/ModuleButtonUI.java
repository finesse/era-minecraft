package pw.finesse.era.ui.click.impl.themes.era.components;

import net.minecraft.client.gui.Gui;
import org.lwjgl.input.Keyboard;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.ui.click.api.component.ComponentUI;
import pw.finesse.era.ui.click.api.theme.Theme;
import pw.finesse.era.ui.click.impl.components.module.ModuleButton;
import pw.finesse.era.ui.font.NahrFont;

import static pw.finesse.era.ui.font.NahrFont.FontType.EMBOSS_TOP;
import static pw.finesse.era.ui.font.NahrFont.FontType.NORMAL;

/**
 * @author Marc
 * @since 11/12/2016
 */
public class ModuleButtonUI extends ComponentUI<ModuleButton> {

    /**
     * The font used to draw the button,
     */
    private final NahrFont font = new NahrFont("Arial", 15.1F);

    /**
     * The font used to draw the key-binding text.
     */
    private final NahrFont secondaryFont = new NahrFont("Arial", 11F);

    public ModuleButtonUI(Theme theme) {
        super(theme);
    }

    @Override
    public void draw(ModuleButton component, int x, int y) {
        // Creates a local instance of the module.
        Toggleable module = component.getModule();
        // Creates a local instance of the keybind text.
        String keybindText;

        // Checks if the button is being bound.
        if(component.isBinding()) {
            keybindText = "...";
        } else {
            // Creates a local instance of the module's keybind.
            int keybind = module.getKeybind();

            // Checks if keybind is valid.
            if(keybind != Keyboard.KEY_NONE) {
                keybindText = Keyboard.getKeyName(keybind);
            } else {
                keybindText = "";
            }
        }

        // Draws the background of the button.
        Gui.drawRect(component.getX(), component.getY(), component.getX() + component.getWidth(), component.getY() + component.getHeight(), component.isEnabled() ? 0xF84c80b2 : 0xF8404040);

        // Checks if the button is being hovered over.
        if(component.isHovered(x, y)) {
            // Draws a white overlay over the button.
            Gui.drawRect(component.getX(), component.getY(), component.getX() + component.getWidth(), component.getY() + component.getHeight(), 0x26FFFFFF);

            // Draws the keybind of the module.
            secondaryFont.drawString(keybindText, component.getX() + component.getWidth() - secondaryFont.getStringWidth(keybindText) - 4, component.getY() + 3, EMBOSS_TOP, 0xFFEEEEEE);
        }

        // Draws the text of the button.
        font.drawString(component.getLabel(), component.getX() + 4, component.getY() + 2, NORMAL, -1);
    }
}
