package pw.finesse.era.ui.click.api.theme;

import pw.finesse.commons.interfaces.Labelled;
import pw.finesse.era.ui.click.api.component.Component;
import pw.finesse.era.ui.click.api.component.ComponentUI;
import pw.finesse.era.ui.click.api.panel.Panel;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Marc
 * @since 11/11/2016
 */
public abstract class Theme implements Labelled {

    /**
     * The label of the theme.
     */
    private String label;

    /**
     * The width and height of the theme's panels.
     */
    private int width, height;

    /**
     * A collection that maps a component class to a UI implementation.
     */
    protected Map<Class<? extends Component>, ComponentUI> componentUIs = new HashMap<>();

    public Theme(String label, int width, int height) {
        this.label = label;
        this.width = width;
        this.height = height;

        this.loadComponents();
    }

    /**
     * Maps all components to their respective UIs.
     */
    protected abstract void loadComponents();


    /**
     * Called to draw the specified panel.
     *
     * @param panel panel to draw
     * @param x     the x coordinate of the mouse
     * @param y     the y coordinate of the mouse
     */
    public abstract void drawPanel(Panel panel, int x, int y);

    /**
     * Called when the mouse is clicked.
     *
     * @param panel  panel to handle
     * @param x      the x coordinate of the mouse
     * @param y      the y coordinate of the mouse
     * @param button the button id of the mouse input
     */
    public abstract void mouseClicked(Panel panel, int x, int y, int button);

    /**
     * Called when the mouse is released.
     *
     * @param panel  panel to handle
     * @param x      the x coordinate of the mouse
     * @param y      the y coordinate of the mouse
     * @param button the button id of the mouse input
     */
    public abstract void mouseReleased(Panel panel, int x, int y, int button);
    /**
     * Called when key input is recorded.
     *
     * @param panel     panel to handle
     * @param typedChar the typed character
     * @param keyCode   the key code of the pressed key
     */
    public abstract void keyTyped(Panel panel, char typedChar, int keyCode);

    @Override
    public String getLabel() {
        return label;
    }

    /**
     * Returns the width of the panel.
     *
     * @return width of the panel
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width of the panel to the specified width.
     *
     * @param width width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Returns the height of the panel.
     *
     * @return height of the panel
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the height of the panel to the specified height.
     *
     * @param height height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }
}
