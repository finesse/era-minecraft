package pw.finesse.era.ui.click.impl.components.module;

import org.lwjgl.input.Keyboard;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.ui.click.api.component.Component;
import pw.finesse.era.ui.click.impl.GuiClick;
import pw.finesse.era.ui.click.impl.components.basic.Button;
import pw.finesse.era.ui.click.impl.components.property.PropertyButton;
import pw.finesse.era.ui.click.impl.components.property.PropertySlider;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Marc
 * @since 11/12/2016
 */
public class ModuleButton extends Button {

    /**
     * An instance of the module the button belongs to.
     */
    private Toggleable module;

    /**
     * A collection of components belonging to the module.
     */
    private List<Component> subcomponents = new ArrayList<>();

    /**
     * Whether the user is binding a key.
     */
    private boolean binding;

    public ModuleButton(GuiClick parent, Toggleable module, int x, int y, int width, int height) {
        super(parent, module.getLabel(), module.isEnabled(), x, y, width, height);

        this.module = module;

        this.registerSubcomponents();
    }

    @Override
    public void mouseClicked(int x, int y, int button) {
        super.mouseClicked(x, y, button);

        // Checks if the mouse is hovered over the component.
        if (this.isHovered(x, y)) {
            // Checks if the button is a middle click.
            if (button == 2) {
                // Toggles the binding state of the button.
                this.binding = !this.binding;
            }
        }

        // Iterates through the subcomponents and calls the mouse click event.
        subcomponents.stream().filter(Component::isVisible).forEach(subcomponent -> subcomponent.mouseClicked(x, y, button));
    }

    @Override
    public void mouseReleased(int x, int y, int button) {
        super.mouseReleased(x, y, button);

        // Checks if the mouse is hovered over the component.
        if (this.isHovered(x, y)) {
            // Checks if the button is a right click.
            if (button == 1) {
                // Iterates through the property components.
                this.subcomponents.forEach(component -> {
                    // Toggles the visibility of the component.
                    component.setVisible(!component.isVisible());
                });
            }
        }
        // Iterates through the subcomponents and calls the mouse release event.
        subcomponents.stream().filter(Component::isVisible).forEach(subcomponent -> subcomponent.mouseReleased(x, y, button));
    }

    @Override
    public void keyTyped(char typedChar, int keyCode) {
        // Checks if user is binding a key.
        if (this.binding) {
            // Checks if the key is the space-bar.
            if (keyCode != Keyboard.KEY_SPACE) {
                // Sets the keybind of the module to the typed key code.
                this.module.setKeybind(keyCode);
            } else {
                // Sets the keybind of the module to nothing.
                this.module.setKeybind(Keyboard.KEY_NONE);
            }

            // Sets the binding state to false.
            this.binding = false;
        }

        // Iterates through the subcomponents and calls the key typing event.
        subcomponents.stream().filter(Component::isVisible).forEach(subcomponent -> subcomponent.keyTyped(typedChar, keyCode));
    }

    @Override
    public boolean isEnabled() {
        return this.module.isEnabled();
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.module.setEnabled(enabled);
    }

    @Override
    public void toggle() {
        this.module.setEnabled(!this.module.isEnabled());
    }

    /**
     * Called to register subcomponents.
     */
    private void registerSubcomponents() {
        // Iterates through the properties belonging to the module.
        for (Field field : this.module.getProperties()) {
            try {
                // Sets the accessibility of the field.
                field.setAccessible(true);
                // Creates a local instance of the name of the property.
                String name = field.getAnnotation(Property.class).label();
                // Creates a local instance of the property object.
                Object property = field.get(this.module);

                // Checks if the property is a boolean.
                if (property instanceof Boolean) {
                    // Adds the property to the list of subcomponents.
                    this.subcomponents.add(new PropertyButton(this.parent, name, field, this.module, this.x, this.y, this.width, this.height));
                }

                // Checks if the property is a number.
                if (property instanceof Number) {

                    // Sets the minimum and maximum values to the supplied values in the clamping annotation.
                    double min = field.getAnnotation(Clamp.class).min();
                    double max = field.getAnnotation(Clamp.class).max();

                    // Adds the property to the list of subcomponents.
                    this.subcomponents.add(new PropertySlider(this.parent, name, field, this.module, min, max, this.x, this.y, this.width, this.height));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns the module the button belongs to.
     *
     * @return module the button belongs to
     */
    public Toggleable getModule() {
        return module;
    }

    /**
     * Returns the subcomponents of the button.
     *
     * @return subcomponents of the button
     */
    public List<Component> getSubcomponents() {
        return subcomponents;
    }

    /**
     * Returns {@code true} if the user is binding a key, otherwise {@code false}.
     *
     * @return {@code true} if the user is binding a key, otherwise {@code false}
     */
    public boolean isBinding() {
        return binding;
    }
}