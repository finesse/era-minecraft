package pw.finesse.era.ui.click.api.component;

import pw.finesse.commons.interfaces.Labelled;
import pw.finesse.era.ui.click.impl.GuiClick;

/**
 * @author Marc
 * @since 11/11/2016
 */
public class Component implements Labelled {

    /**
     * The parent screen of the component.
     */
    protected final GuiClick parent;

    /**
     * The label of the component.
     */
    protected String label;

    /**
     * The x and y coordinates of the component.
     */
    protected int x, y;

    /**
     * The width and height of the component.
     */
    protected int width, height;

    /**
     * The visibility of the component.
     */
    protected boolean visible = false;

    public Component(GuiClick parent, String label, int x, int y, int width, int height) {
        this.parent = parent;
        this.label = label;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Called when the mouse is clicked.
     *
     * @param x      the x coordinate of the mouse
     * @param y      the y coordinate of the mouse
     * @param button the button id of the mouse input
     */
    public void mouseClicked(int x, int y, int button) {}

    /**
     * Called when the mouse is released.
     *
     * @param x      the x coordinate of the mouse
     * @param y      the y coordinate of the mouse
     * @param button the button id of the mouse input
     */
    public void mouseReleased(int x, int y, int button) {}

    /**
     * Called when key input is recorded.
     *
     * @param typedChar the typed character
     * @param keyCode   the key code of the pressed key
     */
    public void keyTyped(char typedChar, int keyCode) {}

    @Override
    public String getLabel() {
        return label;
    }

    /**
     * Returns the x coordinate of the component.
     *
     * @return x coordinate of the component
     */
    public int getX() {
        return x;
    }

    /**
     * Sets the x coordinate of the component to the specified coordinate.
     *
     * @param x x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Returns the y coordinate of the component.
     *
     * @return y coordinate of the component
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the y coordinate of the component to the specified coordinate.
     *
     * @param y y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Returns the width of the component.
     *
     * @return width of the component
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width of the component to the specified width.
     *
     * @param width width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Returns the height of the component.
     *
     * @return height of the component
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the height of the component to the specified height.
     *
     * @param height height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Sets the size or dimensions of the component.
     *
     * @param x x to set
     * @param y y to set
     * @param width width to set
     * @param height height to set
     */
    public void setSize(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Returns {@code true} if the component is visible, otherwise {@code false}.
     *
     * @return {@code true} if the component is visible, otherwise {@code false}
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Sets the visibility of the component to the specified visibility.
     *
     * @param visible visibility to set
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * Returns {@code true} if the user is hovering over the defined area, otherwise {@code false}.
     *
     * @param x the x coordinate of the mouse
     * @param y the y coordinate of the mouse
     * @return Returns {@code true} if the user is hovering over the defined area, otherwise {@code false}.
     */
    public boolean isHovered(int x, int y) {
        return x > this.x && y > this.y && x < this.x + width && y < this.y + height;
    }
}
