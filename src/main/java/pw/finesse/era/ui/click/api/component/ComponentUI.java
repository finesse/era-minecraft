package pw.finesse.era.ui.click.api.component;

import pw.finesse.era.ui.click.api.theme.Theme;

/**
 * @author Marc
 * @since 11/12/2016
 */
public abstract class ComponentUI<T extends Component> {

    /**
     * An instance of the theme.
     */
    protected Theme theme;

    public ComponentUI(Theme theme) {
        this.theme = theme;
    }

    /**
     * Draws the component at the respective x and y coordinates.
     *
     * @param component component to draw
     * @param x         the x coordinate of the mouse
     * @param y         the y coordinate of the mouse
     */
    public abstract void draw(T component, int x, int y);
}
