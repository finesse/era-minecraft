package pw.finesse.era.ui.click.impl;

import net.minecraft.client.gui.GuiScreen;
import pw.finesse.era.core.Era;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.module.impl.visuals.ClickGUI;
import pw.finesse.era.ui.click.api.panel.Panel;
import pw.finesse.era.ui.click.api.theme.Theme;
import pw.finesse.era.ui.click.impl.components.module.ModuleButton;
import pw.finesse.era.ui.click.impl.themes.era.EraTheme;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Marc
 * @since 11/11/2016
 */
public class GuiClick extends GuiScreen {

    /**
     * The theme of the clickable menu.
     */
    private Theme theme = new EraTheme();

    /**
     * The collection of panels.
     */
    private List<Panel> panels;


    /**
     * Called to setup the clickable menu.
     */
    public void setup() {
        // Creates a local instance of the beginning x and y coordinates of the panels.
        int x = 2;
        int y = 2;

        // Instantiates the panel collection.
        this.panels = new CopyOnWriteArrayList<>();

        // Iterates over all the categories of modules.
        for (Category category : Category.values()) {
            // Checks for a specific category.
            if (category == Category.NONE) {
                continue;
            }

            // Registers a panel into the panel collection.
            this.panels.add(new Panel(this, category.getLabel(), x, y, this.theme.getWidth(), this.theme.getHeight()) {
                @Override
                protected void setup() {
                    // Iterates through all the modules in the category.
                    Era.INSTANCE.getModuleManager().registry().stream().filter(module -> module instanceof Toggleable && module.getCategory() == category).forEach(module -> {
                        // Checks if the module is the ClickGUI module.
                        if (module != Era.INSTANCE.getModuleManager().get(ClickGUI.class)) {
                            // Adds a new component to the registry of components in the panel.
                            this.registry().add(new ModuleButton(GuiClick.this, (Toggleable) module, this.getX(), this.getY(), this.getWidth(), this.getHeight()));
                        }
                    });
                }
            });

            // Increments the x coordinate of the panel.
            x += this.theme.getWidth() + 2;
        }
    }

    @Override
    public void drawScreen(int x, int y, float partialTicks) {
        // Iterates through all the panels and draws them.
        this.panels.forEach(panel -> panel.drawPanel(x, y));
    }

    @Override
    public void mouseClicked(int x, int y, int button) {
        // Iterates through all the panels and handles the mouse click event.
        this.panels.forEach(panel -> panel.mouseClicked(x, y, button));
    }

    @Override
    public void mouseReleased(int x, int y, int button) {
        // Iterates through all the panels and handles the mouse release event.
        this.panels.forEach(panel -> panel.mouseReleased(x, y, button));
    }

    @Override
    public void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);

        // Iterates through all the panels and handles the key typing event.
        this.panels.forEach(panel -> panel.keyTyped(typedChar, keyCode));
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    /**
     * Returns the theme of the clickable menu.
     *
     * @return theme of the clickable menu
     */
    public Theme getTheme() {
        return theme;
    }

    /**
     * Returns the collection of panels in the clickable menu.
     *
     * @return collection of panels in the clickable menu
     */
    public List<Panel> getPanels() {
        return panels;
    }
}
