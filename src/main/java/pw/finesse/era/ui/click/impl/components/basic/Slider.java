package pw.finesse.era.ui.click.impl.components.basic;

import pw.finesse.era.ui.click.api.component.Component;
import pw.finesse.era.ui.click.impl.GuiClick;

/**
 * @author Marc
 * @since 11/12/2016
 */
public class Slider extends Component {

    /**
     * The value of the slider.
     */
    private Number value;

    /**
     * The minimum and maximum values of the slider.
     */
    private double min, max;

    /**
     * The completion of the slider, i.e., how far along the slider is.
     */
    private double completion;

    /**
     * Whether the slider is being dragged.
     */
    private boolean dragging;

    public Slider(GuiClick parent, String label, Number value, double min, double max, int x, int y, int width, int height) {
        super(parent, label, x, y, width, height);

        this.value = value;
        this.min = min;
        this.max = max;
    }

    @Override
    public void mouseClicked(int x, int y, int button) {
        // Checks if the user is hovering over the slider.
        if(this.isHovered(x, y)) {
            // Checks if the button is a left click.
            if(button == 0) {
                // Sets the dragging state of the slider.
                this.dragging = true;
            }
        }
    }

    @Override
    public void mouseReleased(int x, int y, int button) {
        // Sets the dragging state of the slider.
        this.dragging = false;
    }
    /**
     * Returns the value of the slider.
     *
     * @return value of the slider
     */
    public Number getValue() {
        return value;
    }

    /**
     * Sets the value of the slider to the specified value.
     *
     * @param value value to set
     */
    public void setValue(Number value) {
        this.value = value;
    }

    /**
     * Returns the minimum value of the slider.
     *
     * @return minimum value of the slider
     */
    public double getMin() {
        return min;
    }

    /**
     * Returns the minimum value of the slider to the specified value.
     *
     * @param min minimum value to set
     */
    public void setMin(double min) {
        this.min = min;
    }

    /**
     * Returns the maximum value of the slider.
     *
     * @return maximum value of the slider
     */
    public double getMax() {
        return max;
    }

    /**
     * Sets the maximum value of the slider to the specified value.
     *
     * @param max maximum value to set
     */
    public void setMax(double max) {
        this.max = max;
    }

    /**
     * Returns the completion of the slider.
     *
     * @return completion of the slider
     */
    public double getCompletion() {
        return completion;
    }

    /**
     * Sets the completion of the slider to the specified completion.
     *
     * @param completion completion to set
     */
    public void setCompletion(double completion) {
        this.completion = completion;
    }

    /**
     * Returns {@code true} if the slider is being dragged, otherwise {@code false}.
     *
     * @return {@code true} if the slider is being dragged, otherwise {@code false}
     */
    public boolean isDragging() {
        return dragging;
    }

    /**
     * Sets the dragging state of the slider to the specified state.
     *
     * @param dragging state to set
     */
    public void setDragging(boolean dragging) {
        this.dragging = dragging;
    }
}
