package pw.finesse.era.ui.click.impl.components.basic;

import pw.finesse.commons.interfaces.Toggleable;
import pw.finesse.era.ui.click.api.component.Component;
import pw.finesse.era.ui.click.impl.GuiClick;

/**
 * @author Marc
 * @since 11/12/2016
 */
public class Button extends Component implements Toggleable {

    /**
     * The enabled state of the button.
     */
    private boolean enabled;

    /**
     * Whether the user was hovered over the button before releasing the mouse.
     */
    private boolean wasHovered;

    public Button(GuiClick parent, String label, boolean enabled, int x, int y, int width, int height) {
        super(parent, label, x, y, width, height);

        this.enabled = enabled;
    }

    @Override
    public void mouseClicked(int x, int y, int button) {
        // Checks if the mouse is hovered over the component.
        if(this.isHovered(x, y)) {
            // Checks if the button is a left click.
            if(button == 0) {
                this.wasHovered = true;
            }
        }
    }

    @Override
    public void mouseReleased(int x, int y, int button) {
        // Checks if the mouse is hovered over the component.
        if(this.isHovered(x, y)) {
            // Checks if the button is a left click.
            if(button == 0) {
                // Checks if the user clicked initially before releasing the mouse.
                if(this.wasHovered) {
                    // Toggles the button.
                    this.toggle();

                    // Resets the hovered state.
                    this.wasHovered = false;
                }
            }
        }
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void toggle() {
        this.setEnabled(!this.isEnabled());
    }
}
