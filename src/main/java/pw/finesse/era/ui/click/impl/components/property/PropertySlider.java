package pw.finesse.era.ui.click.impl.components.property;

import pw.finesse.era.ui.click.impl.GuiClick;
import pw.finesse.era.ui.click.impl.components.basic.Slider;

import java.lang.reflect.Field;

/**
 * @author Marc
 * @since 11/12/2016
 */
public class PropertySlider extends Slider {

    /**
     * The field of the property.
     */
    private Field field;

    /**
     * The object of which the field is to be extracted.
     */
    private Object object;

    public PropertySlider(GuiClick parent, String label, Field field, Object object, double min, double max, int x, int y, int width, int height) throws IllegalAccessException {
        super(parent, label, (Number) field.get(object), min, max, x, y, width, height);

        this.field = field;
        this.object = object;
    }

    @Override
    public Number getValue() {
        try {
            return (Number) this.field.get(this.object);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public void setValue(Number value) {
        try {
            this.field.set(object, value);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the field of the property.
     *
     * @return field of the property
     */
    public Field getField() {
        return field;
    }

    /**
     * Returns the object from which the field is extracted.
     *
     * @return object from which the field is extracted
     */
    public Object getObject() {
        return object;
    }
}
