package pw.finesse.era.ui.tab;

import net.minecraft.client.gui.Gui;
import pw.finesse.commons.interfaces.Labelled;
import pw.finesse.commons.management.impl.ListManager;
import pw.finesse.era.core.Era;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.impl.visuals.TabbedMenu;
import pw.finesse.era.module.impl.visuals.tabbedmenu.GuiTabbedMenu;
import pw.finesse.era.util.RenderUtil;

import java.util.ArrayList;
import java.util.Collections;

import static org.lwjgl.input.Keyboard.*;

/**
 * A tab that holds a category and
 * all of the modules in it. This is used
 * in the {@link TabbedMenu} module.
 *
 * @author Marc
 * @since 8/18/2016
 */
public class GuiTab extends ListManager<GuiTabButton> implements Labelled {

    /**
     * The category that the tab holds.
     */
    private Category category;

    /**
     * Whether the tab is extended/open.
     */
    private boolean open;

    /**
     * The index of the selected button.
     */
    private int index;


    public GuiTab(Category category) {
        super(new ArrayList<>());

        this.category = category;
    }

    /**
     * Called to draw the tab.
     *
     * @param x       x coordinate to draw at
     * @param y       y coordinate to draw at
     * @param width   width of the tab
     * @param height  height of the tab
     * @param hovered whether the tab is hovered
     */
    public void draw(int x, int y, int width, int height, boolean hovered) {
        // Checks if the category is being hovered.
        if (hovered) {
            RenderUtil.drawBorderedRect(x, y, width, height, 1F, GuiTabbedMenu.HOVERED_COLOR, GuiTabbedMenu.BORDER_COLOR);
            //Gui.drawRect(x, y, width, height, 0xE638f274);
        } else {
            Gui.drawRect(x, y, width, height, GuiTabbedMenu.INTERIOR_COLOR);
        }

        // Draws the title.
        Era.INSTANCE.getMinecraft().fontRendererObj.drawStringWithShadow(this.getLabel(), x + 2, y + 2, hovered ? 0xFFF0F0F0 : 0xFFD6D6D6);

        // Checks if the tab is open.
        if (this.isOpen()) {
            // Creates a local instance of the offset.
            int offset = 0;

            // Creates a local instance of the width of the menu.
            int menuWidth = 0;

            // Iterates through the collection of buttons.
            for (GuiTabButton button : this.registry) {
                // Creates a local instance of the width of the string.
                int stringWidth = Era.INSTANCE.getMinecraft().fontRendererObj.getStringWidth(button.getLabel());

                if (stringWidth < menuWidth) {
                    continue;
                }

                menuWidth = stringWidth + 4;
            }

            // Iterates through the collection of buttons.
            for (GuiTabButton button : this.registry) {
                // Draws the button.
                button.draw(x + width, y + offset, x + width + menuWidth, 12, this.registry.get(index) == button);

                // Increments the offset.
                offset += 12;
            }

            // Draws a hollow rectangle around the menu.
            RenderUtil.drawHollowRect(x + width, y, x + width + menuWidth, y + offset, 1F, GuiTabbedMenu.BORDER_COLOR);

            // Sorts the list of buttons alphabetically.
            Collections.sort(this.registry, (first, second) -> first.getLabel().compareTo(second.getLabel()));
        }
    }

    /**
     * Called to handle the key input of the tab.
     *
     * @param key key code
     */
    public void keyInput(int key) {
        // Checks if the tab is extended before recording key input.
        if (this.open) {
            switch (key) {
                case KEY_UP: {
                    // Decrements the index.
                    this.index--;

                    // Makes sure the index doesn't go out of bounds.
                    if (this.index < 0) {
                        this.index = this.registry.size() - 1;
                    }
                    break;
                }

                case KEY_DOWN: {
                    // Increments the index.
                    this.index++;

                    // Makes sure the index doesn't go out of bounds.
                    if (this.index > this.registry.size() - 1) {
                        this.index = 0;
                    }
                    break;
                }

                case KEY_RETURN:
                case KEY_RIGHT:
                    // Checks if the registry is empty.
                    if (!this.isEmpty()) {
                        // Toggles the module at the current selected tab.
                        this.registry.get(index).toggle();
                    }
                    break;
            }
        }
    }


    @Override
    public String getLabel() {
        return category.getLabel();
    }

    /**
     * Returns the category that the tab holds.
     *
     * @return category that the tab holds
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Returns {@code true} if the tab is opened, otherwise {@code false}.
     *
     * @return {@code true} if the tab is opened, otherwise {@code false}
     */
    public boolean isOpen() {
        return open;
    }

    /**
     * Sets the opened state of the tab to the specified state.
     *
     * @param open state to set
     */
    public void setOpen(boolean open) {
        this.open = open;
    }

}
