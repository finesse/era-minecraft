package pw.finesse.era.ui.tab;

import net.minecraft.client.gui.Gui;
import pw.finesse.commons.interfaces.Labelled;
import pw.finesse.era.core.Era;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.module.impl.visuals.tabbedmenu.GuiTabbedMenu;
import pw.finesse.era.util.RenderUtil;

/**
 * A tab button that contains a {@link Module}
 * and is able to modify the state of the button.
 *
 * @author Marc
 * @since 8/18/2016
 */
public class GuiTabButton implements Labelled, pw.finesse.commons.interfaces.Toggleable {

    /**
     * The toggleable module of the button.
     */
    private Toggleable module;

    public GuiTabButton(Toggleable module) {
        this.module = module;
    }

    /**
     * Called to draw the button.
     *
     * @param x       x coordinate to draw at
     * @param y       y coordinate to draw at
     * @param width   width of the button
     * @param height  height of the button
     * @param hovered whether the button is hovered
     */
    public void draw(int x, int y, int width, int height, boolean hovered) {
        // Checks if the module is being hovered.
        if (hovered) {
            RenderUtil.drawBorderedRect(x, y, width, y + height, 1F, GuiTabbedMenu.HOVERED_COLOR, GuiTabbedMenu.BORDER_COLOR);
        } else {
            Gui.drawRect(x, y, width, y + height, GuiTabbedMenu.INTERIOR_COLOR);
        }

        // Draws the label of the module.
        Era.INSTANCE.getMinecraft().fontRendererObj.drawStringWithShadow(this.getLabel(), x + 2, y + 2, this.isEnabled() ? 0xFFEEEEEE : 0xFF999999);
    }

    /**
     * Returns the toggleable module of the button.
     *
     * @return toggleable module of the button
     */
    public Toggleable getddModule() {
        return this.module;
    }

    @Override
    public String getLabel() {
        return this.module.getLabel();
    }

    @Override
    public boolean isEnabled() {
        return this.module.isEnabled();
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.module.setEnabled(enabled);
    }

    @Override
    public void toggle() {
        this.module.toggle();
    }
}
