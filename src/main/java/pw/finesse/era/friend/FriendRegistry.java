package pw.finesse.era.friend;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import pw.finesse.commons.management.impl.ListManager;
import pw.finesse.era.core.Era;

import java.io.*;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Marc
 * @since 8/7/2016
 */
public class FriendRegistry extends ListManager<Friend> {

    /**
     * The directory where the files of the manager are saved.
     */
    public final File DIRECTORY = new File(Era.INSTANCE.getDirectory(), "friends.json");

    public FriendRegistry() {
        super(new CopyOnWriteArrayList<>());

        // Loads the friends list.
        this.load(DIRECTORY);

        // Saves the friends list.
        this.save(DIRECTORY);
    }

    /**
     * Called when the container is loaded.
     *
     * @param source source to load from
     */
    public void load(File source) {
        // Checks if the file exists.
        if (!source.exists()) {
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(source))) {
            // Creates a local Json array.
            JsonArray array = new JsonParser().parse(reader).getAsJsonArray();
            // Checks if the reader is successfully parsed.
            if (!array.isJsonArray()) {
                return;
            }

            // Iterates through the array.
            array.forEach(element -> {
                // Creates a local Json object.
                JsonObject object = element.getAsJsonObject();

                // Checks if the Json object has the required properties.
                if (object.has("username") && object.has("alias")) {
                    // Registers a new friend from the file.
                    this.register(new Friend(object.get("username").getAsString(), object.get("alias").getAsString()));
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when the container is saved.
     *
     * @param destination destination to save to
     */
    public void save(File destination) {
        // Checks if the file exists.
        if (!destination.exists()) {
            try {
                // Creates the necessary file.
                if (!destination.createNewFile()) {
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Creates a local Json array.
        JsonArray array = new JsonArray();

        // Creates a local Json object.
        JsonObject object;

        // Iterates through the contents of the container.
        for (Friend friend : this.registry) {
            // Instantiates the local Json object.
            object = new JsonObject();

            // Adds the friend username as a property.
            object.addProperty("username", friend.getUsername());

            // Adds the friend alias as a property.
            object.addProperty("alias", friend.getAlias());

            // Adds the object to the array.
            array.add(object);
        }

        try (FileWriter writer = new FileWriter(destination)) {
            // Writes the array to the file.
            writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(array));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Attempts to the find the specified {@link Friend} in the
     * friend registry. If the attempt is successful, the method
     * will return a {@link Friend} object. If the attempt fails,
     * the method will return an empty {@link Optional} object.
     *
     * @param username username to find
     * @return friend object
     */
    public Optional<Friend> get(String username) {
        // Iterates through the contents of the container.
        for (Friend friend : this.registry) {
            // Checks if the username matches the specified username.
            if (friend.getUsername().equalsIgnoreCase(username)) {
                return Optional.of(friend);
            }
        }

        return Optional.empty();
    }
}
