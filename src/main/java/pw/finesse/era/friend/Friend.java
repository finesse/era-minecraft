package pw.finesse.era.friend;

/**
 * The main friend class. This class
 * can be used to store user-names and
 * aliases of the user's friends.
 *
 * @author Marc
 * @since 8/2/2016
 */
public class Friend {

    /**
     * The Minecraft username of the friend.
     */
    private final String username;

    /**
     * The alias of the friend.
     */
    private String alias;

    public Friend(String username, String alias) {
        this.username = username;
        this.alias = alias;
    }

    public Friend(String username) {
        this.username = username;
        this.alias = username;
    }

    /**
     * Returns the Minecraft username of the friend.
     *
     * @return username of the friend
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the alias of the friend.
     *
     * @return alias of the friend.
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the alias of the friend to the specified alias.
     *
     * @param alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }
}
