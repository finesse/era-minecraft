package pw.finesse.era.account.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import org.lwjgl.input.Keyboard;
import pw.finesse.era.account.Account;

import java.io.IOException;
import java.util.Optional;

/**
 * A GUI screen that allows the user to add an
 * account to the account manager.
 *
 * @author Daniel, Jack, Marc
 * @since Jul 18, 2015
 */
public final class GuiAddAccount extends GuiScreen {

    /**
     * An optional container of the account to be added.
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private Optional<Account> accountContainer = Optional.empty();

    /**
     * The parent GUI screen for the account slot.
     */
    private GuiAccountManager parent;

    /**
     * The text field for the account username.
     */
    private GuiTextField username;

    /**
     * The text field for the account password.
     */
    private GuiPasswordField password;

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public GuiAddAccount(GuiAccountManager parent, Optional<Account> accountContainer) {
        this.parent = parent;
        this.accountContainer = accountContainer;
    }

    @Override
    public void updateScreen() {
        // Updates the cursor counter for the username text field.
        this.username.updateCursorCounter();
        // Updates the cursor counter for the password text field.
        this.password.updateCursorCounter();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void initGui() {
        // Allow for the user to repeat events, i.e. holding down a key.
        Keyboard.enableRepeatEvents(true);
        // Clears the list of buttons.
        this.buttonList.clear();
        // Adds buttons the list of buttons.
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 114, this.accountContainer.isPresent() ? "Save" : "Add"));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 138, "Cancel"));
        // Creates a new instance of GuiTextField.
        this.username = new GuiTextField(0, this.fontRendererObj, this.width / 2 - 100, 66, 200, 20);
        // Sets the maximum string length.
        this.username.setMaxStringLength(1000);
        // Sets the text field to focused.
        this.username.setFocused(true);
        // Creates a new instance of GuiPasswordField.
        this.password = new GuiPasswordField(1, this.fontRendererObj, this.width / 2 - 100, 106, 200, 20);
        // Sets the maximum string length.
        this.password.setMaxStringLength(1000);
        // Checks if the account container is not null.
        if (this.accountContainer.isPresent()) {
            // Creates a local instance of the account.
            Account account = this.accountContainer.get();
            // Sets the text of the username field to the account's username.
            this.username.setText(account.getUsername());
            // Sets the text of the password field to the account's password.
            this.password.setText(account.getPassword());
        }

    }

    @Override
    public void onGuiClosed() {
        // Disable repeat keyboard events.
        Keyboard.enableRepeatEvents(false);
    }

    @Override
    public void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0: {
                // Checks if the account container is not null.
                if (this.accountContainer.isPresent()) {
                    // Creates a new instance of the account.
                    Account account = this.accountContainer.get();
                    // Sets the username of the account to the username field input.
                    account.setUsername(this.username.getText());
                    // Sets the password of the account to the password field input.
                    account.setPassword(this.password.getText());
                } else {
                    // Adds an account to the account manager.
                    this.parent.addAccount(new Account(this.username.getText(), this.password.getText()));
                }
                // Displays the parent GUI screen.
                this.mc.displayGuiScreen(this.parent);

                break;
            }

            case 1: {
                // Displays the parent GUI screen.
                this.mc.displayGuiScreen(this.parent);

                break;
            }
        }
    }

    @Override
    public void keyTyped(char typedChar, int keyCode) {
        // Processes key input from the username text field.
        this.username.textboxKeyTyped(typedChar, keyCode);

        // Processes key input from the password text field.
        this.password.textboxKeyTyped(typedChar, keyCode);

        // Checks if the key code matches the key code of TAB.
        if (keyCode == Keyboard.KEY_TAB) {
            // Checks if either of the text fields are focused.
            if (!this.username.isFocused() && !this.password.isFocused()) {
                // Sets the focus of the username text field to true.
                this.username.setFocused(true);

                return;
            } else if (this.username.isFocused() && this.password.isFocused()) {
                // Sets the username text field in focus.
                this.username.setFocused(true);
                // Sets the password text field out of focus.
                this.password.setFocused(false);

                return;
            }
            // Sets the focus of the username text field to the opposite focus.
            this.username.setFocused(!this.username.isFocused());
            // Sets the focus of the password text field to the opposite focus.
            this.password.setFocused(!this.password.isFocused());
        }
        // Checks if the key code is the key code of "return".
        if (keyCode == Keyboard.KEY_RETURN) {
            // Executes the action of the button in the first index of the button list.
            this.actionPerformed((GuiButton) this.buttonList.get(0));
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        // Handles the mouse clicking for the superclass.
        super.mouseClicked(mouseX, mouseY, mouseButton);
        // Handles the mouse clicking for the username text field.
        this.username.mouseClicked(mouseX, mouseY, mouseButton);
        // Handles the mouse clicking for the password text field.
        this.password.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // Draws the default background.
        this.drawDefaultBackground();
        // Whether the text fields are populated.
        boolean isPopulated = this.password.getText().length() > 0 && this.username.getText().length() > 0;
        // Gets the button at the first index of the button list.
        GuiButton guiButton = (GuiButton) this.buttonList.get(0);
        // Sets the enabled state of the button to whether the text fields are populated.
        guiButton.enabled = isPopulated;
        // Draws a centered title string.
        this.drawCenteredString(this.fontRendererObj, this.accountContainer.isPresent() ? "Edit Account" : "Add Account", this.width / 2, 17, 16777215);
        // Draws a title above the username text field.
        this.drawString(this.fontRendererObj, "Username", this.width / 2 - 100, 53, 10526880);
        // Draws a title above the password text field.
        this.drawString(this.fontRendererObj, "Password", this.width / 2 - 100, 94, 10526880);
        // Draws the username text field.
        this.username.drawTextBox();
        // Draws the password text field.
        this.password.drawTextBox();
        // Handles the drawing for the superclass.
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
}