package pw.finesse.era.account.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.gui.GuiYesNoCallback;
import net.minecraft.client.resources.I18n;
import org.lwjgl.input.Keyboard;
import pw.finesse.era.account.Account;
import pw.finesse.era.account.yggdrasil.DisplayPayload;
import pw.finesse.era.account.yggdrasil.YggdrasilPayload;
import pw.finesse.era.core.Era;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

/**
 * The main GUI screen for the account manager.
 *
 * @author Daniel, Jack, Marc
 * @since Jul 18, 2015
 */
public final class GuiAccountManager extends GuiScreen implements GuiYesNoCallback {

    /**
     * An optional container for the current status of the account manager.
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private Optional<String> status = Optional.empty();

    /**
     * A container for the accounts.
     */
    private GuiAccountSlot accountSlot;

    /**
     * A button used to log in.
     */
    protected GuiButton loginButton;

    /**
     * A button used to edit account data.
     */
    protected GuiButton editButton;

    /**
     * A button used to delete account data;
     */
    protected GuiButton deleteButton;

    /**
     * The parent GUI screen for the account manager.
     */
    protected GuiScreen parent;

    /**
     * The index of the currently selected account slot.
     */
    private int slotIndex = -1;

    /**
     * Whether an account is being deleted.
     */
    private boolean isDeleting;

    public GuiAccountManager(GuiScreen parent) {
        this.parent = parent;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void initGui() {
        // Load the accounts from the file.
        Era.INSTANCE.getAccountRegistry().load();
        // Allow for the user to repeat events, i.e. holding down a key.
        Keyboard.enableRepeatEvents(true);
        // Clears the list of buttons.
        this.buttonList.clear();
        // Creates a new instance of the account slots.
        this.accountSlot = new GuiAccountSlot(this, this.mc, this.width, this.height, 32, this.height - 64, 36);
        // Adds buttons to the list of buttons.
        this.accountSlot = new GuiAccountSlot(this, this.mc, this.width, this.height, 32, this.height - 64, 36);
        this.buttonList.add(new GuiButton(0, this.width / 2 + 80, this.height - 28, 70, 20, "Back"));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 76, this.height - 52, 70, 20, "Direct Login"));
        this.buttonList.add(this.editButton = new GuiButton(2, this.width / 2 - 154, this.height - 28, 70, 20, "Edit"));
        this.buttonList.add(this.deleteButton = new GuiButton(3, this.width / 2 - 76, this.height - 28, 70, 20, "Delete"));
        this.buttonList.add(this.loginButton = new GuiButton(4, this.width / 2 - 154, this.height - 52, 70, 20, "Login"));
        this.buttonList.add(new GuiButton(5, this.width / 2 + 80, this.height - 52, 70, 20, "Add"));
        this.buttonList.add(new GuiButton(6, this.width / 2 + 2, this.height - 28, 70, 20, "Refresh"));
        this.buttonList.add(new GuiButton(7, this.width / 2 + 2, this.height - 52, 70, 20, "Import"));
    }

    @Override
    public void onGuiClosed() {
        // Make sure to disable what we enabled on initialization.
        Keyboard.enableRepeatEvents(false);
        // Save the accounts to the file.
        Era.INSTANCE.getAccountRegistry().save();
    }

    @Override
    public void actionPerformed(GuiButton guiButton) throws IOException {
        switch (guiButton.id) {
            case 0: {
                // Display the parent screen.
                this.mc.displayGuiScreen(this.parent);

                break;
            }

            case 1: {
                // Display the direct log in screen.
                this.mc.displayGuiScreen(new GuiDirectLogin(this));

                break;
            }

            case 2: {
                // Display the add account screen with the selected account index.
                this.mc.displayGuiScreen(new GuiAddAccount(this, Optional.of(this.getSelectedAccount())));

                break;
            }

            case 3: {
                // Set the deleting state to true and display a yes or no screen.
                this.isDeleting = true;
                // Creates a local string defining the confirmation message.
                String confirmation = I18n.format("Are you sure to want to delete this account?");
                // A new instance of the yes or no screen.
                GuiYesNo guiYesNo = new GuiYesNo(this, confirmation, "\"" + this.getSelectedAccount().getDisplayName() + "\" " + I18n.format("selectServer.deleteWarning"), "Delete account", "Cancel", 0) {
                    @Override
                    public void actionPerformed(GuiButton button) throws IOException {
                        super.actionPerformed(button);

                        switch (button.id) {
                            case 0: {
                                // Removes the selected account from the account collection.
                                GuiAccountManager.this.getAccounts().remove(GuiAccountManager.this.getSelectedAccount());

                                break;
                            }

                            case 1: {
                                // Displays the parent GUI screen.
                                this.mc.displayGuiScreen(new GuiAccountManager(null));

                                break;
                            }
                        }
                    }
                };

                // Displays the yes or no screen.
                this.mc.displayGuiScreen(guiYesNo);

                break;
            }

            case 4: {
                // Creates a new instance of the Yggdrasil payload and runs it.
                new YggdrasilPayload(this, this.getSelectedAccount()).run(this.mc);

                break;
            }

            case 5: {
                // Displays the add account screen.
                this.mc.displayGuiScreen(new GuiAddAccount(this, Optional.empty()));

                break;
            }

            case 6: {
                // Displays the main account manager screen.
                this.mc.displayGuiScreen(this);

                break;
            }

            case 7: {
                // Displays the import account screen.
                this.importAccounts();

                // Displays the main account manager screen.
                this.mc.displayGuiScreen(this);

                break;
            }
        }
    }

    @Override
    public void confirmClicked(boolean result, int id) {
        // Checks if the account is being deleted.
        if (this.isDeleting) {
            // Checks if the click is a left click.
            if (id == 0) {
                // Sets the deleting state to false.
                this.isDeleting = false;
                // Checks if the specified boolean result is true.
                if (result) {
                    // Removes the selected account.
                    Era.INSTANCE.getAccountRegistry().register(this.getSelectedAccount());
                    // Saves the accounts to a file.
                    Era.INSTANCE.getAccountRegistry().save();
                    // Displays the main account manager screen.
                    this.mc.displayGuiScreen(this);
                }
            }
        }
    }

    @Override
    public void keyTyped(char typedChar, int keyCode) throws IOException {
        // Checks if the key code is the key code of "return".
        if (keyCode == Keyboard.KEY_RETURN) {
            // Executes the action of the button in the first index of the button list.
            this.actionPerformed((GuiButton) this.buttonList.get(0));
        }
    }

    @Override
    public void handleMouseInput() throws IOException {
        // Handles the mouse input for the superclass.
        super.handleMouseInput();
        // Handles the mouse input for the account slot.
        this.accountSlot.handleMouseInput();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        // Allows the button to be clicked if an account is selected.
        this.editButton.enabled = slotIndex != -1;
        this.deleteButton.enabled = slotIndex != -1;
        this.loginButton.enabled = slotIndex != -1;
        // Draws the default background.
        this.drawDefaultBackground();
        // Draws the account slot.
        this.accountSlot.drawScreen(mouseX, mouseY, partialTicks);
        // Draws a centered string, displaying the title of the screen, including the number of accounts in the manager.
        this.drawCenteredString(this.fontRendererObj, String.format("Account Manager (%s accounts)", Era.INSTANCE.getAccountRegistry().size()), this.width / 2, this.status.isPresent() ? 7 : 17, 16777215);
        // Checks if the status is not null.
        if (this.status.isPresent()) {
            // Draws the status of the account manager.
            this.drawCenteredString(this.fontRendererObj, this.status.get(), this.width / 2, 17, 16777215);
        }
        // Handles drawing for the superclass.
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    /**
     * Called to import a text file of accounts in standard format.
     *
     * @author nuf, Jack
     */
    private void importAccounts() {
        // Creates a new instance of JFileChooser.
        JFileChooser jFileChooser = new JFileChooser();
        // Sets the visibility of the file chooser to true.
        jFileChooser.setVisible(true);
        // Sets the size of the file chooser.
        jFileChooser.setSize(500, 500);
        // Prevents the file chooser from using the "AcceptAll FileFilter."
        jFileChooser.setAcceptAllFileFilterUsed(false);
        // Filters out all files except for text files (txt).
        jFileChooser.setFileFilter(new FileNameExtensionFilter("File", "txt"));
        // Creates a new JFrame.
        JFrame jFrame = new JFrame("Select a file");

        // Adds an action listener.
        jFileChooser.addActionListener(action -> {
            // Creates an optional container of the file.
            Optional<File> fileContainer = Optional.ofNullable(jFileChooser.getSelectedFile());

            // Checks if the file is present.
            if (fileContainer.isPresent()) {
                // Checks if the selected file is approved.
                if (action.getActionCommand().equals("ApproveSelection")) {
                    // Tries to create a new instance of scanner.
                    try (Scanner scanner = (new Scanner(new FileReader(fileContainer.get()))).useDelimiter("\n")) {
                        // Checks if the scanner has more input.
                        while (scanner.hasNext()) {
                            // The split between the account and password.
                            String[] split = scanner.next().trim().split(":");
                            // Registers the account.
                            Era.INSTANCE.getAccountRegistry().register(new Account(split[0], split[1]));
                        }

                        // Closes the scanner.
                        scanner.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Saves the accounts to the file.
                    Era.INSTANCE.getAccountRegistry().save();
                    // Closes the frame and disposes of it.
                    jFrame.setVisible(false);
                    jFrame.dispose();
                }

                // Checks if the selected file is cancelled.
                if (action.getActionCommand().equals("CancelSelection")) {
                    // Closes the frame and disposes of it.
                    jFrame.setVisible(false);
                    jFrame.dispose();
                }
            }
        });

        // Sets the focused state of the frame to true, to make sure it's always on top.
        jFrame.setAlwaysOnTop(true);
        // Adds the file chooser component to the frame.
        jFrame.add(jFileChooser);
        // Sets the frame's visibility to true.
        jFrame.setVisible(true);
        // Sets the size of the frame.
        jFrame.setSize(750, 700);
    }

    /**
     * Returns the currently selected account using the slot index.
     *
     * @return currently selected account
     */
    protected Account getSelectedAccount() {
        return this.getAccounts().get(this.slotIndex);
    }

    /**
     * Sets the status of the account manager to the specified status.
     *
     * @param status status to set
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public void setStatus(Optional<String> status) {
        this.status = status;
    }

    /**
     * Adds an account to the account manager.
     *
     * @param account account to add
     */
    void addAccount(Account account) {
        // Adds an account to the account manager.
        Era.INSTANCE.getAccountRegistry().register(account);

        // Creates a new instance of the display payload and runs it.
        new DisplayPayload(account).run();

        // Saves the accounts to the file.
        Era.INSTANCE.getAccountRegistry().save();
    }

    /**
     * Returns the list of accounts in the account manager.
     *
     * @return list of accounts
     */
    @SuppressWarnings("unchecked")
    public List<Account> getAccounts() {
        return (List<Account>) Era.INSTANCE.getAccountRegistry().registry();
    }

    /**
     * Returns the index of the slot.
     *
     * @return index of the slot
     */
    public int getSlotIndex() {
        return slotIndex;
    }

    /**
     * Sets the index of the slot to the specified index.
     *
     * @param slotIndex index to set
     */
    public void setSlotIndex(int slotIndex) {
        this.slotIndex = slotIndex;
    }
}