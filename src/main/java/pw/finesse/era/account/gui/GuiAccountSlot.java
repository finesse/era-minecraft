package pw.finesse.era.account.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.GuiSlot;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.util.ResourceLocation;
import pw.finesse.era.account.Account;
import pw.finesse.era.account.yggdrasil.YggdrasilPayload;

/**
 * Creates a slot for an
 * <p>
 * Creates a slot for an account in the account
 * manager screen. This can be used to log in
 * to the account.
 *
 * @author Daniel, Jack, Marc
 * @since Jul 18, 2016
 */
public final class GuiAccountSlot extends GuiSlot {

    /**
     * The parent GUI screen for the account slot.
     */
    private GuiAccountManager parent;

    public GuiAccountSlot(GuiAccountManager parent, Minecraft mcIn, int width, int height, int topIn, int bottomIn, int slotHeightIn) {
        super(mcIn, width, height, topIn, bottomIn, slotHeightIn);
        this.parent = parent;
    }

    @SuppressWarnings("ReplaceAllDot")
    @Override
    public void drawSlot(int index, int posX, int posY, int contentY, int mouseX, int mouseY) {
        // Creates a local instance of the account.
        Account account = this.parent.getAccounts().get(index);
        // Gets the skin of the player.
        ThreadDownloadImageData threadDownloadImageData = AbstractClientPlayer.getDownloadImageSkin(new ResourceLocation(account.getDisplayName() + ".png"), account.getDisplayName());
        // Binds the texture of the skin.
        GlStateManager.bindTexture(threadDownloadImageData.getGlTextureId());
        // Draws a textured rectangle.
        this.parent.drawTexturedModalRect(posX, posY, 32, 32, 32, 32);
        // Draws the username and password of the account.
        this.mc.fontRendererObj.drawStringWithShadow(account.getDisplayName(), posX + 36, posY + 1, -1);
        this.mc.fontRendererObj.drawStringWithShadow(account.getPassword().replaceAll(".", "*"), posX + 36, posY + 12, 0xB8808080);
    }

    @Override
    protected void elementClicked(int slotIndex, boolean isDoubleClick, int mouseX, int mouseY) {
        // Sets the slot index to the specified index.
        this.parent.setSlotIndex(slotIndex);
        // Whether the slot index is above or equal to 0 and less than the total size of the slots.
        boolean isWithinRange = this.parent.getSlotIndex() >= 0 && this.parent.getSlotIndex() < this.getSize();
        // Sets the enabled state of the edit button to whether the slot is within range.
        this.parent.editButton.enabled = isWithinRange;
        // Sets the enabled state of the delete button to whether the slot is within range.
        this.parent.deleteButton.enabled = isWithinRange;
        // Sets the enabled state of the login button to whether the slot is within range.
        this.parent.loginButton.enabled = isWithinRange;
        // Checks if the click is a double click.
        if (isDoubleClick) {
            // Checks if the slot clicked is within range.
            if (isWithinRange) {
                // Creates a new instance of the Yggdrasil payload and runs it.
                new YggdrasilPayload(this.parent, this.parent.getSelectedAccount()).run(this.mc);
            }
        }
    }

    @Override
    protected int getSize() {
        return this.parent.getAccounts().size();
    }

    @Override
    protected void drawBackground() {
        this.parent.drawDefaultBackground();
    }

    @Override
    protected boolean isSelected(int slotIndex) {
        return slotIndex == this.parent.getSlotIndex();
    }
}