package pw.finesse.era.account.yggdrasil;

import com.mojang.authlib.Agent;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;
import net.minecraft.client.Minecraft;
import net.minecraft.util.Session;
import pw.finesse.era.account.Account;
import pw.finesse.era.account.gui.GuiAccountManager;

import java.lang.reflect.Field;
import java.net.Proxy;
import java.util.Optional;
import java.util.UUID;

import static net.minecraft.util.EnumChatFormatting.*;

/**
 * Creates a payload to login to an account using the
 * specified username and password. This is done using
 * Mojang's authentication library and reflection to set the
 * {@link Minecraft#session} field.
 *
 * @author Daniel, Jack
 * @since Jul 18, 2015
 */
public final class YggdrasilPayload {

    /**
     * The parent GUI screen for the account manager.
     */
    private GuiAccountManager parent;

    /**
     * The account to attempt to log in to.
     */
    private Account account;

    /**
     * A cached field of {@link Minecraft}'s session.
     */
    private Field sessionCache;

    public YggdrasilPayload(GuiAccountManager parent, Account account) {
        this.parent = parent;
        this.account = account;
    }

    /**
     * When called, this method will run the payload and log in.
     *
     * @param minecraft instance of {@link Minecraft}
     */
    public void run(Minecraft minecraft) {
        // Creates a new instance of Minecraft's authentication service.
        YggdrasilUserAuthentication authentication = new YggdrasilUserAuthentication(new YggdrasilAuthenticationService(Proxy.NO_PROXY, UUID.randomUUID().toString()), Agent.MINECRAFT);

        // Sets the username of the user to the account's username.
        authentication.setUsername(this.account.getUsername());

        // Sets the password of the user the account's password.
        authentication.setPassword(this.account.getPassword());

        try {
            // Logs in.
            authentication.logIn();

            // Checks if the session cache is null.
            if (this.sessionCache == null) {
                // Creates a new instance of the session.
                Field field = minecraft.getClass().getDeclaredField("session");

                // Sets the accessibility of the field to true.
                field.setAccessible(true);

                // Sets the session cache to the now accessible field.
                this.sessionCache = field;
            }

            // Creates a new session using the information from the Yggdrasil authentication.
            this.sessionCache.set(minecraft, new Session(authentication.getSelectedProfile().getName(), authentication.getSelectedProfile().getId().toString(), authentication.getAuthenticatedToken(), "legacy"));

            // Sets the status message in the parent GUI screen.
            this.parent.setStatus(Optional.of(YELLOW + "Logging in..." + RESET));
        } catch (AuthenticationException | NoSuchFieldException | IllegalAccessException exception) {
            // Sets the status message in the parent GUI screen.
            this.parent.setStatus(Optional.of(RED + String.format("Login failed. (%s)", this.account.getDisplayName() != null ? this.account.getDisplayName() : this.account.getUsername()) + RESET));

            return;
        }
        // Sets the status message in the parent GUI screen.
        this.parent.setStatus(Optional.of(GREEN + String.format("Logged in as '%s'.", authentication.getSelectedProfile().getName()) + RESET));
        // Sets the display name of the account.
        this.account.setDisplayName(authentication.getSelectedProfile().getName());

    }
}