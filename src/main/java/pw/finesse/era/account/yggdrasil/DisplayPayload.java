package pw.finesse.era.account.yggdrasil;

import com.mojang.authlib.Agent;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;
import pw.finesse.era.account.Account;

import java.net.Proxy;
import java.util.UUID;

/**
 * A class that creates a payload that is used to
 * check whether an account is able to be logged
 * into. If so, it sets the display name for the
 * account.
 *
 * @author Daniel, Jack
 * @since Jul 18, 2015
 */
public final class DisplayPayload {
    /**
     * The account to create a display for.
     */
    private final Account account;

    public DisplayPayload(Account account) {
        this.account = account;
    }

    /**
     * When called, this method will run the payload and
     * create a display for the specified account.
     */
    public void run() {
        // Creates a new instance of Minecraft's authentication service.
        YggdrasilUserAuthentication authentication = new YggdrasilUserAuthentication(new YggdrasilAuthenticationService(Proxy.NO_PROXY, UUID.randomUUID().toString()), Agent.MINECRAFT);

        // Sets the username of the user to the account's username.
        authentication.setUsername(this.account.getUsername());

        // Sets the password of the user the account's password.
        authentication.setPassword(this.account.getPassword());

        try {
            // Logs into the account.
            authentication.logIn();
        } catch (AuthenticationException exception) {
            exception.printStackTrace();
            return;
        }

        // Sets the display name of the account to the name of the selected profile.
        this.account.setDisplayName(authentication.getSelectedProfile().getName());
    }
}