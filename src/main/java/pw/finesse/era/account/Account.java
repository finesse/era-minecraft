package pw.finesse.era.account;

/**
 * The main account class. This class can
 * be used to store user-names and passwords
 * of the user's accounts.
 *
 * @author Marc
 * @since 7/26/2016
 */
public class Account {

    /**
     * The Minecraft user-name of the account.
     */
    private String username;

    /**
     * The password of the account.
     */
    private String password;

    /**
     * The display name of the account.
     */
    private String displayName;

    public Account(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Returns the username of the account.
     *
     * @return username of the account
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username of the account to the specified username.
     *
     * @param username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns the password of the account.
     *
     * @return password of the account
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password of the account to the specified password.
     *
     * @param password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns the display name of the account.
     *
     * @return display name of the account
     */
    public String getDisplayName() {
        if (displayName == null || displayName.isEmpty())
            return username;

        return displayName;
    }

    /**
     * Sets the display name of the account to the specified display name.
     *
     * @param displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
