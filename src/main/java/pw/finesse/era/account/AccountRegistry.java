package pw.finesse.era.account;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import pw.finesse.commons.management.impl.ListManager;
import pw.finesse.era.core.Era;

import java.io.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A container for all {@link Account}s.
 *
 * @author Marc
 * @since 7/26/2016
 */
public class AccountRegistry extends ListManager<Account> {

    /**
     * The directory where the files of the manager are saved.
     */
    private final File DIRECTORY = new File(Era.INSTANCE.getDirectory(), "accounts.json");

    public AccountRegistry() {
        super(new CopyOnWriteArrayList<>());
    }

    /**
     * Handles file loading for this container.
     */
    public void load() {
        // Checks if the file doesn't exist. If so, return.
        if (!DIRECTORY.exists())
            return;

        // Tries to create a new instance of a reader.
        try (BufferedReader reader = new BufferedReader(new FileReader(DIRECTORY))) {
            // Creates a new local of accounts from the json file.
            List<Account> accounts = new GsonBuilder().setPrettyPrinting().create().fromJson(reader, new TypeToken<List<Account>>() {
            }.getType());

            // Checks if the local list isn't empty.
            if (!accounts.isEmpty()) {
                // Clears the list of accounts.
                this.clear();

                // Iterates through the local list of accounts.
                accounts.forEach(account -> {
                    // Checks if the list of accounts doesn't already contain the account.
                    if (!this.contains(account)) {
                        // Adds the account.
                        this.register(account);
                    }
                });
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Handles file saving for this container.
     */
    public void save() {
        // Checks if the file doesn't exist.
        if (!DIRECTORY.exists()) {
            try {
                // Checks if the file hasn't been created yet, and returns until it has.
                if (!DIRECTORY.createNewFile()) {
                    return;
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        // Tries to create a new instance of a file writer.
        try (FileWriter writer = new FileWriter(DIRECTORY)) {
            // Writes the list of accounts to the specified file.
            writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(this.registry));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
