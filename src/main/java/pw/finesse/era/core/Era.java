package pw.finesse.era.core;

import net.minecraft.client.Minecraft;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.event.ListenerRegistry;
import pw.finesse.commons.logging.Logger;
import pw.finesse.era.account.AccountRegistry;
import pw.finesse.era.command.CommandManager;
import pw.finesse.era.events.system.ShutdownGameEvent;
import pw.finesse.era.events.system.StartGameEvent;
import pw.finesse.era.friend.FriendRegistry;
import pw.finesse.era.module.ModuleManager;
import pw.finesse.era.ui.click.impl.GuiClick;

import java.io.File;

/**
 * The main class of the Era modified cheat
 * client for version 1.8 of Minecraft.
 * <p>
 * I'd like to give a special thanks to those
 * who have helped me and continue to help me
 * throughout the development of this project.
 *
 * @author Marc
 * @since 8/2/2016
 */
public enum Era {

    /**
     * An instance of the class.
     */
    INSTANCE;

    /**
     * The directory of client, where files are saved.
     */
    private final File directory = new File(System.getProperty("user.home") + File.separator + LABEL);

    /**
     * The label of the client.
     */
    public static final String LABEL = "Era";

    /**
     * The build number of the client.
     */
    public static final int BUILD = 2;

    /**
     * An instance of {@link Logger}.
     */
    private Logger logger;

    /**
     * An instance of {@link Minecraft}.
     */
    private Minecraft minecraft;

    /**
     * An instance of the listener registry of the client.
     */
    private ListenerRegistry listenerRegistry;

    /**
     * An instance of the module manager of the client.
     */
    private ModuleManager moduleManager;

    /**
     * An instance of the command manager of the client.
     */
    private CommandManager commandManager;

    /**
     * An instance of the friend manager of the client.
     */
    private FriendRegistry friendRegistry;

    /**
     * An instance of the account manager of the client.
     */
    private AccountRegistry accountRegistry;

    /**
     * An instance of the clickable menu of the client.
     */
    private GuiClick guiClick;

    /**
     * Suppresses the default constructor, ensuring non-instantiability.
     */
    Era() {
        // Instantiates the listener registry of the client, then listens for the game initialization event.
        (this.listenerRegistry = new ListenerRegistry()).register(new Listener<StartGameEvent>() {
            @Override
            public void run(StartGameEvent event) {
                // Checks if the directory exists.
                if (!Era.INSTANCE.directory.exists()) {
                    // Creates the necessary directories.
                    if (!Era.INSTANCE.directory.mkdirs()) {
                        return;
                    }
                }

                // Instantiates the Minecraft instance.
                Era.INSTANCE.minecraft = event.getMinecraft();

                // Instantiates the module manager of the client.
                Era.INSTANCE.moduleManager = new ModuleManager();

                // Instantiates the command manager of the client.
                Era.INSTANCE.commandManager = new CommandManager();

                // Instantiates the friend registry of the client.
                Era.INSTANCE.friendRegistry = new FriendRegistry();

                // Instantiates the account registry of the client.
                Era.INSTANCE.accountRegistry = new AccountRegistry();

                // Instantiates the clickable menu of the client.
                (Era.INSTANCE.guiClick = new GuiClick()).setup();

                // Adds a new shutdown hook to be ran on the closing of the virtual machine.
                Runtime.getRuntime().addShutdownHook(new Thread("Era Shutdown Thread") {
                    @Override
                    public void run() {
                        // Fires the shutdown event.
                        Era.INSTANCE.listenerRegistry.fire(new ShutdownGameEvent());

                        // Iterates through the contents of the module container and saves all the modules.
                        Era.INSTANCE.moduleManager.registry().forEach(module -> module.save(Era.INSTANCE.moduleManager.DIRECTORY));

                        // Saves the friend container.
                        Era.INSTANCE.friendRegistry.save(Era.INSTANCE.friendRegistry.DIRECTORY);
                    }
                });
            }
        });
    }

    /**
     * Returns the directory of the client.
     *
     * @return directory of the client
     */
    public File getDirectory() {
        return directory;
    }

    /**
     * Returns the {@link Logger} instance.
     *
     * @return {@link Logger} instance
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * Returns the {@link Minecraft} instance.
     *
     * @return {@link Minecraft} instance
     */
    public Minecraft getMinecraft() {
        return minecraft;
    }

    /**
     * Returns the listener registry of the client.
     *
     * @return listener registry of the client
     */
    public ListenerRegistry getListenerRegistry() {
        return listenerRegistry;
    }

    /**
     * Returns the module manager of the client.
     *
     * @return module manager of the client
     */
    public ModuleManager getModuleManager() {
        return moduleManager;
    }

    /**
     * Returns the command manager of the client.
     *
     * @return command manager of the client
     */
    public CommandManager getCommandManager() {
        return commandManager;
    }

    /**
     * Returns the friend registry of the client.
     *
     * @return friend registry of the client
     */
    public FriendRegistry getFriendRegistry() {
        return friendRegistry;
    }

    /**
     * Returns the account registry of the client.
     *
     * @return account registry of the client
     */
    public AccountRegistry getAccountRegistry() {
        return accountRegistry;
    }

    /**
     * Returns the clickable menu of the client.
     *
     * @return clickable menu of the client
     */
    public GuiClick getGuiClick() {
        return guiClick;
    }
}
