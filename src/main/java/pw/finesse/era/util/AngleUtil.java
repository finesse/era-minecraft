package pw.finesse.era.util;

/**
 * A class containing static methods pertaining to angle calculations.
 *
 * @author Marc
 * @since 11/19/2016
 */
public class AngleUtil {

    /**
     * Suppresses the default constructor, ensuring non-instantiability.
     */
    private AngleUtil() {
    }

    /**
     * Returns the required angles needed to aim at the specified destination from the specified source.
     *
     * @param delta delta position
     * @return required angles needed to aim at the specified destination from the specified source
     */
    public static float[] calculateAngle(float[] delta) {
        // Creates a local instance of the hypotenuse of the angles.
        double hypot = Math.hypot(delta[0], delta[2]);
        // Translates the coordinates into angle degree values.
        float yaw = (float) (Math.toDegrees(Math.atan2(delta[2], delta[0]))) + 90F;
        float pitch = (float) -(Math.toDegrees(Math.atan2(delta[1], hypot)));

        return new float[]{yaw, pitch};
    }

    /**
     * Returns the required angles needed to aim at the specified destination from the specified source.
     *
     * @param source      source position
     * @param destination destination position
     * @return required angles needed to aim at the specified destination from the specified source
     */
    public static float[] calculateAngle(float[] source, float[] destination) {
        // Creates a local instance of the difference between the source and destination positions.
        float deltaX = source[0] - destination[0];
        float deltaY = source[1] - destination[1];
        float deltaZ = source[2] - destination[2];

        return calculateAngle(new float[]{deltaX, deltaY, deltaZ});
    }

}
