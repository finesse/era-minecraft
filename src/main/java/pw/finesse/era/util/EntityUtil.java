package pw.finesse.era.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;

import java.util.List;
import java.util.Optional;

/**
 * A class containing static methods pertaining to
 * finding, organizing and managing entities.
 *
 * @author Marc
 * @since 8/6/2016
 */
public class EntityUtil {

    /**
     * An instance of {@link Minecraft}.
     */
    private static Minecraft minecraft = Minecraft.getMinecraft();

    /**
     * Suppresses the default constructor, ensuring non-instantiability.
     */
    private EntityUtil() {
    }

    /**
     * Returns the best {@link EntityLivingBase} target for the player.
     *
     * @param player   player instance
     * @param angle    angle to check within
     * @param distance distance to check within
     * @return best {@link EntityLivingBase} target for the player
     */
    public static EntityLivingBase getEntity(EntityPlayerSP player, float angle, float distance) {
        EntityLivingBase distanceCheck = EntityUtil.getClosestEntity(player, distance);
        EntityLivingBase angleCheck = EntityUtil.getClosestEntityToCursor(player, angle, distance);

        return angleCheck != null ? angleCheck : distanceCheck;
    }

    /**
     * Returns the closest {@link EntityLivingBase} to the player.
     *
     * @param player player instance
     * @param range  range to check within
     * @return closest {@link EntityLivingBase} to the player
     */
    public static EntityLivingBase getClosestEntity(EntityPlayerSP player, float range) {
        // Creates a temporary local entity.
        EntityLivingBase tempEntity = null;

        // Iterates through the loaded entity list of the world.
        for (Entity entity : player.worldObj.loadedEntityList) {
            // Checks if the entity equals the player and is a living entity.
            if (entity == player || !(entity instanceof EntityLivingBase)) {
                continue;
            }

            // Creates a local cast of the entity.
            EntityLivingBase entityLivingBase = (EntityLivingBase) entity;

            // Checks if the entity is valid.
            if (!EntityUtil.isEntityValid(player, entityLivingBase, range)) {
                continue;
            }

            // The current distance to the entity.
            float currentDistance = player.getDistanceToEntity(entityLivingBase);

            // Checks if the current distance is within the range.
            if (currentDistance <= range) {
                // Sets the range to the current distance.
                range = currentDistance;
                // Sets the temporary entity to the living entity.
                tempEntity = entityLivingBase;
            }
        }

        return tempEntity;
    }

    /**
     * Returns the closest {@link EntityLivingBase} to the cursor of the player.
     *
     * @param player player instance
     * @param range  range to check within
     * @param angle  angle to check within
     * @return closest {@link EntityLivingBase} to the player
     */
    public static EntityLivingBase getClosestEntityToCursor(EntityPlayerSP player, float angle, float range) {
        // Creates a temporary local entity.
        EntityLivingBase tempEntity = null;

        // Iterates through the loaded entity list of the world.
        for (Entity entity : player.worldObj.loadedEntityList) {
            // Checks if the entity equals the player and is a living entity.
            if (entity == player || !(entity instanceof EntityLivingBase)) {
                continue;
            }

            // Creates a local cast of the entity.
            EntityLivingBase entityLivingBase = (EntityLivingBase) entity;

            // Checks if the entity is valid.
            if (!EntityUtil.isEntityValid(player, entityLivingBase, range)) {
                continue;
            }

            // Creates a local instance of the needed angles to face the entity.
            float[] angles = calculateAngle(player, entityLivingBase);

            // Gets the angle difference between the player's angles and the needed angles.
            float angleDifference = getDistanceBetweenAngles(player.rotationYawHead, angles[0]) + getDistanceBetweenAngles(player.rotationPitch, angles[1]) / 2;

            // Checks if the angle distance is within the original angle.
            if (angleDifference <= angle) {
                // Sets the angle to the angle difference.
                angle = angleDifference;
                // Sets the temporary entity to the living entity.
                tempEntity = entityLivingBase;
            }
        }

        return tempEntity;
    }

    /**
     * Returns {@code true} if the entity should be attacked, otherwise {@code false}.
     *
     * @param player player instance
     * @param entity entity instance
     * @param range  range to check within
     * @return {@code true} if the entity should be attacked, otherwise {@code false}
     */
    @SuppressWarnings("RedundantIfStatement")
    private static boolean isEntityValid(EntityPlayerSP player, Entity entity, float range) {
        // Checks if the player or the entity is null. In addition, it makes sure the entity isn't the player.
        if (entity == null || entity == player) {
            return false;
        }

        // Checks if the entity is dead.
        if (entity.isDead || !entity.isEntityAlive()) {
            return false;
        }

        // Checks if the entity has existed for less than 20 ticks.
        if (entity.ticksExisted < 20) {
            return false;
        }

        // Checks if the entity is within the proper range.
        if (player.getDistanceToEntity(entity) > range) {
            return false;
        }

        return true;
    }

    /**
     * Returns an array of offsets for the player's x-motion and
     * z-motion using the specified multiplier.
     * <p>
     * The logic behind this comes mostly from
     * {@link EntityLivingBase#jump()}.
     *
     * @param player     instance of the player
     * @param multiplier value to multiply motion by
     * @return an array of motion offsets
     */
    public static double[] getMotionOffsets(EntityPlayerSP player, double multiplier) {
        float direction = formYaw(player);
        double motionX = Math.cos(Math.toRadians(direction)) * multiplier;
        double motionZ = Math.sin(Math.toRadians(direction)) * multiplier;
        return new double[]{motionX, motionZ};
    }

    /**
     * Forms a yaw value that can be used for motion offsets
     * depending on the player's movement forward and sideways. We
     * add 90 to the rotation yaw as Minecraft doesn't follow
     * traditional trigonometry rules as Notch decided to deduct 90
     * from entity yaw.
     *
     * @param player instance of the player
     * @return formed yaw
     */
    private static float formYaw(EntityPlayer player) {
        float direction = player.rotationYaw;
        if (player.moveForward < 0) {
            direction += 180;
        }

        if (player.moveStrafing > 0) {
            direction += (-90 * (player.moveForward < 0 ? -0.5F : (player.moveForward > 0 ? 0.5F : 1)));
        }

        if (player.moveStrafing < 0) {
            direction -= (-90 * (player.moveForward < 0 ? -0.5F : (player.moveForward > 0 ? 0.5F : 1)));
        }

        return direction + 90.0F;
    }

    /**
     * Returns the speed of the specified {@link EntityPlayerSP}.
     *
     * @param player player instance
     * @return speed of the specified {@link EntityPlayerSP}
     */
    public static double getSpeed(EntityPlayerSP player) {
        return Math.hypot(player.motionX, player.motionZ);
    }

    /**
     * Sets the speed of the specified {@link EntityPlayerSP} to the specified speed.
     *
     * @param player player instance
     * @param speed speed to set
     */
    public static void setSpeed(EntityPlayerSP player, double speed) {
        // Creates a local instance of the motion offsets.
        double[] motionOffsets = getMotionOffsets(player, speed);

        // Sets the speed of the player.
        player.motionX = motionOffsets[0];
        player.motionZ = motionOffsets[1];
    }

    /**
     * Returns the required angles needed to aim at the specific position.
     *
     * @param player player instance
     * @param position coordinates
     * @return required angles needed to aim at the specific position.
     */
    public static float[] calculateAngle(EntityPlayerSP player, float[] position) {
        // Creates a local instance of the player's coordinates.
        float[] playerPosition = {(float) player.posX, (float) player.posY, (float) player.posZ};

        return AngleUtil.calculateAngle(playerPosition, position);
    }

    /**
     * Returns the required angles needed to aim at the specified {@link BlockPos}.
     *
     * @param player player instance
     * @param blockPos block instance
     * @return required angles needed to aim at the specified {@link BlockPos}
     */
    public static float[] calculateAngle(EntityPlayerSP player, BlockPos blockPos) {
        // Creates a local instance of the entity's coordinates.
        float[] entityPosition = {(float) blockPos.getX(), (float) blockPos.getY(), (float) blockPos.getZ()};

        return calculateAngle(player, entityPosition);
    }


    /**
     * Returns the required angles needed to aim at the specified {@link Entity}.
     *
     * @param player player instance
     * @param entity entity instance
     * @return required angles needed to aim at the specified {@link Entity}
     */
    public static float[] calculateAngle(EntityPlayerSP player, Entity entity) {
        // Creates a local instance of the entity's coordinates.
        float[] entityPosition = {(float) entity.posX, (float) entity.posY, (float) entity.posZ};

        return calculateAngle(player, entityPosition);
    }

    /**
     * Returns the required angles needed to aim the specified {@link Entity} incremented by the specified values.
     *
     * @param player player instance
     * @param entity entity instance
     * @param yawSpeed increment value for yaw
     * @param pitchSpeed increment value for pitch
     * @return required angles needed to aim the specified {@link Entity} incremented by the specified values
     */
    public static float[] calculateAngle(EntityPlayerSP player, Entity entity, float yawSpeed, float pitchSpeed) {
        // Creates a local instance of the final angle to aim at the specified angle.
        float[] angle = calculateAngle(player, entity);

        // Creates a local instance of the yaw and pitch incremented by the specified values.
        float yaw = angle[0] / yawSpeed;
        float pitch = angle[1] / pitchSpeed;

        return new float[]{yaw, pitch};
    }

    /**
     * Returns {@code true} if the specified entity
     * is within the specified field of view, otherwise
     * {@code false}.
     *
     * @param player player instance
     * @param entity entity instance
     * @param angle  field of view to check within
     * @return {@code true} if the specified entity is within the specified field of view, otherwise {@code false}
     */
    public static boolean isEntityWithinFieldOfView(EntityPlayerSP player, Entity entity, float angle) {
        // The difference between the player's x coordinate and the target's x coordinate.
        double difX = entity.posX - player.posX;
        // The difference between the player's z coordinate and the target's z coordinate.
        double difZ = entity.posZ - player.posZ;
        // Translates the differences into three dimensional space.
        float yaw = (float) (Math.atan2(difZ, difX) * 180.0D / Math.PI) - 90.0F;

        return getDistanceBetweenAngles(yaw, player.rotationYaw) <= angle;
    }

    /**
     * Returns the distance between the specified angles.
     *
     * @param first  first angle
     * @param second second angle
     * @return distance between the specified angles
     */
    private static float getDistanceBetweenAngles(float first, float second) {
        // Gets the remainder of the absolute value of the angles subtracted.
        float distance = Math.abs(first - second) % 360;

        // Checks if the distance is more than 180.
        if (distance > 180) {
            // Sets the distance between the angles.
            distance = 360 - distance;
        }

        return distance;
    }

    /**
     * Returns the closest entity under the user's cursor.
     *
     * @param distance     distance to check within
     * @param partialTicks timer ticks
     * @return closest entity under the user's cursor
     */
    public static Optional<EntityLivingBase> rayTrace(double distance, float partialTicks) {
        // Creates a local instance of the entity.
        EntityLivingBase entity = null;
        // Creates a local instance of the ray trace.
        MovingObjectPosition movingObjectPosition;
        // Checks if the player, the render view entity, or the world is null.
        if (minecraft.thePlayer == null || minecraft.theWorld == null || minecraft.getRenderViewEntity() == null) {
            return Optional.empty();
        }
        // Instantiates the local ray trace instance.
        movingObjectPosition = minecraft.thePlayer.rayTrace(distance, partialTicks);
        // Creates a local instance of the player's eye position.
        Vec3 eyeVector = minecraft.thePlayer.getPositionEyes(partialTicks);
        // Creates a local instance of the distance to the vector.
        double distanceToVector = distance;
        // Checks if the ray trace isn't null.
        if (movingObjectPosition != null) {
            // Sets the distance to the distance to the eyes of the player.
            distanceToVector = movingObjectPosition.hitVec.distanceTo(eyeVector);
        }
        // Creates a local instance of the interpolated look vector of the player.
        Vec3 lookVector = minecraft.thePlayer.getLook(partialTicks);
        // Creates a local instance of the interpolated position vector of the player.
        Vec3 positionVector = eyeVector.addVector(lookVector.xCoord * distance, lookVector.yCoord * distance, lookVector.zCoord * distance);
        // Creates a local list of entities within the bounding box of the ray trace.
        List entities = minecraft.theWorld.getEntitiesWithinAABBExcludingEntity(minecraft.thePlayer, minecraft.thePlayer.getEntityBoundingBox().addCoord(lookVector.xCoord * distance, lookVector.yCoord * distance, lookVector.zCoord * distance).expand(1.0F, 1.0F, 1.0F));
        // Creates a local instance of the how far the user is into the vector.
        double vectorInsideDistance = distanceToVector;
        // Iterates through the collection of entities.
        for (Object o : entities) {
            // Checks if the entity is a living entity.
            if (o instanceof EntityLivingBase) {
                // Creates a local cast of the entity.
                EntityLivingBase e = (EntityLivingBase) o;

                // Creates a local bounding box expanded by the border size of the entity.
                AxisAlignedBB boundingBox = e.getEntityBoundingBox().expand(e.getCollisionBorderSize(), e.getCollisionBorderSize(), e.getCollisionBorderSize());

                // Creates a local moving object instance of the intercept of the specified vectors.
                MovingObjectPosition intercept = boundingBox.calculateIntercept(eyeVector, positionVector);

                // Checks if the eye vector is inside the bounding box.
                if (boundingBox.isVecInside(eyeVector)) {
                    if (0D < vectorInsideDistance || vectorInsideDistance == 0) {
                        // Sets the local entity instance to the entity.
                        entity = e;
                        // Sets the vector distance to the 0.
                        vectorInsideDistance = 0D;
                    }
                } else if (intercept != null) { // Checks if the intercept is not null.
                    // Creates a local instance of the distance from the hit vector of the intercept.
                    double interceptDistance = eyeVector.distanceTo(intercept.hitVec);

                    // Checks if the intercept distance is less than how far the user is inside the vector, or if the vector is 0.
                    if (interceptDistance < vectorInsideDistance || vectorInsideDistance == 0D) {
                        // Sets the local entity instance to the entity.
                        entity = e;
                        // Sets the vector distance to the intercept distance.
                        vectorInsideDistance = interceptDistance;
                    }
                }
            }
        }

        // Checks if the inside distance is less than the distance to the vector, or if the ray trace is null.
        if (vectorInsideDistance < distanceToVector || movingObjectPosition == null) {
            return Optional.ofNullable(entity);
        }

        return Optional.empty();
    }

}
