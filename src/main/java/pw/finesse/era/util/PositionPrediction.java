package pw.finesse.era.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

/**
 * A compensator written to predict a player's position.
 * <p>
 * TODO: Add movement prediction. (find formulas to use)  Hal said use the previous 3 positions.
 *
 * @author Ddong
 * @since Jul 17, 2016
 */
public final class PositionPrediction {

    /**
     * Compensate the player's position based on lag and
     * the target's position.  We use the target's ping divided
     * by the user's current ping as the fraction for the
     * interpolation method.
     * <p>
     * If we divide the target's ping by the user's current ping
     * we end up with a decent enough calculation to predict the position.
     *
     * @param player target player to predict
     * @return simulated player based on ping
     */
    public Vec3 predictLag(EntityPlayer player) {
        // Check if the player or his profile is null
        if (player == null || player.getGameProfile() == null) {
            return new Vec3(-1, -1, -1);
        }

        // Get a local minecraft instance
        Minecraft minecraft = Minecraft.getMinecraft();
        if (minecraft.getNetHandler() == null) {
            return player.getPositionVector();
        }

        // Get the player's network information
        NetworkPlayerInfo information = minecraft.getNetHandler().getPlayerInfo(player.getGameProfile().getId());
        if (information == null) {
            return player.getPositionVector();
        }

        // Get the target's ping
        double targetPing = information.getResponseTime();
        // Get your current ping to the server
        double myPing = minecraft.getNetHandler().getPlayerInfo(minecraft.thePlayer.getGameProfile().getId()).getResponseTime();
        // Return the predicted player position based on the current pings
        return predict(player, (float) Math.ceil(myPing / targetPing));
    }

    /**
     * Compensate the player's position based on a multiplier.
     *
     * @param player   target player to predict
     * @param fraction time difference thingy
     * @return simulated player based on ping
     */
    public Vec3 predict(EntityPlayer player, float fraction) {
        // Use linear interpolation to predict the positions
        double x = lerp(player.posX, player.prevPosX, fraction);
        double y = lerp(player.posY, player.prevPosY, fraction);
        double z = lerp(player.posZ, player.prevPosZ, fraction);
        // Apply ray tracing to prevent it from going underground
        return applyTracing(player.getEntityWorld(), player.getPositionVector(), new Vec3(x, y, z));
    }

    /**
     * Using tracing we can predict the player's position even more
     * accurately.  This prevents the prediction from inside solid
     * blocks.  We can also use this to check collision for the player
     * by checking if they will walk into a solid object.
     *
     * @param world    world we're tracing in
     * @param position the interpolated position
     */
    private Vec3 applyTracing(World world, Vec3 position, Vec3 prediction) {
        // Using ray tracing we can check if there is a block between the position of the player and the prediction
        MovingObjectPosition objectPosition = world.rayTraceBlocks(position, prediction, false, true, false);
        // Check if the result is null
        if (objectPosition == null) {
            return prediction;
        }

        // Return the tracing's hit positions
        return objectPosition.hitVec;
    }

    /**
     * Using linear interpolation we are predicting a position
     * based on the the previous position, the current position
     * and the factor.
     * <p>
     * {@see https://en.wikipedia.org/wiki/Linear_interpolation}
     *
     * @param to       current position
     * @param from     previous position
     * @param fraction target ping / user ping
     * @return interpolated position
     */
    private double lerp(double to, double from, double fraction) {
        return (to - from) * fraction + from;
    }
}