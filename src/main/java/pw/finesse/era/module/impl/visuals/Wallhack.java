package pw.finesse.era.module.impl.visuals;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Property;
import pw.finesse.era.events.game.render.entity.PostRenderEntityEvent;
import pw.finesse.era.events.game.render.entity.PreRenderEntityEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * A module that renders player entities, hostile entities,
 * and passive entities through walls.
 *
 * @author Marc
 * @since 8/6/2016
 */
@AutoLoad(Module.class)
public class Wallhack extends Toggleable {

    /**
     * Whether to render players through walls.
     */
    @Property(label = "Players")
    private boolean players = true;

    /**
     * Whether to render passive entities through walls.
     */
    @Property(label = "Passives")
    private boolean passives = false;

    /**
     * Whether to render hostile entities through walls.
     */
    @Property(label = "Hostiles")
    private boolean hostiles = false;

    public Wallhack() {
        super("Wallhack", Keyboard.KEY_COMMA, Category.VISUALS);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreRenderEntityEvent>() {
            @Override
            public void run(PreRenderEntityEvent event) {
                // Checks if the entity is valid to render.
                if (!Wallhack.this.isEntityValid(mc.thePlayer, event.getEntity())) {
                    return;
                }

                // Disables the lightmap of Minecraft's renderer.
                mc.entityRenderer.disableLightmap();
                // Enables the polygon offset.
                GlStateManager.enablePolygonOffset();
                // Sets the polygon offset.
                GlStateManager.doPolygonOffset(2.0F, -2000000F);
            }
        }, new Listener<PostRenderEntityEvent>() {
            @Override
            public void run(PostRenderEntityEvent event) {
                // Checks if the entity is valid to render.
                if (!Wallhack.this.isEntityValid(mc.thePlayer, event.getEntity())) {
                    return;
                }

                // Sets the polygon offset.
                GlStateManager.doPolygonOffset(2.0F, 2000000F);
                // Disables the polygon offset.
                GlStateManager.disablePolygonOffset();

                // Enables the lightmap of Minecraft's renderer.
                mc.entityRenderer.enableLightmap();
            }
        });
    }

    /**
     *
     * Returns {@code true} if the entity is valid to be rendered, otherwise {@code false}.
     *
     * @param player player instance
     * @param entity entity instance
     * @return {@code true} if the entity is valid to be rendered, otherwise {@code false}
     */
    private boolean isEntityValid(EntityPlayerSP player, EntityLivingBase entity) {
        // Checks if the player or the entity is null. In addition, it makes sure the entity isn't the player.
        if (player == null || entity == null || entity == player) {
            return false;
        }

        // Checks if the entity is dead.
        if (entity.isDead || !entity.isEntityAlive() || entity.deathTime > 0) {
            return false;
        }

        // Checks if the entity is of the proper type.
        if (!this.isProperType(entity)) {
            return false;
        }

        return true;
    }

    /**
     * Returns {@code true} if the entity is of the correct type, otherwise {@code false}.
     *
     * @param entity entity instance
     * @return {@code true} if the entity is of the correct type, otherwise {@code false}
     */
    private boolean isProperType(EntityLivingBase entity) {
        // Checks if the entity is a player entity.
        if (this.players && entity instanceof EntityPlayer) {
            return true;
        }

        // Checks if the entity is a hostile entity.
        if (this.hostiles && entity instanceof IMob) {
            return true;
        }

        // Checks if the entity is a passive entity.
        if (this.passives && entity instanceof IAnimals) {
            return true;
        }

        return false;
    }
}
