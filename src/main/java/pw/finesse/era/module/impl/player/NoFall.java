package pw.finesse.era.module.impl.player;

import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * @author Marc
 * @since 8/17/2016
 */
@AutoLoad(Module.class)
public class NoFall extends Toggleable {

    public NoFall() {
        super("No Fall", Category.PLAYER);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                if (event.getPlayer().fallDistance > 2) {
                    event.setOnGround(true);
                }
            }
        });
    }
}
