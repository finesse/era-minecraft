package pw.finesse.era.module.impl.movement;

import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.network.PacketEvent;
import pw.finesse.era.events.game.player.update.PostMotionUpdateEvent;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

import static net.minecraft.network.play.client.C0BPacketEntityAction.Action.START_SNEAKING;
import static net.minecraft.network.play.client.C0BPacketEntityAction.Action.STOP_SNEAKING;

/**
 * A module that automatically sneaks for
 * the player server-sided.
 *
 * @author Marc
 * @since 8/6/2016
 */
@AutoLoad(Module.class)
public class Sneak extends Toggleable {

    public Sneak() {
        super("Sneak", Keyboard.KEY_Z, Category.MOVEMENT);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                // Creates a local instance of the player's packet queue.
                NetHandlerPlayClient queue = event.getPlayer().sendQueue;
                // Checks if the player isn't already sneaking client-side.
                if (!event.getPlayer().isSneaking()) {
                    if (event.isMoving(event.getPlayer())) {
                        // Sends the start sneaking packet.
                        queue.addToSendQueue(new C0BPacketEntityAction(event.getPlayer(), START_SNEAKING));
                        // Sends the stop sneaking packet.
                        queue.addToSendQueue(new C0BPacketEntityAction(event.getPlayer(), STOP_SNEAKING));
                    } else {
                        // Sends the start sneaking packet.
                        queue.addToSendQueue(new C0BPacketEntityAction(event.getPlayer(), START_SNEAKING));
                    }
                }
            }
        }, new Listener<PostMotionUpdateEvent>() {
            @Override
            public void run(PostMotionUpdateEvent event) {
                // Creates a local instance of the player's packet queue.
                NetHandlerPlayClient queue = event.getPlayer().sendQueue;

                // Checks if the player isn't already sneaking client-side.
                if (!event.getPlayer().isSneaking()) {
                    // Sends the start sneaking packet.
                    queue.addToSendQueue(new C0BPacketEntityAction(event.getPlayer(), START_SNEAKING));
                }
            }
        }, new Listener<PacketEvent>() {
            @Override
            public void run(PacketEvent event) {
                // Checks if the packet is a block placement packet.
                if (event.getPacket() instanceof C08PacketPlayerBlockPlacement) {
                    // Checks if the player isn't already sneaking client-side.
                    if (!mc.thePlayer.isSneaking()) {
                        // Sends the stop sneaking packet.
                        mc.thePlayer.sendQueue.addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, STOP_SNEAKING));
                    }
                }
            }
        });
    }

    @Override
    public void onDisable() {
        super.onDisable();

        // Checks if the player isn't already sneaking client-side.
        if (!mc.thePlayer.isSneaking()) {
            // Sends the stop sneaking packet.
            mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, STOP_SNEAKING));
        }
    }
}
