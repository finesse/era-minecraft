package pw.finesse.era.module.impl.player;

import net.minecraft.network.play.server.S08PacketPlayerPosLook;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.network.PacketEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

import java.lang.reflect.Field;

/**
 * A module that prevents the server from
 * modifying or setting the player's angles.
 *
 * @author Marc
 * @since 8/17/2016
 */
@AutoLoad(Module.class)
public class NoRotate extends Toggleable {

    public NoRotate() {
        super("No Rotate", Category.PLAYER);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PacketEvent>() {
            @Override
            public void run(PacketEvent event) {
                // Checks if the packet is of the correct type.
                if (event.getPacket() instanceof S08PacketPlayerPosLook) {
                    // Creates a local cast of the packet.
                    S08PacketPlayerPosLook packet = (S08PacketPlayerPosLook) event.getPacket();



                    // Sets the angles.
                    NoRotate.this.setYaw(packet, mc.thePlayer.rotationYaw);
                    NoRotate.this.setPitch(packet, mc.thePlayer.rotationPitch);
                }
            }
        });
    }

    /**
     * Sets the yaw of the specified packet to the specified yaw.
     *
     * @param packet packet to modify
     * @param yaw    yaw to set
     */
    private void setYaw(S08PacketPlayerPosLook packet, float yaw) {
        try {
            // Creates a local instance of the field.
            Field field = packet.getClass().getDeclaredField("yaw");
            // Sets the accessibility of the field.
            field.setAccessible(true);
            // Sets the field.
            field.set(packet, yaw);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the pitch of the specified packet to the specified pitch.
     *
     * @param packet packet to modify
     * @param pitch  pitch to set
     */
    private void setPitch(S08PacketPlayerPosLook packet, float pitch) {
        try {
            // Creates a local instance of the field.
            Field field = packet.getClass().getDeclaredField("pitch");
            // Sets the accessibility of the field.
            field.setAccessible(true);
            // Sets the field.
            field.set(packet, pitch);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


}
