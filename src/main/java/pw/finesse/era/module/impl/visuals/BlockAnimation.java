package pw.finesse.era.module.impl.visuals;

import net.minecraft.client.renderer.ItemRenderer;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * A module that uses the Minecraft 1.7 blocking animation.
 * {@link ItemRenderer#renderItemInFirstPerson(float)}
 *
 * @author Marc
 * @since 8/10/2016
 */
@AutoLoad(Module.class)
public class BlockAnimation extends Toggleable {

    public BlockAnimation() {
        super("Block Animation", Category.VISUALS);
        this.setEnabled(true);
        this.setVisible(false);
    }

    @Override
    protected void setup() {

    }
}
