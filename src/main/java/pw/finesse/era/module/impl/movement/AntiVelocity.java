package pw.finesse.era.module.impl.movement;

import net.minecraft.network.play.server.S12PacketEntityVelocity;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.era.events.game.network.PacketEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * A module that reduces or nullifies
 * the player's knock-back and velocity.
 *
 * @author Marc
 * @since 8/6/2016
 */
@AutoLoad(Module.class)
public class AntiVelocity extends Toggleable {

    /**
     * The horizontal multiplier of the player's velocity.
     */
    @Property(label = "Horizontal multiplier")
    @Clamp(min = -1D, max = 1D)
    private double horizontal = 0;

    /**
     * The vertical multiplier of the player's velocity.
     */
    @Property(label = "Vertical multiplier")
    @Clamp(min = -1D, max = 1D)
    private double vertical = 0;

    public AntiVelocity() {
        super("Anti Velocity", Keyboard.KEY_LBRACKET, Category.MOVEMENT);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PacketEvent>() {
            @Override
            public void run(PacketEvent event) {
                // Checks if the packet is the correct packet.
                if (event.getPacket() instanceof S12PacketEntityVelocity) {
                    // Creates a local cast of the packet.
                    S12PacketEntityVelocity packet = (S12PacketEntityVelocity) event.getPacket();

                    // Checks if the entity ID of the packet matches the player's entity id.
                    if (packet.getEntityID() == mc.thePlayer.getEntityId()) {
                        // Checks if the horizontal and vertical settings are 0.
                        if (AntiVelocity.this.horizontal == 0.0D && AntiVelocity.this.vertical == 0.0D) {
                            // Cancels the event.
                            event.cancel();
                        }

                        // Checks if the horizontal and vertical settings are 1.
                        if(AntiVelocity.this.horizontal == 1.0D && AntiVelocity.this.vertical == 1.0D) {
                            return;
                        }

                        // Creates a local instance of the x, y, and z motion multipliers of the packet.
                        double motionX = packet.getMotionX() / 8000D * AntiVelocity.this.horizontal;
                        double motionY = packet.getMotionY() / 8000D * AntiVelocity.this.vertical;
                        double motionZ = packet.getMotionZ() / 8000D * AntiVelocity.this.horizontal;

                        // Modifies the outbound packet using the new x, y, and z motion multipliers.
                        event.setPacket(new S12PacketEntityVelocity(packet.getEntityID(), motionX, motionY, motionZ));
                    }
                }
            }
        });
    }
}
