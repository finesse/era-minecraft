package pw.finesse.era.module.impl.visuals;

import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.events.system.ShutdownGameEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * A module that fully brightens the world
 * to allow for better visibility.
 *
 * @author Marc
 * @since 8/17/2016
 */
@AutoLoad(Module.class)
public class Fullbright extends Toggleable {

    /**
     * The custom gamma value for the module.
     */
    @Property(label = "Gamma")
    @Clamp(min = 0F, max = 10F)
    private float gamma = 10F;

    /**
     * The previous gamma setting before the module is enabled.
     */
    private float previousGamma = -1;

    public Fullbright() {
        super("Fullbright", Keyboard.KEY_B, Category.VISUALS);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                // Checks if the gamma value is already set.
                if(mc.gameSettings.gammaSetting != Fullbright.this.gamma) {
                    // Sets the gamma value to the user's specified gamma value.
                    mc.gameSettings.gammaSetting = Fullbright.this.gamma;
                }
            }
        }, new Listener<ShutdownGameEvent>() {
            @Override
            public void run(ShutdownGameEvent event) {
                // Sets the gamma back to the previous setting.
                mc.gameSettings.gammaSetting = Fullbright.this.previousGamma;
            }
        });
    }

    @Override
    public void onEnable() {
        super.onEnable();

        // Saves the gamma value for later use.
        Fullbright.this.previousGamma = mc.gameSettings.gammaSetting;

        // Sets the gamma value to the user's specified gamma value.
        mc.gameSettings.gammaSetting = Fullbright.this.gamma;

    }

    @Override
    public void onDisable() {
        super.onDisable();

        // Sets the gamma back to the previous setting.
        mc.gameSettings.gammaSetting = Fullbright.this.previousGamma;
    }

}
