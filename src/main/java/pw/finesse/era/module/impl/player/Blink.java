package pw.finesse.era.module.impl.player;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import net.minecraft.util.Timer;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.network.PacketEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

import java.lang.reflect.Field;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author Marc
 * @since 8/8/2016
 */
@AutoLoad(Module.class)
public class Blink extends Toggleable {

    /**
     * A {@link Queue} of packets to be delayed and sent at a different time.
     */
    private Queue<Packet> packetQueue = new ConcurrentLinkedQueue<>();

    /**
     * The cached field of the timer.
     */
    private Field timerCache;

    public Blink() {
        super("Blink", Keyboard.KEY_N, Category.PLAYER);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PacketEvent>() {
            @Override
            public void run(PacketEvent event) {
                // Checks if the packet is of the correct type.
                if (event.getPacket() instanceof C03PacketPlayer || event.getPacket() instanceof C07PacketPlayerDigging || event.getPacket() instanceof C08PacketPlayerBlockPlacement || event.getPacket() instanceof C0BPacketEntityAction || event.getPacket() instanceof C03PacketPlayer.C04PacketPlayerPosition || event.getPacket() instanceof C03PacketPlayer.C05PacketPlayerLook) {
                    // Cancels the event.
                    event.cancel();

                    // Checks if the player is moving.
                    if (Blink.this.isMoving(mc.thePlayer)) {
                        // Queues the packet.
                        Blink.this.packetQueue.add(event.getPacket());

                        // Sets the display data of the module.
                        Blink.this.setData(Blink.this.packetQueue.size() + "");
                    }

                    // Checks if the queue is empty.
                    if (Blink.this.packetQueue.isEmpty()) {
                        // Resets the data of the module.
                        Blink.this.setData(null);
                    }
                }
            }
        });
    }

    @Override
    public void onEnable() {
        super.onEnable();

        // Sets the timer speed.
        this.getTimer().timerSpeed = 1.3F;
    }

    @Override
    public void onDisable() {
        super.onDisable();

        // Resets the timer speed to the default.
        this.getTimer().timerSpeed = 1.0F;

        // Checks if the queue is empty.
        while (!this.packetQueue.isEmpty()) {
            // Polls the queue.
            mc.getNetHandler().addToSendQueue(packetQueue.poll());
        }
    }

    /**
     * Return {@code true} if the player is moving, otherwise {@code false}.
     *
     * @return {@code true} if the player is moving, otherwise {@code false}
     */
    private boolean isMoving(EntityPlayerSP player) {
        double xDif = player.posX - player.lastTickPosX;
        double yDif = player.posY - player.lastTickPosY;
        double zDif = player.posZ - player.lastTickPosZ;

        return xDif * xDif + yDif * yDif + zDif * zDif > 9.0E-4D;
    }

    /**
     * Uses reflection to return the timer of the game.
     *
     * @return timer of the game
     */
    private Timer getTimer() {
        try {
            // Checks if the cached field is present.
            if (this.timerCache == null) {
                // Creates a local instance of the timer field.
                Field field = mc.getClass().getDeclaredField("timer");
                // Sets the accessibility of the field.
                field.setAccessible(true);
                // Sets the field.
                this.timerCache = field;
            }

            return (Timer) this.timerCache.get(mc);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
