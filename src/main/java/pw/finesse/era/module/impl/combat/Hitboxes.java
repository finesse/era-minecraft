package pw.finesse.era.module.impl.combat;

import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.era.events.game.player.combat.CollisionBorderEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * A module that expands the hit-boxes of enemy
 * players, allowing the user to hit outside of
 * the intended area.
 *
 * @author Marc
 * @since 8/14/2016
 */
@AutoLoad(Module.class)
public class Hitboxes extends Toggleable {

    /**
     * The size of the player's collision border.
     */
    @Property(label = "Size")
    @Clamp(min = 0F, max = 1F)
    private float size = 0.3F;

    public Hitboxes() {
        super("Hitboxes", Category.COMBAT);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<CollisionBorderEvent>() {
            @Override
            public void run(CollisionBorderEvent event) {
                // Sets the border size.
                event.setBorderSize(Hitboxes.this.size);
            }
        });
    }
}
