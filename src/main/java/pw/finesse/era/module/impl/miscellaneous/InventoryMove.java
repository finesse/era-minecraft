package pw.finesse.era.module.impl.miscellaneous;

import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.inventory.GuiEditSign;
import net.minecraft.client.settings.KeyBinding;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.events.input.UnpressKeyEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

import java.util.Arrays;

/**
 * A module which allows for the player to send
 * movement input while in inventories or GUI screens.
 *
 * @author Marc
 * @since 8/14/2016
 */
@AutoLoad(Module.class)
public class InventoryMove extends Toggleable {

    /**
     * A collection of keys to listen for.
     */
    private KeyBinding[] keys = {mc.gameSettings.keyBindForward, mc.gameSettings.keyBindBack, mc.gameSettings.keyBindRight, mc.gameSettings.keyBindLeft, mc.gameSettings.keyBindJump};


    public InventoryMove() {
        super("Inventory Move", Category.MISCELLANEOUS);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                // Checks if the current screen is valid.
                if (mc.currentScreen == null || mc.currentScreen instanceof GuiChat || mc.currentScreen instanceof GuiEditSign) {
                    return;
                }

                // Iterates through the keys.
                Arrays.stream(keys).forEach(key -> {
                    // Sets the key bind pressed state.
                    KeyBinding.setKeyBindState(key.getKeyCode(), Keyboard.isKeyDown(key.getKeyCode()));
                });
            }
        }, new Listener<UnpressKeyEvent>() {
            @Override
            public void run(UnpressKeyEvent event) {
                // Iterates through the keys.
                if (mc.currentScreen == null) {
                    Arrays.stream(keys).forEach(key -> {
                        if (event.getKey() == key) {
                            event.cancel();
                        }
                    });
                }
            }
        });
    }

}
