package pw.finesse.era.module.impl.combat;

import net.minecraft.entity.EntityLivingBase;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Toggleable;

/**
 * A module that smoothly aims for the
 * player to simulate realistic mouse
 * movements.
 *
 * @author Marc
 * @since 8/22/2016
 */
public class SmoothAimbot extends Toggleable {

    /**
     * The speed at which the yaw will update.
     */
    @Property(label = "Yaw Speed")
    @Clamp(min = 0, max = 25)
    private int yawSpeed = 7;

    /**
     * The speed at which the pitch will update.
     */
    @Property(label = "Pitch Speed")
    @Clamp(min = 0, max = 25)
    private int pitchSpeed = 0;

    /**
     * The field of view the entity needs to be within to start aiming.
     */
    @Property(label = "FOV")
    @Clamp(min = 0, max = 180)
    private int fov = 90;

    /**
     * The range at which the trigger bot will attack, in blocks.
     */
    @Property(label = "Range")
    @Clamp(min = 0, max = 10F)
    private float range = 4.0F;

    /**
     * The rate at which the speed will fluctuate.
     */
    @Property(label = "Fluctuation")
    @Clamp(min = 0, max = 50)
    private int fluctuation = 25;

    /**
     * The ticks the client will wait before attacking a new entity.
     */
    @Property(label = "Ticks Existed")
    @Clamp(min = 0, max = 100)
    private int ticksExisted = 25;

    /**
     * Whether to attack players on your team.
     */
    @Property(label = "Team")
    private boolean team = false;

    /**
     * Whether to only attack when holding a weapon.
     */
    @Property(label = "Weapon Only")
    private boolean weaponOnly = true;

    /**
     * An instance of the target of attack.
     */
    private EntityLivingBase target;

    public SmoothAimbot() {
        super("Smooth Aimbot", Keyboard.KEY_R, Category.COMBAT);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {

            }
        });
    }
}
