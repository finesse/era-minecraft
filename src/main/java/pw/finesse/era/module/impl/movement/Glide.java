package pw.finesse.era.module.impl.movement;

import org.lwjgl.input.Keyboard;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Toggleable;

/**
 * @author Marc
 * @since 8/14/2016
 */
public class Glide extends Toggleable {

    public Glide() {
        super("Glide", Keyboard.KEY_APOSTROPHE, Category.MOVEMENT);
    }

    @Override
    protected void setup() {

    }
}
