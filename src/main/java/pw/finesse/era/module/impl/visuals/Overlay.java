package pw.finesse.era.module.impl.visuals;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import net.minecraft.block.material.Material;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Property;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.game.render.ingame.RenderOverlayEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

import java.awt.*;
import java.io.*;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import static net.minecraft.util.EnumChatFormatting.GRAY;
import static net.minecraft.util.EnumChatFormatting.RESET;

/**
 * A module that renders a custom in-game overlay.
 *
 * @author Marc
 * @since 8/4/2016
 */
@AutoLoad(Module.class)
public class Overlay extends Toggleable {

    /**
     * Whether to render the watermark.
     */
    @Property(label = "Watermark")
    private boolean watermark = true;

    /**
     * Whether to render the client name.
     */
    @Property(label = "Client Name")
    private boolean clientName = true;

    /**
     * Whether to render the build number.
     */
    @Property(label = "Build Number")
    private boolean buildNumber = true;

    /**
     * Whether to render the enabled list.
     */
    @Property(label = "Enabled List")
    private boolean enabledList = true;

    /**
     * Whether to render information about the currently held item.
     */
    @Property(label = "Held Item")
    private boolean heldItem = true;

    /**
     * Whether to render the coordinates.
     */
    @Property(label = "Coordinates")
    private boolean coordinates = true;

    /**
     * Whether to render potions.
     */
    @Property(label = "Potions")
    private boolean potions = true;

    /**
     * The file of the color map.
     */
    private File file = new File(Era.INSTANCE.getDirectory(), "colormap.json");

    /**
     * A map of colors corresponding to the module name.
     */
    private Map<String, Integer> colorMap = new TreeMap<>();

    public Overlay() {
        super("Overlay", Category.VISUALS);
        this.setEnabled(true);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<RenderOverlayEvent>() {
            @Override
            public void run(RenderOverlayEvent event) {
                // Checks if the Minecraft debug overlay is enabled.
                if (event.getMinecraft().gameSettings.showDebugInfo) {
                    return;
                }

                // Checks if the watermark property is enabled.
                if (Overlay.this.watermark) {
                    // Creates a local instance of the string builder.
                    StringBuilder stringBuilder = new StringBuilder();

                    // Checks if the client name property is enabled.
                    if (Overlay.this.clientName) {
                        stringBuilder.append(Era.LABEL);
                        stringBuilder.append(" ");
                    }

                    // Checks if the build number property is enabled.
                    if (Overlay.this.buildNumber) {
                        stringBuilder.append(GRAY);
                        stringBuilder.append("b");
                        stringBuilder.append(Era.BUILD);
                        stringBuilder.append(" ");
                        stringBuilder.append(RESET);
                    }

                    GlStateManager.enableBlend();
                    // Draws the contents of the string builder.
                    event.getFontRenderer().drawStringWithShadow(stringBuilder.toString(), 2, 2, 0xE6FFFFFF);
                    GlStateManager.disableBlend();
                }

                // Checks if the enabled list property is enabled.
                if (Overlay.this.enabledList) {
                    // Creates a local instance of the string builder.
                    StringBuilder stringBuilder = new StringBuilder();

                    // Defines the x and y coordinate offsets.
                    int offset = 2;

                    // Iterates through the client's collection of modules.
                    for (Module module : Era.INSTANCE.getModuleManager().registry()) {
                        // Checks if the module is toggleable.
                        if (module instanceof Toggleable) {
                            // Creates a local cast of the module.
                            Toggleable toggleable = (Toggleable) module;
                            // Checks if the module is enabled.
                            if (toggleable.isEnabled()) {
                                // Checks if the module is visible.
                                if (toggleable.isVisible()) {
                                    stringBuilder.append(toggleable.getLabel());
                                    // Checks if the module's displayable data is not empty.
                                    if (toggleable.getData() != null) {
                                        stringBuilder.append(GRAY);
                                        stringBuilder.append(" ");
                                        stringBuilder.append(toggleable.getData());
                                        stringBuilder.append(RESET);
                                    }

                                    // Creates a local color.
                                    int color = -1;

                                    // Creates an optional container of the color.
                                    Optional<Integer> colorContainer = Optional.ofNullable(colorMap.get(toggleable.getName().toLowerCase()));

                                    // Checks if the color is present.
                                    if (colorContainer.isPresent()) {
                                        // Sets the color to the mapped color.
                                        color = colorContainer.get();
                                    }

                                    GlStateManager.enableBlend();
                                    //Draws the contents of the string builder.
                                    event.getFontRenderer().drawStringWithShadow(stringBuilder.toString(), event.getScaledResolution().getScaledWidth() - event.getFontRenderer().getStringWidth(stringBuilder.toString()) - 2, offset, color);
                                    GlStateManager.disableBlend();

                                    // Clears the string builder.
                                    stringBuilder.delete(0, stringBuilder.length());

                                    // Updates the y coordinate offset with the height of the client's font.
                                    offset += event.getFontRenderer().FONT_HEIGHT + 1;
                                }
                            }
                        }
                    }
                }

                // Checks if the held item property is enabled.
                if (Overlay.this.heldItem) {
                    // Checks if the currently held item is null.
                    if (mc.thePlayer.getHeldItem() != null) {
                        // Creates a local instance of the currently held item.
                        ItemStack item = mc.thePlayer.getHeldItem();

                        // Checks if the held item is damageable.
                        if (item.isItemStackDamageable()) {
                            // Creates a local instance of the current durability of the item.
                            String durability = String.valueOf(item.getMaxDamage() - item.getItemDamage());

                            // Creates a local instance of the width of the string.
                            int width = event.getScaledResolution().getScaledWidth() / 2 + 90 - event.getFontRenderer().getStringWidth(durability);

                            // Creates a local instance of the height of the string.
                            int height = event.getScaledResolution().getScaledHeight() - event.getFontRenderer().FONT_HEIGHT;

                            // Checks if the player is in creative.
                            if (mc.thePlayer.capabilities.isCreativeMode) {
                                height -= 12 + event.getFontRenderer().FONT_HEIGHT;
                            } else {
                                height -= 30 + event.getFontRenderer().FONT_HEIGHT;

                                // Checks if the player is in water.
                                if(mc.thePlayer.isInsideOfMaterial(Material.water) && mc.thePlayer.getAir() > 0) {
                                    height -= event.getFontRenderer().FONT_HEIGHT + 1;
                                }
                            }

                            // Creates a local instance of the item's maximum damage.
                            float maxDamage = item.getMaxDamage();

                            // Creates a local instance of the green and red values of the color.
                            float green = Integer.parseInt(durability) / maxDamage;
                            float red = 1F - green;

                            // Checks if red is less than green.
                            if (red > green) {
                                // Increments the colors by the absolute value of the red color.
                                green += Math.abs(red - 1F);
                                red += Math.abs(red - 1F);
                            } else {
                                // Increments the colors by the absolute value of the green color.
                                red += Math.abs(green - 1F);
                                green += Math.abs(green - 1F);
                            }

                            // Creates a local instance of the color.
                            Color color = new Color(red, green, 0.25F);

                            // Draws the durability.
                            event.getFontRenderer().drawStringWithShadow(durability, width, height, color.brighter().getRGB());
                        }
                    }
                }

                // Checks if the coordinates property is enabled.
                if (Overlay.this.coordinates) {
                    // Creates a local instance of the height of the coordinates.
                    int height = event.getScaledResolution().getScaledHeight() - event.getFontRenderer().FONT_HEIGHT;

                    // Checks the current screen.
                    if (mc.currentScreen instanceof GuiChat) {
                        height -= event.getFontRenderer().FONT_HEIGHT + 5;
                    }

                    // Creates a local instance of the x, y, and z coordinates of the player.
                    double x = Math.round(mc.thePlayer.posX * 10) / 10D;
                    double y = Math.round(mc.thePlayer.posY * 10) / 10D;
                    double z = Math.round(mc.thePlayer.posZ * 10) / 10D;

                    GlStateManager.enableBlend();
                    // Draws the coordinates.
                    event.getFontRenderer().drawStringWithShadow(String.format("%s, %s, %s", x, y, z), 2, height, 0xBFFFFFFF);
                    GlStateManager.enableBlend();
                }


            }
        });
    }

    @Override
    public void load(File source) {
        super.load(source);

        // Checks if the file exists.
        if (!this.file.exists()) {
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(this.file))) {
            // Creates a temporary color map to load from.
            Map<String, String> tempColorMap = new GsonBuilder().setPrettyPrinting().create().fromJson(reader, new TypeToken<HashMap<String, String>>() {}.getType());
            // Iterates through the temporary color map.
            tempColorMap.entrySet().forEach(color -> {
                // Creates a local decimal color of the hexadecimal color.
                int decimalColor = new BigInteger(color.getValue().replaceAll("0x", ""), 16).intValue();
                // Registers the name and the color into the color map of the module.
                this.colorMap.put(color.getKey(), decimalColor);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(File destination) {
        super.save(destination);

        // Checks if the file exists.
        if (!file.exists()) {
            try {
                // Creates the necessary file.
                if (!file.createNewFile()) {
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // Creates a temporary color map to save to.
        Map<String, String> tempColorMap = new TreeMap<>();

        // Checks if the color map of the module isn't empty.
        if (!this.colorMap.isEmpty()) {
            // Iterates through the key set of the color map.
            this.colorMap.keySet().forEach(label -> {
                // Creates a local optional object of the color.
                Optional<Integer> colorContainer = Optional.ofNullable(this.colorMap.get(label));

                // Checks if the index of the color in the color map isn't null.
                if (colorContainer.isPresent()) {
                    // Registers the color into the temporary map.
                    tempColorMap.put(label, Integer.toHexString(colorContainer.get()).toUpperCase());
                }
            });
        } else {
            // Registers an example.
            tempColorMap.put("module", "FFFFFFFF");
        }

        try (FileWriter writer = new FileWriter(this.file)) {
            // Writes the color map to the file.
            writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(tempColorMap));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
