package pw.finesse.era.module.impl.movement;

import net.minecraft.client.entity.EntityPlayerSP;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * A module that automatically sprints for the player.
 *
 * @author Marc
 * @since 8/5/2016
 */
@AutoLoad(Module.class)
public class Sprint extends Toggleable {

    public Sprint() {
        super("Sprint", Keyboard.KEY_G, Category.MOVEMENT);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                // Creates a local instance of the player.
                EntityPlayerSP player = event.getPlayer();

                // Sets the player's sprinting state to true.
                event.setSprinting(shouldSprint(player) && event.isMoving(player));
            }
        });
    }

    /**
     * Checks whether the player should sprint.
     *
     * @param player player instance
     * @return whether the player should sprint
     */
    @SuppressWarnings("RedundantIfStatement")
    private boolean shouldSprint(EntityPlayerSP player) {
        // Checks if the player is collided horizontally.
        if (player.isCollidedHorizontally) {
            return false;
        }

        // Checks if the players food level is high enough.
        if (player.getFoodStats().getFoodLevel() < 7) {
            return false;
        }

        return true;
    }
}
