package pw.finesse.era.module.impl.miscellaneous;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C02PacketUseEntity;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Property;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.game.network.PacketEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

import java.lang.reflect.Field;

import static net.minecraft.network.play.client.C02PacketUseEntity.Action.ATTACK;

/**
 * A module that prevents the user
 * from hitting players on their friends
 * list and on the same team.
 *
 * @author Marc
 * @since 8/17/2016
 */
@AutoLoad(Module.class)
public class Safe extends Toggleable {

    /**
     * Whether to prevent attacks on friends.
     */
    @Property(label = "Friend")
    private boolean friend = true;

    /**
     * Whether to prevent attacks on team mates.
     */
    @Property(label = "Team")
    private boolean team = true;

    public Safe() {
        super("Safe", Category.MISCELLANEOUS);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PacketEvent>() {
            @Override
            public void run(PacketEvent event) {
                // Checks if the packet is the correct one.
                if (event.getPacket() instanceof C02PacketUseEntity) {
                    // Creates a local cast of the packet.
                    C02PacketUseEntity packet = (C02PacketUseEntity) event.getPacket();

                    // Checks if the action is an attack.
                    if (packet.getAction() == ATTACK) {

                        // Creates a local instance of the entity.
                        Entity entity = mc.theWorld.getEntityByID(Safe.this.getEntityId(packet));

                        // Checks if the entity is a player.
                        if (entity instanceof EntityPlayer) {
                            // Creates a local instance of the proper flags to cancel the attack.
                            boolean friendFlag = Safe.this.friend && Era.INSTANCE.getFriendRegistry().get(entity.getCommandSenderName()).isPresent();
                            boolean teamFlag = Safe.this.team && mc.thePlayer.isOnSameTeam((EntityLivingBase) entity);

                            // Cancels the event.
                            event.setCancelled(friendFlag || teamFlag);
                        }
                    }
                }
            }
        });
    }

    /**
     * Returns the entity id of the specified packet.
     *
     * @param packet packet instance
     * @return entity id
     */
    private int getEntityId(C02PacketUseEntity packet) {
        try {
            // Creates a local instance of the field.
            Field field = packet.getClass().getDeclaredField("entityId");
            // Sets the accessibility of the field.
            field.setAccessible(true);

            return (int) field.get(packet);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return -1;
    }
}
