package pw.finesse.era.module.impl.player;

import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * @author Marc
 * @since 11/18/2016
 */
@AutoLoad(Module.class)
public class Jesus extends Toggleable {

    protected Jesus() {
        super("Jesus", Category.PLAYER);
    }

    @Override
    protected void setup() {

    }
}
