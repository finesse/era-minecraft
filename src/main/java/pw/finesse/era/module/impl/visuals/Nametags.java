package pw.finesse.era.module.impl.visuals;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.StringUtils;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.game.render.entity.RenderNametagEvent;
import pw.finesse.era.events.game.render.world.RenderWorldEvent;
import pw.finesse.era.friend.Friend;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.module.impl.miscellaneous.Friends;
import pw.finesse.era.util.RenderUtil;

import java.awt.*;
import java.util.Collections;
import java.util.Optional;

/**
 * A module that displays scaling name tags
 * for the players around the user.
 *
 * @author Marc
 * @since 8/11/2016
 */
@AutoLoad(Module.class)
public class Nametags extends Toggleable {

    /**
     * The type of health to display.
     */
    @Property(label = "Format")
    private Format format = Format.PERCENT;

    /**
     * The base size of the name tag.
     */
    @Property(label = "Size")
    @Clamp(min = 0, max = 1)
    private double size = 0.3;

    /**
     * The scale factor of the name tag.
     */
    @Property(label = "Scale")
    @Clamp(min = 0, max = 1)
    private double scale = 0.3;

    /**
     * Whether to scale name tags.
     */
    @Property(label = "Scaling")
    private boolean scaling = true;

    /**
     * Whether to use the server's display name for the name tag.
     */
    @Property(label = "Server Text")
    private boolean serverText = false;

    /**
     * Whether to display health in the name tags.
     */
    @Property(label = "Health")
    private boolean health = true;

    public Nametags() {
        super("Nametags", Keyboard.KEY_BACKSLASH, Category.VISUALS);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<RenderWorldEvent>() {
            @Override
            public void run(RenderWorldEvent event) {
                // Iterates through the player entities of the world and filters out the player.
                event.getWorld().playerEntities.stream().filter(player -> player != mc.thePlayer).forEach(player -> {
                    // Creates a local instance of the interpolated position of the player.
                    double deltaX = RenderUtil.interpolate(player.posX, player.lastTickPosX, event.getPartialTicks()) - mc.getRenderManager().viewerPosX;
                    double deltaY = RenderUtil.interpolate(player.posY, player.lastTickPosY, event.getPartialTicks()) - mc.getRenderManager().viewerPosY;
                    double deltaZ = RenderUtil.interpolate(player.posZ, player.lastTickPosZ, event.getPartialTicks()) - mc.getRenderManager().viewerPosZ;

                    // Renders the name tag.
                    Nametags.this.renderNametag(mc.thePlayer, player, Nametags.this.getDisplayName(player), deltaX, deltaY, deltaZ);
                });

                // Sorts the collection of player entities to avoid overlapping.
                Collections.sort(event.getWorld().playerEntities, (first, second) -> Integer.compare((int) mc.thePlayer.getDistanceToEntity(second), (int) mc.thePlayer.getDistanceToEntity(first)));

            }
        }, new Listener<RenderNametagEvent>() {
            @Override
            public void run(RenderNametagEvent event) {
                // Checks if the entity is a player.
                if (event.getEntity() instanceof EntityPlayer) {
                    // Cancels the event.
                    event.cancel();
                }
            }
        });
    }

    /**
     * Renders a nametag over the specified entity at the specified coordinates.
     *
     * @param player player instance
     * @param entity entity instance
     * @param name   name to display in tag
     * @param posX   x coordinate to render at
     * @param posY   y coordinate to render at
     * @param posZ   z coordinate to render at
     */
    private void renderNametag(EntityPlayerSP player, EntityPlayer entity, String name, double posX, double posY, double posZ) {
        // Creates a local instance of the distance to the player.
        double distance = player.getDistance(posX + mc.getRenderManager().viewerPosX, posY + mc.getRenderManager().viewerPosY, posZ + mc.getRenderManager().viewerPosZ);
        // Creates a local instance of the default scale value.
        double scale = 0.01;

        // Checks if the scaling property is enabled.
        if(Nametags.this.scaling) {
            // Sets the scaling factor of the name tag.
            scale = (this.scale / 100D) * distance;
        }

        // Creates a local instance of the width of the name tag.
        int width = mc.fontRendererObj.getStringWidth(name) / 2;

        // Checks if the health property is enabled.
        if(Nametags.this.health) {
            // Sets the width of the name tag.
            width = mc.fontRendererObj.getStringWidth(name + " " + this.getHealthString(entity, Nametags.this.format)) / 2;
        }

        // Prevents the scale from going below the base size.
        if (scale < Nametags.this.size / 10) {
            scale = Nametags.this.size / 10;
        }

        // Checks if the entity is sneaking.
        if (entity.isSneaking()) {
            // Decrements the y position.
            posY -= 0.275;
        }

        RenderUtil.beginGl();

        GlStateManager.translate(posX, posY + entity.getEyeHeight() + 0.475, posZ);

        GlStateManager.rotate(-mc.getRenderManager().playerViewY, 0F, 1F, 0F);
        GlStateManager.rotate(mc.getRenderManager().playerViewX, mc.gameSettings.thirdPersonView == 2 ? -1F : 1F, 0F, 0F);

        GlStateManager.scale(-scale, -scale, scale);

        GlStateManager.disableTexture2D();
        RenderUtil.drawBorderedRect(-width - 2, -(mc.fontRendererObj.FONT_HEIGHT + 2), width + 2, 0.5, 1.25F, 0x90000000, 0xFF000000);
        GlStateManager.enableTexture2D();

        mc.fontRendererObj.drawStringWithShadow(name, -width, -mc.fontRendererObj.FONT_HEIGHT, this.getNametagColor(player, entity));

        // Checks if the health property is enabled.
        if(Nametags.this.health) {
            mc.fontRendererObj.drawStringWithShadow(" " + this.getHealthString(entity, Nametags.this.format), -width + mc.fontRendererObj.getStringWidth(name), -mc.fontRendererObj.FONT_HEIGHT, this.getHealthColor(entity, this.format));
        }
        RenderUtil.endGl();
    }

    /**
     * Returns the display name for the player's name tag.
     *
     * @param player player instance
     * @return display name for the player's name tag
     */
    private String getDisplayName(EntityPlayer player) {
        // Creates a local instance of the player's username.
        String username = StringUtils.stripControlCodes(player.getCommandSenderName());

        // Checks if the server text property is enabled.
        if (Nametags.this.serverText) {
            // Sets the username to the server's display name for the player.
            username = player.getDisplayName().getFormattedText();
        }

        // Creates a local optional object of the friend.
        Optional<Friend> friendContainer = Era.INSTANCE.getFriendRegistry().get(player.getCommandSenderName());

        // Checks if the player is a friend.
        if (friendContainer.isPresent()) {
            // Sets the username to the alias of the player.
            username = friendContainer.get().getAlias();
        }

        return username;
    }

    /**
     * Returns the name tag color corresponding to the specified entity.
     *
     * @param player player instance
     * @param entity entity instance
     * @return name tag color corresponding to the specified entity
     */
    private int getNametagColor(EntityPlayerSP player, EntityPlayer entity) {
        // Creates a local instance of the red, green, and blue color values.
        float[] color = {1, 1, 1};

        // Checks if the player is sneaking.
        if (entity.isSneaking()) {
            color = new float[]{1, 0.1F, 0.1F};
        }

        // Checks if the player is invisible to the user.
        if (entity.isInvisibleToPlayer(player)) {
            color = new float[]{0.425F, 0.425F, 0.5F};
        }

        // Checks if the player is a friend.
        if (Era.INSTANCE.getFriendRegistry().get(entity.getCommandSenderName()).isPresent()) {
            // Checks if the friends module is enabled.
            if (Era.INSTANCE.getModuleManager().get(Friends.class).isEnabled()) {
                color = new float[]{0.25F, 0.75F, 1.0F};
            } else {
                color = new float[]{0.55F, 0.525F, 0.5F};
            }
        }

        return new Color(color[0], color[1], color[2]).getRGB();
    }

    /**
     * Returns a color corresponding to the specified health
     * and maximum health values. This method ensures a
     * gradual and smooth color change, fading from one
     * color to another.
     *
     * @param entity entity instance
     * @param format format of the health
     * @return color corresponding to the specified health and maximum health values
     */
    private int getHealthColor(EntityPlayer entity, Format format) {
        // Creates a local instance of the entity's health.
        float health = (entity.getHealth() + entity.getAbsorptionAmount());
        // Creates a local instance of the entity's maximum health.
        float maxHealth = (entity.getMaxHealth() + entity.getAbsorptionAmount());

        // Creates a local instance of the green and red values of the color.
        float green = health / maxHealth;
        float red = 1F - green;

        // Checks if red is less than green.
        if (red > green) {
            // Increments the colors by the absolute value of the red color.
            green += Math.abs(red - 1F);
            red += Math.abs(red - 1F);
        } else {
            // Increments the colors by the absolute value of the green color.
            red += Math.abs(green - 1F);
            green += Math.abs(green - 1F);
        }

        // Checks that the color is within bounds.
        if(red > 255) {
            red = 255;
        }

        // Checks that the color is within bounds.
        if(green > 255) {
            red = 255;
        }

        return new Color(red, green, 0.25F).brighter().getRGB();
    }

    /**
     * Returns a string of health displayed in the
     * specified formatting method.
     *
     * @param format text formatting
     * @param entity entity instance
     * @return string of health displayed in the specified formatting method
     */
    private String getHealthString(EntityPlayer entity, Format format) {
        // Creates a local instance of the entity's health.
        float health = (entity.getHealth() + entity.getAbsorptionAmount()) * format.getMultiplier();

        switch (format) {
            case PERCENT: {
                return String.format("%.1f", health).replace(".0", "") + "%";
            }

            case HEARTS:
            case NUMERIC:
                return String.format(" %s", (int) health);
        }

        return null;
    }

    /**
     * An enum containing different types of
     * formatting for name tag health display.
     *
     * @author Marc
     * @since 8/14/2016
     */
    private enum Format {
        PERCENT(5),
        HEARTS(0.5F),
        NUMERIC(1);

        private float multiplier;

        Format(float multiplier) {
            this.multiplier = multiplier;
        }

        /**
         * Returns the multiplier of the mode.
         *
         * @return multiplier of the mode
         */
        public float getMultiplier() {
            return multiplier;
        }
    }
}
