package pw.finesse.era.module.impl.movement;

import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Property;
import pw.finesse.era.events.game.player.movement.SoulSandSlowdownEvent;
import pw.finesse.era.events.game.player.movement.UseItemSlowdownEvent;
import pw.finesse.era.events.game.player.update.PostMotionUpdateEvent;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * @author Marc
 * @since 8/8/2016
 */
@AutoLoad(Module.class)
public class NoSlowdown extends Toggleable {

    /**
     * Whether to prevent the player from slowing down on soul sand.
     */
    @Property(label = "Soul Sand")
    private boolean soulSand = true;

    public NoSlowdown() {
        super("No Slowdown", Keyboard.KEY_MINUS, Category.MOVEMENT);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                // Checks if the player is blocking.
                if (!event.getPlayer().isBlocking()) {
                    return;
                }

                // Creates a local instance of the player's packet queue.
                NetHandlerPlayClient queue = event.getPlayer().sendQueue;

                // Checks if the player is moving.
                if (event.isMoving(event.getPlayer())) {
                    // Sends a release item packet.
                    queue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, BlockPos.ORIGIN, EnumFacing.DOWN));
                }
            }
        }, new Listener<PostMotionUpdateEvent>() {
            @Override
            public void run(PostMotionUpdateEvent event) {
                // Checks if the player is blocking.
                if (!event.getPlayer().isBlocking()) {
                    return;
                }

                // Creates a local instance of the player's packet queue.
                NetHandlerPlayClient queue = event.getPlayer().sendQueue;

                // Checks if the player is moving.
                if (event.isMoving(event.getPlayer())) {
                    // Sends the use item packet.
                    queue.addToSendQueue(new C08PacketPlayerBlockPlacement(event.getPlayer().getCurrentEquippedItem()));
                }
            }
        }, new Listener<UseItemSlowdownEvent>() {
            @Override
            public void run(UseItemSlowdownEvent event) {
                // Checks if the player is using an item.
                if (event.getPlayer().isUsingItem()) {
                    // Sets the walking speed of the player to the default.
                    event.setWalkSpeed(1.0F);
                }
            }
        }, new Listener<SoulSandSlowdownEvent>() {
            @Override
            public void run(SoulSandSlowdownEvent event) {
                // Sets the cancelled state of the event depending on whether the soul sand setting is enabled.
                event.setCancelled(soulSand);
            }
        });
    }
}
