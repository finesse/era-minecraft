package pw.finesse.era.module.impl.combat;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C03PacketPlayer;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.network.PacketEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * A module that forces criticals hits
 * when attacking other players, meaning
 * more damage is dealt to the opponent.
 *
 * @author Marc
 * @since 8/8/2016
 */
@AutoLoad(Module.class)
public class Criticals extends Toggleable {

    public Criticals() {
        super("Criticals", Keyboard.KEY_C, Category.COMBAT);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PacketEvent>() {
            @Override
            public void run(PacketEvent event) {
                // Checks if the packet is the correct packet.
                if (event.getPacket() instanceof C02PacketUseEntity) {
                    // Creates a local cast of the packet.
                    C02PacketUseEntity packet = (C02PacketUseEntity) event.getPacket();

                    // Checks if the action is an attack.
                    if (packet.getAction() == C02PacketUseEntity.Action.ATTACK) {
                        // Creates a local instance of the player.
                        EntityPlayerSP player = mc.thePlayer;

                        // Creates a local instance of the player's packet queue.
                        NetHandlerPlayClient queue = player.sendQueue;

                        // Checks if the player should perform a critical hit.
                        if (Criticals.this.shouldCrit(player)) {
                            // Performs the critical hit by briefly sending the player off the ground.
                            queue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(player.posX, player.posY + 0.05, player.posZ, false));
                            queue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(player.posX, player.posY, player.posZ, false));
                            queue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(player.posX, player.posY + 0.012511, player.posZ, false));
                            queue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(player.posX, player.posY, player.posZ, false));
                        }
                    }
                }
            }
        });
    }

    /**
     * Returns {@code true} if the player should perform a
     * critical hit, otherwise {@code false}.
     *
     * @param player player instance
     * @return {@code true} if the player should perform a critical hit, otherwise {@code false}
     */
    @SuppressWarnings("RedundantIfStatement")
    private boolean shouldCrit(EntityPlayerSP player) {
        // Checks if the player is on ground.
        if (!player.isCollidedVertically || !player.onGround) {
            return false;
        }

        // Checks if the player is in water.
        if (player.isInWater()) {
            return false;
        }

        // Checks if the player is in lava.
        if (player.isInLava()) {
            return false;
        }

        // Checks if the player is on a ladder.
        if (player.isOnLadder()) {
            return false;
        }

        if (player.ridingEntity != null) {
            return false;
        }

        return true;
    }
}
