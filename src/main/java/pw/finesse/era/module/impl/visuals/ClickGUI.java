package pw.finesse.era.module.impl.visuals;

import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.era.core.Era;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * @author Marc
 * @since 11/12/2016
 */
@AutoLoad(Module.class)
public class ClickGUI extends Toggleable {

    public ClickGUI() {
        super("ClickGUI", Keyboard.KEY_RSHIFT, Category.VISUALS);
        this.setVisible(false);
    }

    @Override
    protected void onEnable() {
        super.onEnable();

        mc.displayGuiScreen(Era.INSTANCE.getGuiClick());
        this.setEnabled(false);
    }

    @Override
    protected void setup() {

    }
}
