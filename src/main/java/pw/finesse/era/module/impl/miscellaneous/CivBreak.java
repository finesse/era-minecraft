package pw.finesse.era.module.impl.miscellaneous;

import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C0APacketAnimation;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.commons.timing.Timer;
import pw.finesse.era.events.game.network.PacketEvent;
import pw.finesse.era.events.game.player.update.PostMotionUpdateEvent;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.events.game.render.world.RenderWorldEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.util.EntityUtil;

import java.util.Random;

import static net.minecraft.network.play.client.C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK;

/**
 * A module that will break constantly
 * re-appearing blocks almost instantly.
 * This is useful for servers such as
 * Shotbow and CivCraft.
 *
 * @author Marc
 * @since 8/8/2016
 */
@AutoLoad(Module.class)
public class CivBreak extends Toggleable {

    /**
     * The range at which the module will break.
     */
    @Property(label = "Range")
    @Clamp(min = 0F, max = 10F)
    private float range = 5F;

    /**
     * The delay between each break.
     */
    @Property(label = "Delay")
    @Clamp(min = 0, max = 1000)
    private int delay = 75;

    /**
     * An instance of the block to break.
     */
    private BlockPos blockPos;

    /**
     * An instance of the facing direction.
     */
    private EnumFacing enumFacing;

    /**
     * An instance of {@link Timer}.
     */
    private Timer timer = new Timer();

    /**
     * An instance of {@link Random}.
     */
    private Random random = new Random();

    public CivBreak() {
        super("Civ Break", Keyboard.KEY_U, Category.MISCELLANEOUS);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                // Checks if the block or the facing state is present.
                if (CivBreak.this.blockPos == null || CivBreak.this.enumFacing == null) {
                    return;
                }

                // Checks if the player is within the required range of the block.
                if (event.getPlayer().getDistance(blockPos.getX(), blockPos.getY(), blockPos.getZ()) <= range) {
                    // Creates a local instance of the required angles to face the block.
                    float[] angles = EntityUtil.calculateAngle(event.getPlayer(), blockPos);

                    // Sets the player's rotation yaw and pitch to the correct angles needed to face the block.
                    event.setRotationYaw(angles[0]);
                    event.setRotationPitch(angles[1]);
                }
            }
        }, new Listener<PostMotionUpdateEvent>() {
            @Override
            public void run(PostMotionUpdateEvent event) {
                // Checks if the block or the facing state is present.
                if (CivBreak.this.blockPos == null || CivBreak.this.enumFacing == null) {
                    return;
                }

                // Checks if the required delay has passed.
                if (timer.sleep(CivBreak.this.delay)) {
                    // Creates a local instance of the player's packet queue.
                    NetHandlerPlayClient queue = event.getPlayer().sendQueue;

                    // Sends the server-sided swinging packet.
                    queue.addToSendQueue(new C0APacketAnimation());

                    // Sends the digging packet.
                    queue.addToSendQueue(new C07PacketPlayerDigging(STOP_DESTROY_BLOCK, CivBreak.this.blockPos, CivBreak.this.enumFacing));

                    timer.reset();
                }
            }
        }, new Listener<PacketEvent>() {
            @Override
            public void run(PacketEvent event) {
                // Checks if the packet is the correct packet.
                if (event.getPacket() instanceof C07PacketPlayerDigging) {
                    // Creates a local cast of the packet;
                    C07PacketPlayerDigging packet = (C07PacketPlayerDigging) event.getPacket();

                    // Checks if the packet is in the correct state.
                    if (packet.getStatus() == STOP_DESTROY_BLOCK) {
                        // Checks if the block and facing state are present.
                        if (CivBreak.this.blockPos == null && CivBreak.this.enumFacing == null) {
                            // Sets the block.
                            CivBreak.this.blockPos = packet.getPosition();

                            // Sets the facing state of the block.
                            CivBreak.this.enumFacing = packet.getFacing();
                        }
                    }
                }
            }
        }, new Listener<RenderWorldEvent>() {
            @Override
            public void run(RenderWorldEvent event) {
//                // Checks if the block or the facing state is present.
//                if (CivBreak.this.blockPos == null || CivBreak.this.enumFacing == null) {
//                    return;
//                }
//
//                // Creates a local instance of where to render the box.
//                double posX = blockPos.getX() - minecraft.getRenderManager().viewerPosX;
//                double posY = blockPos.getY() - minecraft.getRenderManager().viewerPosY;
//                double posZ = blockPos.getZ() - minecraft.getRenderManager().viewerPosZ;
//
//                RenderUtil.beginGl();
//
//                RenderUtil.drawFilledBox(new AxisAlignedBB(posX, posY, posZ, posX + 1, posY + 1, posZ + 1));
//
//                RenderUtil.endGl();
            }
        });
    }

    @Override
    public void onDisable() {
        super.onDisable();

        // Nullifies the block instance.
        this.blockPos = null;

        // Nullifies the facing state.
        this.enumFacing = null;
    }
}