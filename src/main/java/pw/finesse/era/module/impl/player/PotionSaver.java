package pw.finesse.era.module.impl.player;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.network.PacketEvent;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

import java.lang.reflect.Field;

/**
 * A module which freezes the timers of good potion
 * effects when the player is standing still in an
 * attempt to "save" and "preserve" the potions when
 * not in immediate use.
 *
 * @author Marc
 * @since 8/6/2016
 */
@AutoLoad(Module.class)
public class PotionSaver extends Toggleable {

    /**
     * The cached field of the duration of the potion.
     */
    private Field durationCache;

    public PotionSaver() {
        super("Potion Saver", Category.PLAYER);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                // Iterates through the active actions of the player.
                for (Object o : event.getPlayer().getActivePotionEffects()) {
                    // Creates a local cast of the potion.
                    PotionEffect potionEffect = (PotionEffect) o;
                    // Creates a local instance of the potion from the index.
                    Potion potion = Potion.potionTypes[potionEffect.getPotionID()];

                    // Checks if the potion is a bad potion.
                    if (potion.isBadEffect()) {
                        return;
                    }

                    // Checks if the player is moving.
                    if (!event.isMoving(event.getPlayer())) {
                        // Increments the duration of the potion by one, effectively freezing the timer of the potion.
                        PotionSaver.this.setDuration(potionEffect, potionEffect.getDuration() + 1);
                    }
                }
            }
        }, new Listener<PacketEvent>() {
            @Override
            public void run(PacketEvent event) {
                // Checks if the player has any active potions.
                if (mc.thePlayer.getActivePotionEffects().isEmpty()) {
                    return;
                }

                mc.thePlayer.capabilities.allowFlying = true;

                // Checks if the packet is of the correct type.
                if (event.getPacket() instanceof C03PacketPlayer || event.getPacket() instanceof C0BPacketEntityAction || event.getPacket() instanceof C03PacketPlayer.C04PacketPlayerPosition || event.getPacket() instanceof C03PacketPlayer.C05PacketPlayerLook) {
                    // Checks if the player is moving.
                    if (!PotionSaver.this.isMoving(mc.thePlayer)) {
                        // Cancels the event.
                        event.cancel();
                    }
                }
            }
        });
    }

    /**
     * Uses reflections to the set duration of the specified potion.
     *
     * @param potionEffect potion effect
     * @param duration     duration to set
     */
    private void setDuration(PotionEffect potionEffect, int duration) {
        try {
            if (this.durationCache == null) {
                // Creates a local instance of the field.
                Field field = potionEffect.getClass().getDeclaredField("duration");
                // Makes the field accessible.
                field.setAccessible(true);
                // Sets the cache.
                this.durationCache = field;
            }

            // Sets the duration.
            this.durationCache.set(potionEffect, duration);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return {@code true} if the player is moving, otherwise {@code false}.
     *
     * @return {@code true} if the player is moving, otherwise {@code false}
     */
    private boolean isMoving(EntityPlayerSP player) {
        double xDif = player.posX - player.lastTickPosX;
        double yDif = player.posY - player.lastTickPosY;
        double zDif = player.posZ - player.lastTickPosZ;

        return xDif * xDif + yDif * yDif + zDif * zDif > 9.0E-4D;
    }

}
