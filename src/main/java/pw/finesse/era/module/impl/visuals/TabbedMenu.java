package pw.finesse.era.module.impl.visuals;

import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.game.render.ingame.RenderOverlayEvent;
import pw.finesse.era.events.input.KeyInputEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.module.impl.visuals.tabbedmenu.GuiTabbedMenu;

import java.lang.reflect.Field;

/**
 * An in-game menu that allows the user to
 * enable and disable modules on-the-fly
 * using the arrow keys.
 *
 * @author Marc
 * @since 8/18/2016
 */
@AutoLoad(Module.class)
public class TabbedMenu extends Toggleable {

    /**
     * An instance of {@link GuiTabbedMenu}.
     */
    private GuiTabbedMenu tabbedMenu;

    /**
     * The cached field of the watermark property.
     */
    private Field watermarkCache;

    public TabbedMenu() {
        super("Tabbed Menu", Keyboard.KEY_GRAVE, Category.VISUALS);
        this.setVisible(false);
        this.setEnabled(true);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<RenderOverlayEvent>() {
            @Override
            public void run(RenderOverlayEvent event) {
                // Checks if the Minecraft debug overlay is enabled.
                if (event.getMinecraft().gameSettings.showDebugInfo) {
                    return;
                }

                // Checks if the tabbed menu is null.
                if (TabbedMenu.this.tabbedMenu == null) {
                    // Sets up the tabbed menu.
                    (TabbedMenu.this.tabbedMenu = new GuiTabbedMenu()).setup();
                }

                // Creates a local instance of the x and y coordinates to render at.
                int x = 2, y = 2;

                // Creates a local instance of the overlay module.
                Overlay overlay = Era.INSTANCE.getModuleManager().get(Overlay.class);

                // Checks if the overlay module is enabled.
                if (overlay.isEnabled()) {
                    try {
                        // Checks if the cached field is present.
                        if (TabbedMenu.this.watermarkCache == null) {
                            // Creates a local instance of the watermark field.
                            Field field = Overlay.class.getDeclaredField("watermark");
                            // Sets the accessibility of the field.
                            field.setAccessible(true);
                            // Sets the field.
                            TabbedMenu.this.watermarkCache = field;
                        }

                        // Checks if the field is true.
                        if (TabbedMenu.this.watermarkCache.getBoolean(overlay)) {
                            // Increments the y coordinate.
                            y += 10;
                        }
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }

                // Draws the menu.
                TabbedMenu.this.tabbedMenu.draw(x, y);
            }
        }, new Listener<KeyInputEvent>() {
            @Override
            public void run(KeyInputEvent event) {
                // Handles the key input.
                TabbedMenu.this.tabbedMenu.keyInput(event.getKey());
            }
        });
    }
}
