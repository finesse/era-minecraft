package pw.finesse.era.module.impl.visuals.tabbedmenu;

import pw.finesse.commons.management.impl.ListManager;
import pw.finesse.era.core.Era;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.module.impl.visuals.Overlay;
import pw.finesse.era.ui.tab.GuiTab;
import pw.finesse.era.ui.tab.GuiTabButton;
import pw.finesse.era.util.RenderUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.lwjgl.input.Keyboard.*;

/**
 * The main class for the tabbed menu.
 *
 * @author Marc
 * @since 8/19/2016
 */
public class GuiTabbedMenu extends ListManager<GuiTab> {

    /**
     * The interior color of the menu.
     */
    public static final int INTERIOR_COLOR = 0x90000000;

    /**
     * The border color of the menu.
     */
    public static final int BORDER_COLOR = 0xFF000000;

    /**
     * The hovered color of the menu.
     */
    public static final int HOVERED_COLOR = 0xBF6e7e7e;

    /**
     * The cached field of the watermark property in the module {@link Overlay}.
     */
    private Field watermarkCache;

    /**
     * The index of the currently selected tab.
     */
    private int index;

    public GuiTabbedMenu() {
        super(null);
    }

    /**
     * Called when the tabbed menu is set up.
     */
    public void setup() {
        // Checks if the registry is empty.
        if (this.registry == null) {
            // Clears the registry.
            this.registry = new ArrayList<>();

            // Iterates through the categories.
            Arrays.stream(Category.values()).filter(category -> category != Category.NONE).forEach(category -> {
                // Adds the tab to the collection of tabs.
                this.register(new GuiTab(category) {
                    {
                        // Iterates through the contents of the client's module manager and filters out the incorrect types.
                        Era.INSTANCE.getModuleManager().registry().stream().filter(module -> module instanceof Toggleable && module.getCategory() == category).forEach(module -> {
                            // Adds a new button to the menu.
                            this.registry.add(new GuiTabButton((Toggleable) module));
                        });
                    }
                });
            });
        }
    }

    /**
     * Draws the menu at the specified coordinates.
     *
     * @param x x coordinate to draw at
     * @param y y coordinate to draw at
     */
    public void draw(int x, int y) {
        // Draws the menu.
        // Creates a local instance of the offset.
        int offset = y;

        // Creates a local instance of the width of the menu.
        int width = 0;

        // Iterates through the collection of tabs.
        for (GuiTab tab : this.registry) {
            // Creates a local instance of the string width.
            int stringWidth = Era.INSTANCE.getMinecraft().fontRendererObj.getStringWidth(tab.getLabel());

            // Checks if the string width is less than the pre-existing width.
            if (stringWidth < width) {
                continue;
            }

            width = stringWidth + 6;
        }

        // Iterates through the collection of tabs.
        for (GuiTab tab : this.registry) {
            // Draws the tab.
            tab.draw(x, offset, width, offset + 12, this.registry.get(index) == tab);
            offset += 12;
        }

        // Draws a hollow rectangle around the menu.
        RenderUtil.drawHollowRect(x, y, width, offset, 1F, 0xFF000000);

        // Sorts the tabs alphabetically.
        Collections.sort(this.registry, (first, second) -> first.getLabel().compareTo(second.getLabel()));
    }

    /**
     * Called to handle the key input of the menu.
     *
     * @param key key code
     */
    public void keyInput(int key) {
        // Creates a local instance of the currently selected tab.
        GuiTab tab = this.registry.get(index);

        // Checks if the key is the left arrow.
        if (key == KEY_LEFT) {
            // Closes the tab.
            tab.setOpen(false);
        }

        // Checks if the tab is open.
        if (tab.isOpen()) {
            // Runs the key input for the currently selected tab.
            tab.keyInput(key);

            return;
        }

        switch (key) {
            case KEY_UP:
                // Decrements the index.
                this.index--;

                // Makes sure the index doesn't go out of bounds.
                if (this.index < 0) {
                    this.index = this.size() - 1;
                }
                break;

            case KEY_DOWN:
                // Increments the index.
                this.index++;

                // Makes sure the index doesn't go out of bounds.
                if (this.index > this.size() - 1) {
                    this.index = 0;
                }
                break;

            case KEY_RIGHT:
                // Opens the tab.
                tab.setOpen(true);
                break;

        }
    }

}
