package pw.finesse.era.module.impl.visuals;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL32;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Property;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.game.render.world.RenderWorldEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.module.impl.miscellaneous.Friends;
import pw.finesse.era.util.RenderUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Marc
 * @since 8/14/2016
 */
@AutoLoad(Module.class)
public class Tracers extends Toggleable {

    /**
     * Whether to render lines.
     */
    @Property(label = "Lines")
    private boolean lines = true;

    /**
     * Whether to render spines.
     */
    @Property(label = "Spines")
    private boolean spines = false;

    /**
     * Whether to only render tracers for friends.
     */
    @Property(label = "Friends only")
    private boolean friendsOnly = false;

    /**
     * The cached method of the orientation of the camera.
     */
    private Method orientCameraCache;

    public Tracers() {
        super("Tracers", Keyboard.KEY_PERIOD, Category.VISUALS);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<RenderWorldEvent>() {
            @Override
            public void run(RenderWorldEvent event) {
                // Iterates through the list of player entities.
                event.getWorld().playerEntities.forEach(player -> {
                    // Checks if the entity is valid to render.
                    if (Tracers.this.isEntityValid(mc.thePlayer, player)) {
                        // Creates a local instance of the interpolated position of the player.
                        double posX = RenderUtil.interpolate(player.posX, player.lastTickPosX, event.getPartialTicks()) - mc.getRenderManager().viewerPosX;
                        double posY = RenderUtil.interpolate(player.posY, player.lastTickPosY, event.getPartialTicks()) - mc.getRenderManager().viewerPosY;
                        double posZ = RenderUtil.interpolate(player.posZ, player.lastTickPosZ, event.getPartialTicks()) - mc.getRenderManager().viewerPosZ;

                        // Checks if the lines or spines property is enabled.
                        if (Tracers.this.lines || Tracers.this.spines) {
                            // Creates a local instance of the color of the tracers.
                            float[] color = Tracers.this.getColor(mc.thePlayer, player);

                            RenderUtil.beginGl();

                            GlStateManager.loadIdentity();
                            Tracers.this.orientCamera(event.getPartialTicks());

                            GL11.glEnable(GL32.GL_DEPTH_CLAMP);

                            GlStateManager.color(color[0], color[1], color[2], 0.8F);

                            GL11.glBegin(GL11.GL_LINES);

                            if (Tracers.this.lines) {
                                GL11.glVertex3d(0, mc.thePlayer.getEyeHeight(), 0);
                                GL11.glVertex3d(posX, posY, posZ);
                            }

                            if (Tracers.this.spines) {
                                GL11.glVertex3d(posX, posY, posZ);
                                GL11.glVertex3d(posX, posY + player.height + (player.isSneaking() ? -0.2 : 0), posZ);
                            }

                            GL11.glEnd();

                            GL11.glDisable(GL32.GL_DEPTH_CLAMP);

                            RenderUtil.endGl();
                        }
                    }
                });
            }
        });
    }

    /**
     * Returns the corresponding color to the specified entity.
     *
     * @param player player instance
     * @param entity entity instance
     * @return corresponding color to the specified entity
     */
    private float[] getColor(EntityPlayerSP player, EntityPlayer entity) {
        // Checks if the player is a friend.
        if (Era.INSTANCE.getFriendRegistry().get(entity.getCommandSenderName()).isPresent()) {
            if (Era.INSTANCE.getModuleManager().get(Friends.class).isEnabled()) {
                return new float[]{0.3F, 0.75F, 1.0F};
            }

            return new float[]{0.55F, 0.525F, 0.5F};
        }

        // Checks if the player has recently been hurt.
        if (entity.hurtTime > 0) {
            return new float[]{1.0F, 0.75F, 0.3F};
        }

        // Checks if the player is invisible.
        if (entity.isInvisibleToPlayer(player)) {
            return new float[]{0.4F, 0.4F, 0.6F};
        }

        return this.getDistanceColor(player.getDistanceToEntity(entity), 64);
    }

    /**
     * Returns a color corresponding to the specified distance
     * and maximum distance values. This method ensures a
     * gradual and smooth color change, fading from one color
     * to another.
     *
     * @param distance current distance
     * @param maxDistance maximum distance
     * @return color corresponding to the specified distance and maximum distance values
     */
    private float[] getDistanceColor(float distance, float maxDistance) {
        // Creates a local instance of the green and red values of the color.
        float green = distance / maxDistance;
        float red = 1F - green;

        // Checks if red is less than green.
        if (red > green) {
            // Increments the colors by the absolute value of the red color.
            green += Math.abs(red - 1F);
            red += Math.abs(red - 1F);
        } else {
            // Increments the colors by the absolute value of the green color.
            red += Math.abs(green - 1F);
            green += Math.abs(green - 1F);
        }

        return new float[]{ red, green, 0.25F };
    }


    /**
     * Returns {@code true} if the entity is valid to render, otherwise {@code false}.
     *
     * @param entity living entity instance
     * @return {@code true} if the entity is valid to render, otherwise {@code false}
     */
    private boolean isEntityValid(EntityPlayerSP player, EntityPlayer entity) {
        // Checks if the player or the entity is null. In addition, it makes sure the entity isn't the player.
        if (player == null || entity == null || entity == player) {
            return false;
        }

        // Checks if the entity is dead.
        if (entity.isDead || !entity.isEntityAlive() || entity.deathTime > 0) {
            return false;
        }

        // Checks if the friends only property is enabled.
        if (this.friendsOnly) {
            return Era.INSTANCE.getFriendRegistry().get(entity.getCommandSenderName()).isPresent();
        }

        return true;
    }

    /**
     * Invokes the {@link EntityRenderer#orientCamera(float)} method using the specified argument.
     *
     * @param partialTicks partial timer ticks
     */
    private void orientCamera(float partialTicks) {
        try {
            // Checks if the cached method is present.
            if (this.orientCameraCache == null) {
                // Creates a local instance of the method.
                Method method = mc.entityRenderer.getClass().getDeclaredMethod("orientCamera", Float.TYPE);
                // Sets the accessibility of the method.
                method.setAccessible(true);
                // Sets the method.
                this.orientCameraCache = method;
            }

            // Invokes the method using the specified argument.
            this.orientCameraCache.invoke(mc.entityRenderer, partialTicks);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
