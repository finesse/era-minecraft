package pw.finesse.era.module.impl.miscellaneous;

import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.input.KeyInputEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * A module that allows hot keys to be
 * used in order to enable or disable
 * modules.
 *
 * @author Marc
 * @since 8/5/2016
 */
@AutoLoad(Module.class)
public class Keybinds extends Toggleable {

    public Keybinds() {
        super("Keybinds", Category.MISCELLANEOUS);
        this.setEnabled(true);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<KeyInputEvent>() {
            @Override
            public void run(KeyInputEvent event) {
                Era.INSTANCE.getModuleManager().registry().stream().filter(module -> module instanceof Toggleable).forEach(module -> {
                    // Creates a local cast of the module.
                    Toggleable toggleable = (Toggleable) module;

                    // Checks if the key bind of the module matches the pressed key and makes sure the key bind isn't null.
                    if (toggleable.getKeybind() == event.getKey() && toggleable.getKeybind() != Keyboard.KEY_NONE) {
                        // Toggles the module.
                        toggleable.toggle();
                    }
                });
            }
        });
    }
}
