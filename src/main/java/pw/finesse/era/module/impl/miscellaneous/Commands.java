package pw.finesse.era.module.impl.miscellaneous;

import net.minecraft.network.play.client.C01PacketChatMessage;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.game.network.PacketEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

import java.util.Optional;

import static pw.finesse.era.command.CommandManager.COMMAND_PREFIX;

/**
 * @author Marc
 * @since 8/9/2016
 */
@AutoLoad(Module.class)
public class Commands extends Toggleable {

    public Commands() {
        super("Commands", Category.MISCELLANEOUS);
        this.setEnabled(true);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PacketEvent>() {
            @Override
            public void run(PacketEvent event) {
                // Checks if the packet is a client chat packet.
                if (event.getPacket() instanceof C01PacketChatMessage) {
                    // Creates a local cast of the packet.
                    C01PacketChatMessage packet = (C01PacketChatMessage) event.getPacket();

                    // Checks if the message starts with the prefix.
                    if (packet.getMessage().startsWith(COMMAND_PREFIX)) {
                        // Cancels the event.
                        event.cancel();

                        // Creates a local instance of the input from the client.
                        String input = packet.getMessage().substring(COMMAND_PREFIX.length());

                        // Creates an optional container of the corresponding output from the input.
                        Optional<String> outputContainer = Era.INSTANCE.getCommandManager().parse(input);

                        // Checks if the output is present.
                        if (outputContainer.isPresent()) {
                            // Prints the output to the chat.
                            Era.INSTANCE.getCommandManager().print(outputContainer.get());
                        }
                    }
                }
            }
        });
    }

}
