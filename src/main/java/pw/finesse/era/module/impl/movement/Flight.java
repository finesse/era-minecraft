package pw.finesse.era.module.impl.movement;

import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * @author Marc
 * @since 8/22/2016
 */
@AutoLoad(Module.class)
public class Flight extends Toggleable {

    public Flight() {
        super("Flight", Keyboard.KEY_F, Category.MOVEMENT);
    }

    @Override
    protected void setup() {

    }

    @Override
    public void onEnable() {
        super.onEnable();

        mc.thePlayer.capabilities.isFlying = true;
    }

    @Override
    public void onDisable() {
        super.onDisable();

        mc.thePlayer.capabilities.isFlying = false;
    }
}
