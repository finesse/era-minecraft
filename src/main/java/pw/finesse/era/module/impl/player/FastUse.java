package pw.finesse.era.module.impl.player;

import net.minecraft.item.Item;
import net.minecraft.item.ItemSword;
import net.minecraft.network.play.client.C03PacketPlayer;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * @author Marc
 * @since 8/17/2016
 */
@AutoLoad(Module.class)
public class FastUse extends Toggleable {

    /**
     * The time in ticks before the item is used.
     */
    @Property(label = "Threshold")
    @Clamp(min = 0, max = 25)
    private int threshold = 15;

    public FastUse() {
        super("Fast Use", Keyboard.KEY_L, Category.PLAYER);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                // Checks if the player is using an item or is on the ground.
                if (!event.getPlayer().isUsingItem() || !event.isOnGround()) {
                    return;
                }

                // Creates a local instance of the item in use.
                Item item = event.getPlayer().getItemInUse().getItem();

                // Checks if the player is using a sword.
                if (item instanceof ItemSword) {
                    return;
                }

                // Checks if the duration has surpassed the threshold.
                if (event.getPlayer().getItemInUseDuration() > FastUse.this.threshold) {
                    for (int i = 0; i < 20; i++) {
                        // Sends the packet.
                        event.getPlayer().sendQueue.addToSendQueue(new C03PacketPlayer());
                    }
                }
            }
        });
    }
}
