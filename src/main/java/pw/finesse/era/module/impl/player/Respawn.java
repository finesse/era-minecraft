package pw.finesse.era.module.impl.player;

import net.minecraft.network.play.client.C16PacketClientStatus;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.player.death.DeathEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

import static net.minecraft.network.play.client.C16PacketClientStatus.EnumState.PERFORM_RESPAWN;

/**
 * A module that automatically re-spawns
 * in the event that the player dies.
 *
 * @author Marc
 * @since 8/17/2016
 */
@AutoLoad(Module.class)
public class Respawn extends Toggleable {

    public Respawn() {
        super("Respawn", Category.PLAYER);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<DeathEvent>() {
            @Override
            public void run(DeathEvent event) {
                // Checks if the player died.
                if (event.getEntity() == mc.thePlayer) {
                    // Checks the death time so it only re-spawns once.
                    if (event.getDeathTime() == 1) {
                        // Performs the respawn.
                        mc.thePlayer.sendQueue.addToSendQueue(new C16PacketClientStatus(PERFORM_RESPAWN));
                    }
                }
            }
        });
    }
}
