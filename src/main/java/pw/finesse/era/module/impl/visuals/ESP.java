package pw.finesse.era.module.impl.visuals;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Property;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.game.render.entity.RenderPlayerModelEvent;
import pw.finesse.era.events.game.render.world.RenderWorldEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.module.impl.combat.KillAura;
import pw.finesse.era.module.impl.combat.Triggerbot;
import pw.finesse.era.module.impl.miscellaneous.Friends;
import pw.finesse.era.util.RenderUtil;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Marc
 * @since 8/18/2016
 */
@AutoLoad(Module.class)
public class ESP extends Toggleable {

    /**
     * Whether to render ESP outlines.
     */
    @Property(label = "Outline")
    private boolean outline = true;

    /**
     * Whether to render ESP boxes;
     */
    @Property(label = "Box")
    private boolean box = false;

    /**
     * Whether to only render ESPs for friends.
     */
    @Property(label = "Friends only")
    private boolean friendsOnly = false;

    /**
     * A cached method for the model rendering .
     */
    private Method renderModelCache;

    public ESP() {
        super("ESP", Keyboard.KEY_SEMICOLON, Category.VISUALS);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<RenderWorldEvent>() {
            @Override
            public void run(RenderWorldEvent event) {
                // Checks if the box property is enabled.
                if(ESP.this.box) {
                    // Iterates through the list of player entities.
                    event.getWorld().playerEntities.forEach(player -> {
                        // Checks if the entity is valid to render.
                        if (ESP.this.isEntityValid(mc.thePlayer, player)) {
                            // Creates a local instance of the interpolated position of the player.
                            double posX = RenderUtil.interpolate(player.posX, player.lastTickPosX, event.getPartialTicks()) - mc.getRenderManager().viewerPosX;
                            double posY = RenderUtil.interpolate(player.posY, player.lastTickPosY, event.getPartialTicks()) - mc.getRenderManager().viewerPosY;
                            double posZ = RenderUtil.interpolate(player.posZ, player.lastTickPosZ, event.getPartialTicks()) - mc.getRenderManager().viewerPosZ;

                            // Creates a local instance of the interpolated yaw of the player.
                            float yaw = (float) RenderUtil.interpolate(player.rotationYaw, player.prevRotationYaw, event.getPartialTicks());

                            // Creates a local instance of the bounding box to be rendered.
                            AxisAlignedBB boundingBox = AxisAlignedBB.fromBounds(posX - player.width, posY, posZ - player.width, posX + player.width, posY + player.height + (player.isSneaking() ? -0.15D : 0.2D), posZ + player.width);

                            // Contracts the bounding box.
                            boundingBox = boundingBox.contract(0.2D, 0.0D, 0.2D);

                            // Creates a local instance of the color of the ESP.
                            float[] color = ESP.this.getColor(mc.thePlayer, player);

                            RenderUtil.beginGl();

                            GL11.glLineWidth(1.75F);

                            GlStateManager.color(color[0], color[1], color[2], 0.5F);

                            GlStateManager.translate(posX, posY, posZ);
                            GlStateManager.rotate(-yaw, 0.0F, player.height, 0.0F);
                            GlStateManager.translate(-posX, -posY, -posZ);

                            RenderGlobal.drawOutlinedBoundingBox(boundingBox, -1);

                            RenderUtil.drawDiagonalLines(boundingBox);

                            RenderUtil.endGl();
                        }
                    });
                }
            }
        }, new Listener<RenderPlayerModelEvent>() {
            @Override
            public void run(RenderPlayerModelEvent event) {
                // Checks if the outline property is enabled.
                if (ESP.this.outline) {
                    // Checks if the entity is valid.
                    if (ESP.this.isEntityValid(mc.thePlayer, event.getEntity())) {
                        // Creates a local instance of the color of the ESP.
                        float[] color = ESP.this.getColor(mc.thePlayer, event.getEntity());

                        ESP.this.renderModel(event.getCaller(), event.getEntity(), event.getLimbSwing(), event.getLimbSwingAmount(), event.getTicksExisted(), event.getRotationYawHead(), event.getRotationPitch(), 0.0625F);
                        RenderUtil.initializeOutlineProcess();
                        GlStateManager.color(color[0], color[1], color[2], 0.75F);
                        ESP.this.renderModel(event.getCaller(), event.getEntity(), event.getLimbSwing(), event.getLimbSwingAmount(), event.getTicksExisted(), event.getRotationYawHead(), event.getRotationPitch(), 0.0625F);
                        RenderUtil.beginStencilOutline();
                        ESP.this.renderModel(event.getCaller(), event.getEntity(), event.getLimbSwing(), event.getLimbSwingAmount(), event.getTicksExisted(), event.getRotationYawHead(), event.getRotationPitch(), 0.0625F);
                        RenderUtil.continueStencilOutline();
                        ESP.this.renderModel(event.getCaller(), event.getEntity(), event.getLimbSwing(), event.getLimbSwingAmount(), event.getTicksExisted(), event.getRotationYawHead(), event.getRotationPitch(), 0.0625F);
                        RenderUtil.finalizeStencilOutline();
                        ESP.this.renderModel(event.getCaller(), event.getEntity(), event.getLimbSwing(), event.getLimbSwingAmount(), event.getTicksExisted(), event.getRotationYawHead(), event.getRotationPitch(), 0.0625F);
                        RenderUtil.endOutlineProcess();
                    }
                }
            }
        });
    }

    /**
     * Returns the corresponding color to the specified entity.
     *
     * @param player player instance
     * @param entity entity instance
     * @return corresponding color to the specified entity
     */
    private float[] getColor(EntityPlayerSP player, EntityPlayer entity) {
        // Checks if the player is a friend.
        if (Era.INSTANCE.getFriendRegistry().get(entity.getCommandSenderName()).isPresent()) {
            // Checks if the friends module is enabled.
            if (Era.INSTANCE.getModuleManager().get(Friends.class).isEnabled()) {
                return new float[]{0.3F, 0.75F, 1.0F};
            }

            return new float[]{0.55F, 0.525F, 0.5F};
        }

        // Checks if the entity is a target of attack.
        if (entity == this.getTarget(Era.INSTANCE.getModuleManager().get(KillAura.class)) || entity == this.getTarget(Era.INSTANCE.getModuleManager().get(Triggerbot.class))) {
            return new float[]{1, 0.2F, 0.3F};
        }

        // Checks if the player has recently been hurt.
        if (entity.hurtTime > 0) {
            return new float[]{1.0F, 0.75F, 0.3F};
        }

        // Checks if the player is invisible.
        if (entity.isInvisibleToPlayer(player)) {
            return new float[]{0.4F, 0.4F, 0.6F};
        }

        return this.getHealthColor(entity);
    }

    /**
     * Returns {@code true} if the entity is valid to render, otherwise {@code false}.
     *
     * @param entity living entity instance
     * @return {@code true} if the entity is valid to render, otherwise {@code false}
     */
    @SuppressWarnings("RedundantIfStatement")
    private boolean isEntityValid(EntityPlayerSP player, EntityPlayer entity) {
        // Checks if the player or the entity is null. In addition, it makes sure the entity isn't the player.
        if (player == null || entity == null || entity == player) {
            return false;
        }

        // Checks if the entity is dead.
        if (entity.isDead || !entity.isEntityAlive() || entity.deathTime > 0) {
            return false;
        }

        // Checks if the friends only property is enabled.
        if (this.friendsOnly) {
            return Era.INSTANCE.getFriendRegistry().get(entity.getCommandSenderName()).isPresent();
        }

        return true;
    }

    /**
     * Returns a color corresponding to the specified health
     * and maximum health values. This method ensures a
     * gradual and smooth color change, fading from one
     * color to another.
     *
     * @param entity entity instance
     * @return color corresponding to the specified health and maximum health values
     */
    private float[] getHealthColor(EntityPlayer entity) {
        // Creates a local instance of the entity's health.
        float health = (entity.getHealth() + entity.getAbsorptionAmount());
        // Creates a local instance of the entity's maximum health.
        float maxHealth = (entity.getMaxHealth() + entity.getAbsorptionAmount());

        // Creates a local instance of the green and red values of the color.
        float green = health / maxHealth;
        float red = 1F - green;

        // Checks if red value is less than green.
        if (red > green) {
            // Increments the colors by the absolute value of the red color.
            green += Math.abs(red - 1F);
            red += Math.abs(red - 1F);
        } else {
            // Increments the colors by the absolute value of the green color.
            red += Math.abs(green - 1F);
            green += Math.abs(green - 1F);
        }

        return new float[]{red, green, 0.25F};
    }

    /**
     * Invokes the {@link RendererLivingEntity#doRender(EntityLivingBase, double, double, double, float, float)} method using the specified arguments.
     *
     * @param caller class called from
     * @param entity player to render
     * @param limbSwing limb swing time
     * @param limbSwingAmount limb swing amount
     * @param ticksExisted age in ticks
     * @param rotationYawHead yaw of the head
     * @param rotationPitch pitch of the player
     * @param offset render offset
     */
    private void renderModel(RendererLivingEntity caller, EntityPlayer entity, float limbSwing, float limbSwingAmount, float ticksExisted, float rotationYawHead, float rotationPitch, float offset) {
        try {
            // Checks if the cahced method is present.
            if (this.renderModelCache == null) {
                // Creates a local instance of the method.
                Method method = caller.getClass().getSuperclass().getDeclaredMethod("renderModel", EntityLivingBase.class, Float.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, Float.TYPE);
                // Sets the accessibility of the method.
                method.setAccessible(true);
                // Sets the method.
                this.renderModelCache = method;
            }

            // Invokes the method.
            this.renderModelCache.invoke(caller, entity, limbSwing, limbSwingAmount, ticksExisted, rotationYawHead, rotationPitch, offset);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Returns the target of the attack the specified module.
     *
     * @param module module instance
     * @return target of attack
     */
    private EntityLivingBase getTarget(Module module) {
        try {
            // Creates a local instance of the target field.
            Field field = module.getClass().getDeclaredField("target");
            // Sets the accessibility of the field.
            field.setAccessible(true);

            return (EntityLivingBase) field.get(module);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
