package pw.finesse.era.module.impl.movement;

import net.minecraft.util.MathHelper;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

import java.lang.reflect.Field;

@AutoLoad(Module.class)
public class Speed extends Toggleable {

    private boolean cancel;

    private Field timerCache;

    public Speed() {
        super("Speed", Keyboard.KEY_H, Category.MOVEMENT);
    }

    @Override
    protected void onDisable() {
        super.onDisable();

        getTimer().timerSpeed = 1F;
    }

    @Override
    protected void setup() {
        registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                if (mc.thePlayer.moveForward == 0 && mc.thePlayer.moveStrafing == 0 || mc.theWorld == null) {
                    getTimer().timerSpeed = 1;
                    return;
                }

                if(mc.thePlayer.onGround) {
                    getTimer().timerSpeed = 1.8f;
                    mc.thePlayer.motionY = 0.4f;
                    final float var1 = mc.thePlayer.rotationYaw * 0.017453292F;
                    cancel = !cancel;
                    mc.thePlayer.motionX -= (double) (MathHelper.sin(var1) * (cancel ? 0.24f : 0.233F));
                    mc.thePlayer.motionZ += (double) (MathHelper.cos(var1) * (cancel ? 0.24f : 0.233F));

                    mc.thePlayer.isAirBorne = true;
                } else if(mc.thePlayer.isAirBorne){
                    if(getTimer().timerSpeed == 1.2f) {
                        getTimer().timerSpeed = 1.0f;
                    }
                    if(getTimer().timerSpeed == 1.8f) {
                        getTimer().timerSpeed = 1.2f;
                    }
                }
            }
        });
    }

    /**
     * Uses reflection to return the timer of the game.
     *
     * @return timer of the game
     */
    private net.minecraft.util.Timer getTimer() {
        try {
            if (this.timerCache == null) {
                // Creates a local instance of the timer field.
                Field field = mc.getClass().getDeclaredField("timer");
                // Makes the field accessible.
                field.setAccessible(true);
                // Sets the cache of the timer.
                this.timerCache = field;
            }

            return (net.minecraft.util.Timer) this.timerCache.get(mc);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

}