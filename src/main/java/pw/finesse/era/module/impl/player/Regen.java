package pw.finesse.era.module.impl.player;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.network.play.client.C03PacketPlayer;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * A module that allows the player to regenerate
 * health faster than normal by sending excessive
 * packets.
 *
 * @author Marc
 * @since 8/22/2016
 */
@AutoLoad(Module.class)
public class Regen extends Toggleable {

    /**
     * The number of hearts at which the module should start to regenerate health.
     */
    @Property(label = "Threshold")
    @Clamp(min = 0, max = 10)
    private float threshold = 8.5F;

    /**
     * The number of packets to send.
     */
    @Property(label = "Packets")
    @Clamp(min = 0, max = 1000)
    private int packets = 15;

    public Regen() {
        super("Regen", Keyboard.KEY_EQUALS, Category.MISCELLANEOUS);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                // Checks if the player should regen.
                if (Regen.this.shouldRegen(event.getPlayer())) {
                    int i = 0;

                    while(i < Regen.this.packets) {
                        // Sends the packet.
                        event.getPlayer().sendQueue.addToSendQueue(new C03PacketPlayer());

                        // Increments the index.
                        i++;
                    }
                }
            }
        });
    }

    /**
     * Returns {@code true} if the player should regenerate health, otherwise {@code false}.
     *
     * @param player player instance
     * @return {@code true} if the player should regenerate health, otherwise {@code false}
     */
    private boolean shouldRegen(EntityPlayerSP player) {
        // Checks if the player is off the ground in any way.
        if (!player.onGround) {
            return false;
        }

        // Checks if the player's hunger is full enough to regenerate health.
        if (player.getFoodStats().getFoodLevel() < 19) {
            return false;
        }

        // Checks if the health is above the threshold.
        if (player.getHealth() > this.threshold * 2) {
            return false;
        }

        return true;
    }
}
