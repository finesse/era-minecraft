package pw.finesse.era.module.impl.miscellaneous;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Property;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.input.MouseInputEvent;
import pw.finesse.era.friend.Friend;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.util.EntityUtil;

import java.util.Optional;

/**
 * @author Marc
 * @since 8/7/2016
 */
@AutoLoad(Module.class)
public class Friends extends Toggleable {

    @Property(label = "Name Protect")
    private boolean nameProtect = true;

    @Property(label = "Dash Name")
    private boolean dashName = true;

    @Property(label = "Middle click to add")
    private boolean middleClick = true;

    public Friends() {
        super("Friends", Category.MISCELLANEOUS);
        this.setEnabled(true);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<MouseInputEvent>() {
            @Override
            public void run(MouseInputEvent event) {
                // Checks if the middle click property is enabled.
                if (middleClick) {
                    // Checks if the event button is a middle click.
                    if (event.getButton() == 2) {
                        // Creates an optional container of the ray-traced entity.
                        Optional<EntityLivingBase> entityContainer = EntityUtil.rayTrace(256, 1.0F);

                        // Checks if the entity container is present and a player entity.
                        if (entityContainer.isPresent() && entityContainer.get() instanceof EntityPlayer) {
                            // Creates an optional container of the friend.
                            Optional<Friend> friendContainer = Era.INSTANCE.getFriendRegistry().get(entityContainer.get().getCommandSenderName());

                            // Checks if the container is present.
                            if (friendContainer.isPresent()) {
                                // Unregisters the player from the friend registry.
                                Era.INSTANCE.getFriendRegistry().unregister(friendContainer.get());
                            } else {
                                // Creates and registers a new friend object of the player.
                                Era.INSTANCE.getFriendRegistry().register(new Friend(entityContainer.get().getCommandSenderName()));
                            }
                        }
                    }
                }
            }
        });
    }
}
