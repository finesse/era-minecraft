package pw.finesse.era.module.impl.player;

import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * @author Marc
 * @since 8/17/2016
 */
@AutoLoad(Module.class)
public class Zoot extends Toggleable {

    public Zoot() {
        super("Zoot", Keyboard.KEY_O, Category.PLAYER);
    }

    @Override
    protected void setup() {

    }
}
