package pw.finesse.era.module.impl.miscellaneous;

import net.minecraft.network.play.client.C0DPacketCloseWindow;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.network.PacketEvent;
import pw.finesse.era.events.game.player.container.ContainerClosedEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * A module that allows for the player to
 * carry more items in their inventory.
 *
 * @author Marc
 * @since 8/17/2016
 */
@AutoLoad(Module.class)
public class MoreInventory extends Toggleable {

    public MoreInventory() {
        super("More Inventory", Category.MISCELLANEOUS);
        this.setVisible(false);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PacketEvent>() {
            @Override
            public void run(PacketEvent event) {
                // Checks if the packet is of the correct type.
                if (event.getPacket() instanceof C0DPacketCloseWindow) {
                    // Cancels the event.
                    event.cancel();
                }
            }
        }, new Listener<ContainerClosedEvent>() {
            @Override
            public void run(ContainerClosedEvent event) {
                // Checks if the player is of the correct type.
                if (event.getPlayer() == mc.thePlayer) {
                    // Cancels the event.
                    event.cancel();
                }
            }
        });
    }

    @Override
    public void onDisable() {
        super.onDisable();

        // Sends the close window packet.
        mc.thePlayer.sendQueue.addToSendQueue(new C0DPacketCloseWindow());
    }
}
