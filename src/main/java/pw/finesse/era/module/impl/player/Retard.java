package pw.finesse.era.module.impl.player;

import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;

/**
 * @author Marc
 * @since 8/17/2016
 */
@AutoLoad(Module.class)
public class Retard extends Toggleable {

    public Retard() {
        super("Retard", Category.PLAYER);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                event.setRotationYaw(mc.thePlayer.rotationYaw + 10);
                event.setRotationPitch(-180);
            }
        });
    }
}
