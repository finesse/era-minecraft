package pw.finesse.era.module.impl.combat;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.commons.randomization.Randomizer;
import pw.finesse.commons.timing.Timer;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.game.player.update.PostMotionUpdateEvent;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.module.impl.miscellaneous.Friends;
import pw.finesse.era.util.EntityUtil;

/**
 * A module that automatically aims at
 * and attacks other entities.
 *
 * @author Marc
 * @since 8/6/2016
 */
@AutoLoad(Module.class)
public class KillAura extends Toggleable {

    /**
     * The speed of attack in attacks per second.
     */
    @Property(label = "Speed")
    @Clamp(min = 0, max = 25)
    private float speed = 10F;

    /**
     * The range of attack in blocks.
     */
    @Property(label = "Range")
    @Clamp(min = 0D, max = 10D)
    private float range = 4F;

    /**
     * The FOV of attack in degrees.
     */
    @Property(label = "FOV")
    @Clamp(min = 0, max = 360)
    private int fov = 360;

    /**
     * Whether to attack player entities.
     */
    @Property(label = "Players")
    private boolean players = true;

    /**
     * Whether to attack hostile entities.
     */
    @Property(label = "Hostiles")
    private boolean hostiles = false;

    /**
     * Whether to attack passive entities.
     */
    @Property(label = "Passives")
    private boolean passives = false;

    /**
     * Whether to attack teamed entities.
     */
    @Property(label = "Team")
    private boolean team = false;

    /**
     * Whether to attack naked or non-armored players.
     */
    @Property(label = "Naked")
    private boolean naked = false;

    /**
     * Whether to attack invisible entities.
     */
    @Property(label = "Invisibles")
    private boolean invisibles = false;

    /**
     * An instance of {@link EntityLivingBase}.
     */
    private EntityLivingBase target;

    /**
     * An instance of {@link Timer}.
     */
    private Timer timer = new Timer();

    /**
     * An instance of {@link Randomizer}.
     */
    private Randomizer randomizer = new Randomizer();

    public KillAura() {
        super("Kill Aura", Keyboard.KEY_V, Category.COMBAT);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                // Instantiates the living entity instance.
                KillAura.this.target = EntityUtil.getClosestEntityToCursor(event.getPlayer(), 360, KillAura.this.range);

                // Checks if the target is null.
                if (KillAura.this.target == null) {
                    return;
                }

                // Checks if the entity is a valid target.
                if (KillAura.this.isEntityValid(event.getPlayer(), KillAura.this.target, KillAura.this.range)) {
                    // Checks if the entity is within the proper field of view.
                    if (EntityUtil.isEntityWithinFieldOfView(event.getPlayer(), KillAura.this.target, KillAura.this.fov)) {
                        // Creates a local instance of the required angles.
                        float[] angles = EntityUtil.calculateAngle(event.getPlayer(), KillAura.this.target);

                        // Sets the player's rotation yaw and pitch to the correct angles needed to face the target.
                        event.setRotationYaw(angles[0]);
                        event.setRotationPitch(angles[1]);
                    }
                } else {
                    // Nullifies the target instance.
                    KillAura.this.target = null;
                }
            }
        }, new Listener<PostMotionUpdateEvent>() {
            @Override
            public void run(PostMotionUpdateEvent event) {
                // Checks if the target is null;
                if (KillAura.this.target == null) {
                    return;
                }

                // Checks if the entity is a valid target.
                if (KillAura.this.isEntityValid(event.getPlayer(), KillAura.this.target, KillAura.this.range)) {
                    // Checks if the entity is within the proper field of view.
                    if (EntityUtil.isEntityWithinFieldOfView(event.getPlayer(), KillAura.this.target, KillAura.this.fov)) {
                        // Creates a local instance of the minimum and maximum fluctuation.
                        int floor = -randomizer.nextInt(30);
                        int ceiling = randomizer.nextInt(30);

                        // Creates a local instance of the random factor of the delay.
                        int factor = randomizer.nextInt(floor, ceiling);

                        // Creates a local instance of the speed in milliseconds.
                        int milliseconds = (int) (1000 / KillAura.this.speed);

                        // Checks if the required delay has passed.
                        if (timer.sleep(milliseconds + factor)) {
                            // Performs an attack.
                            KillAura.this.performAttack(event.getPlayer(), KillAura.this.target);

                            timer.reset();
                        }
                    }
                } else {
                    // Nullifies the target instance.
                    KillAura.this.target = null;
                }
            }
        });
    }

    @Override
    public void onDisable() {
        super.onDisable();

        // Nullifies the target instance.
        this.target = null;
    }

    /**
     * Performs an attack on the specified entity.
     *
     * @param player player instance
     * @param entity entity instance
     */
    private void performAttack(EntityPlayerSP player, Entity entity) {
        // Swings the player's item.
        player.swingItem();

        // Checks if the player is blocking.
        if (player.isBlocking()) {
            // Sends the stop using item packet.
            player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, BlockPos.ORIGIN, EnumFacing.DOWN));
        }

        // Sends an attack packet.
        player.sendQueue.addToSendQueue(new C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK));

        // Checks if the player is blocking.
        if (player.isBlocking()) {
            // Sends the start using item packet.
            player.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(player.getCurrentEquippedItem()));
        }
    }

    /**
     * Returns if the entity is valid to attack.
     *
     * @param player player instance
     * @param entity entity instance
     * @param range  range to check within
     * @return if the entity is valid to attack
     */
    private boolean isEntityValid(EntityPlayerSP player, EntityLivingBase entity, float range) {
        // Checks if the player or the entity is null. In addition, it makes sure the entity isn't the player.
        if (player == null || entity == null || entity == player) {
            return false;
        }

        // Checks if the entity is dead.
        if (entity.isDead || !entity.isEntityAlive() || entity.deathTime > 0) {
            return false;
        }

        // Checks if the entity has existed for the proper time.
        if (entity.ticksExisted < 10) {
            return false;
        }

        // Checks if the entity's name is populated.
        if (entity.getCommandSenderName().isEmpty() || entity.getCommandSenderName().contains(" ")) {
            return false;
        }

        // Checks if the entity is within the specified range.
        if (player.getDistanceToEntity(entity) > range) {
            return false;
        }

        // Checks if the entity is of the proper type.
        if (!this.isProperType(player, entity)) {
            return false;
        }

        // Checks if the entity is invisible.
        if(!this.invisibles && entity.isInvisibleToPlayer(player)) {
            return false;
        }

        return true;
    }

    /**
     * Returns {@code true} if the entity is of the correct type, otherwise {@code false}.
     *
     * @param player player instance
     * @param entity entity instance
     * @return {@code true} if the entity is of the correct type, otherwise {@code false}
     */
    private boolean isProperType(EntityPlayerSP player, EntityLivingBase entity) {
        // Checks if the entity is a player entity.
        if(this.players && entity instanceof EntityPlayer) {
            // Checks if the Friends module is enabled, and if the entity is a friend.
            if (Era.INSTANCE.getModuleManager().get(Friends.class).isEnabled() && Era.INSTANCE.getFriendRegistry().get(entity.getCommandSenderName()).isPresent()) {
                return false;
            }

            // Checks the armor value of the player to make sure no armor is being worn.
            if(!this.naked && entity.getTotalArmorValue() == 0) {
                return false;
            }

            // Checks if the player is on the same team.
            if(this.team && entity.isOnSameTeam(player)) {
                return false;
            }

            return true;
        }

        // Checks if the entity is a hostile entity.
        if(this.hostiles && entity instanceof IMob) {
            return true;
        }

        // Checks if the entity is a passive entity.
        if(this.passives && entity instanceof IAnimals) {
            return true;
        }

        return false;
    }
}