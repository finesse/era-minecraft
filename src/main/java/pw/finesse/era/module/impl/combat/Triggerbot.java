package pw.finesse.era.module.impl.combat;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemSword;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.property.Clamp;
import pw.finesse.commons.property.Property;
import pw.finesse.commons.randomization.Randomizer;
import pw.finesse.commons.timing.Timer;
import pw.finesse.era.core.Era;
import pw.finesse.era.events.game.player.update.PreMotionUpdateEvent;
import pw.finesse.era.module.Category;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.module.impl.miscellaneous.Friends;
import pw.finesse.era.util.EntityUtil;

import java.awt.*;
import java.lang.reflect.Field;
import java.util.Optional;

/**
 * A module that will automatically attack
 * other players when the user is within
 * a specified range and is aiming within
 * the hit-box.
 *
 * @author Marc
 * @since 8/16/2016
 */
@AutoLoad(Module.class)
public class Triggerbot extends Toggleable {

    /**
     * The speed at which the trigger bot will attack, in clicks per second.
     */
    @Property(label = "Speed")
    @Clamp(min = 0, max = 20)
    private float speed = 8F;

    /**
     * The range at which the trigger bot will attack, in blocks.
     */
    @Property(label = "Range")
    @Clamp(min = 0, max = 10F)
    private float range = 4.0F;

    /**
     * The rate at which the speed will fluctuate.
     */
    @Property(label = "Fluctuation")
    @Clamp(min = 0, max = 50)
    private int fluctuation = 25;

    /**
     * The ticks the client will wait before attacking a new entity.
     */
    @Property(label = "Ticks Existed")
    @Clamp(min = 0, max = 100)
    private int ticksExisted = 25;

    /**
     * Whether to attack players on your team.
     */
    @Property(label = "Team")
    private boolean team = false;

    /**
     * Whether to attack naked or non-armored players.
     */
    @Property(label = "Naked")
    private boolean naked = false;

    /**
     * Whether to attack invisible entities.
     */
    @Property(label = "Invisibles")
    private boolean invisibles = false;

    /**
     * Whether to disable the module on death.
     */
    @Property(label = "Disable on Death")
    private boolean disableOnDeath = false;

    /**
     * Whether to only attack when holding a weapon.
     */
    @Property(label = "Weapon Only")
    private boolean weaponOnly = true;

    /**
     * An instance of the target of attack.
     */
    private EntityLivingBase target;

    /**
     * An instance of {@link Timer}.
     */
    private Timer timer = new Timer();

    /**
     * An instance of {@link Randomizer}.
     */
    private Randomizer randomizer = new Randomizer();

    /**
     * An instance of {@link Robot}.
     */
    private Robot robot = new Robot();

    /**
     * A cached field of {@link Timer}.
     */
    private Field timerCache;

    public Triggerbot() throws AWTException {
        super("Triggerbot", Keyboard.KEY_X, Category.COMBAT);
    }

    @Override
    protected void setup() {
        this.registerListeners(new Listener<PreMotionUpdateEvent>() {
            @Override
            public void run(PreMotionUpdateEvent event) {
                Optional<EntityLivingBase> entityContainer = EntityUtil.rayTrace(Triggerbot.this.range, Triggerbot.this.getTimer().renderPartialTicks);

                if (!entityContainer.isPresent()) {
                    target = null;
                    return;
                }

                // Creates a local instance of the target.
                Triggerbot.this.target = entityContainer.get();

                // Checks if the target is a player.
                if (Triggerbot.this.target instanceof EntityPlayer) {
                    // Creates a local cast of the player.
                    EntityPlayer player = (EntityPlayer) Triggerbot.this.target;

                    // Checks if the entity is valid to attack.
                    if (Triggerbot.this.isEntityValid(event.getPlayer(), player)) {
                        // Checks if the player should attack.
                        if (Triggerbot.this.shouldAttack(event.getPlayer())) {
                            // Creates a local instance of the minimum and maximum fluctuation.
                            int floor = -randomizer.nextInt(Triggerbot.this.fluctuation);
                            int ceiling = randomizer.nextInt(Triggerbot.this.fluctuation);

                            // Creates a local instance of the random factor of the delay.
                            int factor = randomizer.nextInt(floor, ceiling);

                            // Creates a local instance of the speed in milliseconds.
                            int milliseconds = (int) (1000 / Triggerbot.this.speed);

                            if (timer.sleep(milliseconds + factor)) {
                                // Performs the attack.
                                Triggerbot.this.performAttack(event.getPlayer(), player);

                                timer.reset();
                            }
                        }
                    } else {
                        Triggerbot.this.target = null;
                    }
                }
            }
        });
    }

    @Override
    public void onDisable() {
        super.onDisable();

        // Nullifies the target instance.
        this.target = null;
    }


    /**
     * Performs an attack on the specified entity.
     *
     * @param player player instance
     * @param entity entity instance
     */
    private void performAttack(EntityPlayerSP player, EntityPlayer entity) {
        // Swings the player's item.
        player.swingItem();

        // Checks if the player is blocking.
        if (player.isBlocking()) {
            // Sends the stop using item packet.
            player.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, BlockPos.ORIGIN, EnumFacing.DOWN));
        }

        // Sends an attack packet.
        player.sendQueue.addToSendQueue(new C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK));

        // Checks if the player is blocking.
        if (player.isBlocking()) {
            // Sends the start using item packet.
            player.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(player.getCurrentEquippedItem()));
        }
    }


    /**
     * Returns if the entity is valid to attack.
     *
     * @param player player instance
     * @param entity entity instance
     * @return if the entity is valid to attack
     */
    private boolean isEntityValid(EntityPlayerSP player, EntityPlayer entity) {
        // Checks if the player or the entity is null. In addition, it makes sure the entity isn't the player.
        if (player == null || entity == null || entity == player) {
            return false;
        }

        // Checks if the entity is dead.
        if (entity.isDead || !entity.isEntityAlive() || entity.deathTime > 0) {
            return false;
        }

        // Checks if the entity has existed for the proper time.
        if (entity.ticksExisted < this.ticksExisted) {
            return false;
        }

        // Checks if the entity's name is populated.
        if (entity.getCommandSenderName().isEmpty() || entity.getCommandSenderName().contains(" ")) {
            return false;
        }

        // Checks if the entity is within the specified range.
        if (player.getDistanceToEntity(entity) > this.range) {
            return false;
        }

        // Checks if the Friends module is enabled, and if the entity is a friend.
        if (Era.INSTANCE.getModuleManager().get(Friends.class).isEnabled() && Era.INSTANCE.getFriendRegistry().get(entity.getCommandSenderName()).isPresent()) {
            return false;
        }

        // Checks if the entity is on the same team as the player.
        if (!this.team && ((EntityPlayer) entity).isOnSameTeam(player)) {
            return false;
        }

        // Checks if the entity is invisible.
        if (!this.invisibles && entity.isInvisibleToPlayer(player)) {
            return false;
        }

        // Checks the armor value of the player to make sure no armor is being worn.
        if (!this.naked && entity.getTotalArmorValue() == 0) {
            return false;
        }

        return true;
    }

    /**
     * Returns {@code true} if the player should attack, otherwise {@code false}.
     *
     * @param player player instance
     * @return {@code true} if the player should attack, otherwise {@code false}
     */
    private boolean shouldAttack(EntityPlayerSP player) {
        // Checks if the player is null.
        if (player == null) {
            return false;
        }

        // Checks if the player is dead in any way.
        if (player.isDead || !player.isEntityAlive() || player.deathTime > 0) {
            return false;
        }

        // Checks if the player can swing.
        if (!this.canSwing(player)) {
            return false;
        }

        // Checks if the game is in focus.
        if (mc.currentScreen != null || !mc.inGameHasFocus) {
            return false;
        }

        return true;
    }

    /**
     * Returns {@code true} if the player is holding a sword or an axe, otherwise {@code false}.
     *
     * @param player player instance
     * @return {@code true} if the player is holding a sword or an axe, otherwise {@code false}
     */
    private boolean canSwing(EntityPlayerSP player) {
        // Checks if the weapon only property is enabled.
        if (this.weaponOnly) {
            // Checks if the current held item is null.
            if (player != null && player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem() != null) {
                // Creates a local instance of the player's held item.
                Item item = player.getCurrentEquippedItem().getItem();

                // Checks if the item is a sword or a axe.
                if (item instanceof ItemSword || item instanceof ItemAxe) {
                    return true;
                }
            }

            return false;
        }

        return true;
    }

    /**
     * Uses reflection to return the timer of the game.
     *
     * @return timer of the game
     */
    private net.minecraft.util.Timer getTimer() {
        try {
            if (this.timerCache == null) {
                // Creates a local instance of the timer field.
                Field field = mc.getClass().getDeclaredField("timer");
                // Makes the field accessible.
                field.setAccessible(true);
                // Sets the cache of the timer.
                this.timerCache = field;
            }

            return (net.minecraft.util.Timer) this.timerCache.get(mc);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
