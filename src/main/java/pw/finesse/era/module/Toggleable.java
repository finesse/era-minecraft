package pw.finesse.era.module;

import org.lwjgl.input.Keyboard;
import pw.finesse.era.core.Era;

/**
 * This class is an extension of {@link Module} and is a
 * toggleable variant. Extensions of this class will create
 * toggleable in-game modifications. In addition, this class
 * is an implementation of the {@link Toggleable} interface.
 *
 * @author Marc
 * @since 8/3/2016
 */
public abstract class Toggleable extends Module implements pw.finesse.commons.interfaces.Toggleable {

    /**
     * The data of the module displayed graphically.
     */
    private String data;

    /**
     * The key bind of the module. When this key is pressed, this will toggle the enabled state of the module.
     */
    private int keybind;

    /**
     * The visibility of the module.
     */
    private boolean visible;

    /**
     * The enabled state of the module.
     */
    private boolean enabled;

    public Toggleable(String label, int keybind, Category category) {
        super(label, category);

        this.data = null;
        this.keybind = keybind;
        this.visible = true;
    }

    public Toggleable(String label, Category category) {
        this(label, Keyboard.KEY_NONE, category);
    }

    /**
     * Returns the data of the module.
     *
     * @return data of the module
     */
    public String getData() {
        return data;
    }

    /**
     * Sets the data of the module to the specified data.
     *
     * @param data data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * Returns the key bind of the module.
     *
     * @return key bind of the module.
     */
    public int getKeybind() {
        return keybind;
    }

    /**
     * Sets the key bind of the module to the specified key bind.
     *
     * @param keybind key bind to set
     */
    public void setKeybind(int keybind) {
        this.keybind = keybind;
    }

    /**
     * Returns the visibility of the module.
     *
     * @return visibility of the module
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Sets the visibility of the module to the specified visibility.
     *
     * @param visible visibility to set
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;

        if (this.enabled) {
            this.onEnable();
        } else {
            this.onDisable();
        }

        this.onToggle();
    }

    @Override
    public void toggle() {
        this.setEnabled(!this.isEnabled());

        // Removes the module from the client's collection of modules.
        Era.INSTANCE.getModuleManager().registry().remove(this);
        // Adds the module to client's collection of modules at the first index.
        Era.INSTANCE.getModuleManager().registry().add(this);
    }

    /**
     * Called when the module is enabled.
     */
    protected void onEnable() {
        // Registers the listeners for the module.
        this.listeners.forEach(listener -> Era.INSTANCE.getListenerRegistry().register(listener));
    }

    /**
     * Called when the module is disabled.
     */
    protected void onDisable() {
        // Unregisters the listeners for the module.
        this.listeners.forEach(listener -> Era.INSTANCE.getListenerRegistry().unregister(listener));
    }

    /**
     * Called when the module is toggled.
     */
    protected void onToggle() {
    }
}
