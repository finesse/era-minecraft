package pw.finesse.era.module;

import com.google.gson.*;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;
import pw.finesse.commons.event.Listener;
import pw.finesse.commons.interfaces.Labelled;
import pw.finesse.commons.parsing.Parser;
import pw.finesse.commons.property.Property;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * This class is the root of the client module system and
 * an implementation of the {@link Labelled} interface.
 * Extensions of this class will create non-toggleable
 * in-game modifications.
 *
 * @author Marc
 * @since 8/3/2016
 */
public abstract class Module implements Labelled {

    /**
     * The system name of the module.
     */
    private final String name;

    /**
     * The label of the module.
     */
    private String label;

    /**
     * The description of the module.
     */
    private String description;

    /**
     * The {@link Category} of the module.
     */
    private final Category category;

    /**
     * An instance of {@link Minecraft}.
     */
    protected Minecraft mc = Minecraft.getMinecraft();

    /**
     * A collection of {@link Listener}s belonging to the module.
     */
    protected final Set<Listener> listeners = new CopyOnWriteArraySet<>();

    protected Module(String label, Category category) {
        this.name = this.getClass().getSimpleName();
        this.label = label;
        this.category = category;

        this.setup();
    }

    /**
     * An abstract method called on the initialization of the module.
     */
    protected abstract void setup();

    /**
     * Returns the system name of the module.
     *
     * @return system name of the module
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the label of the module.
     *
     * @return label of the module
     */
    @Override
    public String getLabel() {
        return label;
    }

    /**
     * Sets the label of the module to the specified label.
     *
     * @param label label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Returns the {@link Category} of the module.
     *
     * @return category of the module
     * @see Category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Registers (or adds) one or more {@link Listener}s to the collection.
     *
     * @param listeners {@link Listener}s to register
     */
    public void registerListeners(Listener... listeners) {
        // Iterates through the collection of listeners and adds it to the module's listener collection.
        Arrays.stream(listeners).forEach(this.listeners::add);
    }

    /**
     * Unregisters (or removes) one or more {@link Listener}s from the collection.
     *
     * @param listeners {@link Listener}s to unregister
     */
    public void unregisterListeners(Listener... listeners) {
        // Iterates through the collection of listeners and removes it from the module's listener collection.
        Arrays.stream(listeners).forEach(this.listeners::remove);
    }

    /**
     * Returns a collection of the annotated properties of the module.
     *
     * @return a collection of the annotated properties of the module
     */
    public List<Field> getProperties() {
        // Creates a local collection of fields.
        List<Field> properties = new ArrayList<>();

        // Iterates through the declared fields of the class of the module.
        for (Field field : this.getClass().getDeclaredFields()) {
            // Checks if the field is properly annotated.
            if (field.isAnnotationPresent(Property.class)) {
                // Adds the field to the local list of properties.
                properties.add(field);
            }
        }

        return properties;
    }

    /**
     * Called when the module is loaded.
     *
     * @param source file to load from
     */
    public void load(File source) {
        // Creates a local file.
        File file = new File(source + File.separator + this.getName() + ".json");

        // Checks if the file exists.
        if (!file.exists()) {
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            // Creates a local Json array.
            JsonArray array = new JsonParser().parse(reader).getAsJsonArray();
            // Checks if the reader is successfully parsed.
            if (!array.isJsonArray()) {
                return;
            }

            // Iterates through the array.
            array.forEach(element -> {
                // Creates a local Json object.
                JsonObject object = element.getAsJsonObject();

                // Checks if the object has the property "label".
                if (object.has("label")) {
                    // Sets the label of the module to the property.
                    this.setLabel(object.get("label").getAsString());
                }

                // Checks if the module is toggleable.
                if (this instanceof Toggleable) {
                    // Creates a local cast of the module.
                    Toggleable toggleable = (Toggleable) this;
                    // Checks if the object has the property "keybind".
                    if (object.has("keybind")) {
                        // Sets the key bind of the module to the property.
                        toggleable.setKeybind(Keyboard.getKeyIndex(object.get("keybind").getAsString()));
                    }
                    // Checks if the object has the property "visible".
                    if (object.has("visible")) {
                        // Sets the visibility of the module to the property.
                        toggleable.setVisible(object.get("visible").getAsBoolean());
                    }
                }

                this.getProperties().stream().filter(field -> object.has(field.getName())).forEach(field -> {
                    // Sets the accessibility of the field.
                    field.setAccessible(true);
                    // Creates a local Json element of the field.
                    JsonElement value = object.get(field.getName());

                    try {
                        // Creates an optional container of the parsed value.
                        Optional<Object> parsedContainer = Optional.ofNullable(Parser.parse(value.getAsString(), field.getType()));

                        // Checks if the parsed value is present.
                        if (parsedContainer.isPresent()) {
                            // Sets the value of the field.
                            field.set(this, parsedContainer.get());
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when the module is saved.
     *
     * @param destination file to save to
     */
    public void save(File destination) {
        // Checks if the destination exists.
        if (!destination.exists()) {
            // Creates the necessary destination to save to.
            if (!destination.mkdir()) {
                return;
            }
        }

        // Creates a local file.
        File file = new File(destination + File.separator + this.getName() + ".json");

        // Checks if the file exists.
        if (!file.exists()) {
            try {
                // Creates the necessary file.
                if (!file.createNewFile()) {
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Creates a local Json array.
        JsonArray array = new JsonArray();

        // Creates a local Json object.
        JsonObject object = new JsonObject();

        // Creates a local Json object for properties.
        JsonObject propertyObject = new JsonObject();

        // Adds the module label as a property.
        object.addProperty("label", this.getLabel());

        // Checks if the module is toggleable.
        if (this instanceof Toggleable) {
            // Creates a local cast of the module.
            Toggleable toggleable = (Toggleable) this;
            // Adds the module key bind as a property.
            object.addProperty("keybind", Keyboard.getKeyName(toggleable.getKeybind()));
            // Adds the visibility of the module as a property.
            object.addProperty("visible", toggleable.isVisible());
        }

        // Iterates through the properties of the module.
        this.getProperties().forEach(field -> {
            try {
                // Sets the accessibility of the field.
                field.setAccessible(true);

                // Adds the field as a property of the Json object.
                propertyObject.addProperty(field.getName(), String.valueOf(field.get(this)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });

        // Adds the object to the array.
        array.add(object);

        // Adds the property object to the array.
        array.add(propertyObject);

        try (FileWriter writer = new FileWriter(file)) {
            // Writes the array to the file.
            writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(array));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
