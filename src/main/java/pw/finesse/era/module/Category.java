package pw.finesse.era.module;

import pw.finesse.commons.interfaces.Labelled;

/**
 * A list of constants used to categorize in-game modifications
 * graphically. In addition, this class is an implementation
 * of the {@link Labelled} interface.
 *
 * @author Marc
 * @since 7/19/2016
 */
public enum Category implements Labelled {

    COMBAT("Combat"),
    MISCELLANEOUS("Miscellaneous"),
    MOVEMENT("Movement"),
    NONE("None"),
    PLAYER("Player"),
    VISUALS("Visuals");

    /**
     * The display name (or label) of the category.
     */
    private final String label;

    Category(String label) {
        this.label = label;
    }

    /**
     * Return the label of the category.
     *
     * @return label of the category
     */
    @Override
    public String getLabel() {
        return label;
    }
}
