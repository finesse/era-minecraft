package pw.finesse.era.module;

import net.minecraft.client.Minecraft;
import pw.finesse.commons.classloading.AutoLoader;
import pw.finesse.commons.classloading.Loader;
import pw.finesse.commons.management.impl.ListManager;
import pw.finesse.era.core.Era;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A container for all {@link Module}s.
 *
 * @author Marc
 * @since 8/3/2016
 */
public class ModuleManager extends ListManager<Module> {

    /**
     * The directory where the files of the manager are saved.
     */
    public final File DIRECTORY = new File(Era.INSTANCE.getDirectory(), "modules");

    public ModuleManager() {
        super(new CopyOnWriteArrayList<>());

        // Automatically loads the classes in the specified package path and of the specified type.
        AutoLoader.load(ModuleManager.class, "pw.finesse.era.module.impl", new Loader<Module>() {
            @Override
            public void run(Module loadedClass) {
                // Adds the module to the collection of modules.
                ModuleManager.this.register(loadedClass);

                // Loads the module.
                loadedClass.load(DIRECTORY);

                // Saves the module.
                loadedClass.save(DIRECTORY);
            }
        });

        // Sorts the collection of modules alphabetically.
        Collections.sort(this.registry, new AlphabeticalComparator());
    }

    /**
     * Attempts to find the specified {@link Module}
     * in the contents of the container. If the
     * attempt is successful, the method will return
     * a {@link Module} object. If the attempt fails,
     * the method will return {@code null}.
     *
     * @param moduleClass class of the module
     * @param <T>         type of class
     * @return {@link Module} object if successful, otherwise {@code null}
     */
    @SuppressWarnings("unchecked")
    public <T extends Module> T get(Class<T> moduleClass) {
        // Iterates through the collection of modules.
        for (Module module : this.registry) {
            // Checks if the module's class matches the specified class.
            if (module.getClass() == moduleClass) {
                return (T) module;
            }
        }

        return null;
    }

    /**
     * An implementation of {@link Comparator} modified to sort alphabetically.
     *
     * @author Marc
     * @since 8/5/2016
     */
    private class AlphabeticalComparator implements Comparator<Module> {
        @Override
        public int compare(Module first, Module second) {
            return first.getLabel().compareTo(second.getLabel());
        }
    }

    /**
     * An implementation of {@link Comparator} modified to sort by string length.
     *
     * @author Marc
     * @since 8/14/2016
     */
    private class LengthComparator implements Comparator<Module> {
        @Override
        public int compare(Module first, Module second) {
            return Integer.compare(Minecraft.getMinecraft().fontRendererObj.getStringWidth(second.getLabel()), Minecraft.getMinecraft().fontRendererObj.getStringWidth(first.getLabel()));
        }
    }
}
