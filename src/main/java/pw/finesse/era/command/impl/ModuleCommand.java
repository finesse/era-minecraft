package pw.finesse.era.command.impl;

import pw.finesse.commons.command.Command;
import pw.finesse.commons.parsing.Parser;
import pw.finesse.era.core.Era;
import pw.finesse.era.module.Module;
import pw.finesse.era.module.Toggleable;
import pw.finesse.era.module.impl.visuals.Overlay;

import java.lang.reflect.Field;
import java.util.Optional;

import static net.minecraft.util.EnumChatFormatting.*;

/**
 * @author Marc
 * @since 8/11/2016
 */
public class ModuleCommand extends Command {

    /**
     * An instance of the specified {@link Module}.
     */
    private Module module;

    public ModuleCommand(Module module) {
        super(module.getName());

        this.module = module;
    }

    @Override
    public String run(String message, String... input) {
        // Checks if the arguments are of the proper length.
        if (input.length > 1) {
            for (Field field : module.getProperties()) {
                if (input[1].equalsIgnoreCase(field.getName())) {
                    if (input.length > 2) {
                        field.setAccessible(true);

                        try {
                            Optional<Object> parsedContainer = Optional.ofNullable(Parser.parse(input[2], field.getType()));

                            if (parsedContainer.isPresent()) {
                                field.set(module, parsedContainer.get());
                            }

                            return String.format("%s%s %s has been set to %s%s%s.", GRAY, module.getLabel(), field.getName().toLowerCase(), GREEN, field.get(module), GRAY);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (field.getType() == Boolean.TYPE) {
                            field.setAccessible(true);
                            try {
                                field.set(module, !((boolean) field.get(module)));
                                return String.format("%s%s %s has been set to %s%s%s.", GRAY, module.getLabel(), field.getName().toLowerCase(), GREEN, field.get(module), GRAY);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            switch (input[1]) {
                case "col":
                case "color": {
                    if (input.length > 2) {
                        for (Field field : Overlay.class.getDeclaredFields()) {
                            try {
                                field.setAccessible(true);
                                Field map = Overlay.class.getDeclaredField("colorMap");


                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    break;
                }

                case "l":
                case "label": {
                    if (input.length > 2) {
                        // The previous label of the module.
                        String label = module.getLabel();

                        // Sets the label to the specified label.
                        module.setLabel(message.substring(input[0].length() + input[1].length() + 2));

                        return String.format("%s%s has been labelled %s'%s'%s.", GRAY, label, GREEN, module.getLabel(), GRAY);
                    }
                    break;
                }

                case "v":
                case "vis":
                case "visible": {
                    if (module instanceof Toggleable) {
                        // Creates a local cast of the module.
                        Toggleable toggleable = (Toggleable) module;

                        // Checks if the arguments are of the proper length.
                        if (input.length > 2) {
                            try {
                                // Sets the visibility of the module to the parsed input.
                                toggleable.setVisible(Boolean.parseBoolean(input[2]));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            // Swaps the visible states of the module.
                            toggleable.setVisible(!toggleable.isVisible());
                        }

                        // Creates a local string displaying the visibility state of the module.
                        String visibility = toggleable.isVisible() ? "visible" : "hidden";

                        return String.format("%s%s has been made" + " %s" + visibility + "%s.", GRAY, module.getLabel(), toggleable.isVisible() ? GREEN : RED, GRAY);
                    } else {
                        return String.format("%s%s%s is not a toggleable module!", GREEN, module.getLabel(), RESET);
                    }
                }

            }
        } else {
            // Checks if the module is a toggleable module.
            if (module instanceof Toggleable) {
                // Creates a local cast of the module.
                Toggleable toggleable = (Toggleable) module;

                // Swaps the states of the module.
                toggleable.toggle();

                // Creates a local string displaying the enabled state of the module.
                String enabled = toggleable.isEnabled() ? "enabled" : "disabled";

                return String.format("%s%s has been" + " %s" + enabled + "%s.", GRAY, module.getLabel(), toggleable.isEnabled() ? GREEN : RED, GRAY);
            } else {
                return String.format("%s%s is not a toggleable module!", GRAY, module.getLabel());
            }
        }

        this.module.save(Era.INSTANCE.getModuleManager().DIRECTORY);

        return null;
    }
}
