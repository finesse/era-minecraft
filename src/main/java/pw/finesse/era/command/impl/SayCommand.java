package pw.finesse.era.command.impl;

import net.minecraft.network.play.client.C01PacketChatMessage;
import pw.finesse.commons.classloading.AutoLoad;
import pw.finesse.commons.command.Command;
import pw.finesse.era.core.Era;

/**
 * A command that sends the specified message. This is
 * useful for users to circumvent the "command checks"
 * of admins.
 *
 * @author Marc
 * @since 8/11/2016
 */
@AutoLoad(Command.class)
public class SayCommand extends Command {

    public SayCommand() {
        super("say", "s");
    }

    @Override
    public String run(String message, String... input) {
        // Creates a local instance of the message to send.
        message = message.substring(input[0].length());

        // Sends a chat packet containing the specified message.
        Era.INSTANCE.getMinecraft().getNetHandler().addToSendQueue(new C01PacketChatMessage(message));

        return null;
    }
}
