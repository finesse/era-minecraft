package pw.finesse.era.command;

import net.minecraft.util.ChatComponentText;
import pw.finesse.commons.classloading.AutoLoader;
import pw.finesse.commons.classloading.Loader;
import pw.finesse.commons.command.Command;
import pw.finesse.commons.management.impl.ListManager;
import pw.finesse.era.command.impl.ModuleCommand;
import pw.finesse.era.core.Era;

import java.util.ArrayList;
import java.util.Optional;

import static net.minecraft.util.EnumChatFormatting.DARK_GRAY;
import static net.minecraft.util.EnumChatFormatting.RESET;

/**
 * @author Marc
 * @since 8/11/2016
 */
public class CommandManager extends ListManager<Command> {

    /**
     * The prefix of all commands.
     */
    public static final String COMMAND_PREFIX = "-";

    public CommandManager() {
        super(new ArrayList<>());

        // Automatically loads the classes in the specified package path and of the specified type.
        AutoLoader.load(CommandManager.class, "pw.finesse.era.command.impl", new Loader<Command>() {
            @Override
            public void run(Command loadedClass) {
                // Adds the command to the collection of commands.
                CommandManager.this.register(loadedClass);
            }
        });

        // Loads a command for every module in the client.
        Era.INSTANCE.getModuleManager().registry().forEach(module -> CommandManager.this.register(new ModuleCommand(module)));
    }

    /**
     * Returns the corresponding output from the specified input
     * in the form of an {@link Optional} object.
     *
     * @param input text input
     * @return console output
     */
    public Optional<String> parse(String input) {
        // Creates a local instance of the arguments from the specified input.
        String[] arguments = input.split(" ");

        // Iterates through the registry of commands.
        for (Command command : this.registry) {
            // Checks if the first index of the arguments matches the label of the command.
            if (arguments[0].equalsIgnoreCase(command.getLabel())) {
                // Creates an optional container of the output and runs it.
                Optional<String> outputContainer = Optional.ofNullable(command.run(input, arguments));

                // Checks if the output is present.
                if (outputContainer.isPresent()) {
                    return outputContainer;
                }
            } else {
                // Iterates through the aliases of the command.
                for (String alias : command.getAliases()) {
                    // Checks if the first index of the arguments matches the alias of the command.
                    if (arguments[0].equalsIgnoreCase(alias)) {
                        // Creates an optional container of the output and runs it.
                        Optional<String> outputContainer = Optional.ofNullable(command.run(input, arguments));

                        // Checks if the output is present.
                        if (outputContainer.isPresent()) {
                            return outputContainer;
                        }
                    }
                }
            }
        }

        return Optional.empty();
    }

    /**
     * Prints the specified text to the chat.
     *
     * @param text text to print
     */
    public void print(String text) {
        Era.INSTANCE.getMinecraft().thePlayer.addChatMessage(new ChatComponentText(String.format("%s[E]%s %s", DARK_GRAY, RESET, text)));
    }

}
